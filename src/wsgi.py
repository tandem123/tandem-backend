# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

"""This module acts an access point for the WSGI Gunicorn"""

# Import the Tandem Web Server API
import tandem
tandem_web_api = tandem.get_web_api()

if( __name__ == '__main__' ):
	tandem_web_api.run()