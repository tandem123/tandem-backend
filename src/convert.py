# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, April 2017

import old_db_models
import tandem.task.doc_models
import tandem.tools.helpers
import math
import uuid

def main():
	old_db_models.database.connect()
	tandem.task.doc_models.connect()
	
	for agreement in old_db_models.Agreement.select():
		transaction = old_db_models.TransactionLog.select( old_db_models.TransactionLog.transaction_amt ).where( old_db_models.TransactionLog.resource_id == agreement.agreement_id & old_db_models.TransactionLog.type_ind == 'Charge' ).get()
		agreement_data = tandem.task.doc_models.FixedAgreement(
			agreement_id=agreement.agreement_id,
			base_cost_amt=transaction.transaction_amt,
			driver_service_fee_amt=0,
			host_service_fee_amt=int( math.floor( transaction.transaction_amt * 0.75 ) ),
			rules=[
				tandem.task.doc_models.RuleCost(
					rate_amt=agreement.locked_rate_amt,
					cost_amt=transaction.transaction_amt,
					type_ind='Hourly',
					dates=[
						tandem.task.doc_models.RuleCostDate(
							start_dtm=tandem.tools.helpers.datetime_str2naive( agreement.creation_dtm.isoformat() ),
							end_dtm=tandem.tools.helpers.datetime_str2naive( agreement.finish_dtm.isoformat() )
						)
					]
				)
			]
		)
		
		agreement_data.save()
		
	for listing in old_db_models.Listing.select():
		dates = []
		try:
			whiteon = old_db_models.Whiteon.select().where( old_db_models.Whiteon.listing_id == listing.listing_id ).get()
			dates = [
				tandem.task.doc_models.AvailableDate(
					repeating_ind=whiteon.timeslice.repeating_ind,
					start_dtm=tandem.tools.helpers.datetime_str2naive( whiteon.timeslice.start_dtm.isoformat() ),
					end_dtm=tandem.tools.helpers.datetime_str2naive( whiteon.timeslice.end_dtm.isoformat() )
				)
			]
		except old_db_models.Whiteon.DoesNotExist:
			pass
			
		listing_data = tandem.task.doc_models.ListingData(
			listing_id=listing.listing_id,
			payment_rules=[
				tandem.task.doc_models.HourlyPaymentRule(
					rate_amt=listing.rate_amt,
					rule_id=uuid.uuid4(),
					available_dates=dates
				)
			]
		)
		
		listing_data.save()
