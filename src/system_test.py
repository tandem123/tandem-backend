# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, November 2016

import unittest
from tandem.web import tandem_web_api
import tandem.tools.constants
import json
import time
import toml

session_data = {}
app = tandem_web_api.test_client()

class TestError( Exception ):
	pass
	
class BadTokenError( TestError ):
	pass
	
def post( endpoint, data, headers ):
	global app
	
	ret = app.post( endpoint, data = json.dumps( data ), headers = headers )
	ret_data = json.loads( ret.data )
	
	assert 'data' in ret_data, 'Post response missing data section'
	assert 'status' in ret_data, 'Post response missing status section'
	
	if ret_data['status'] == 'fail':
		if ret_data['data']['code'] == 'BadToken':
			raise BadTokenError
	
	return ret_data
	
def get( endpoint, headers ):
	global app
	
	ret = app.get( endpoint, headers = headers )
	ret_data = json.loads( ret.data )
	
	assert 'data' in ret_data, 'Post response missing data section'
	assert 'status' in ret_data, 'Post response missing status section'
	
	if ret_data['status'] == 'fail':
		if ret_data['data']['code'] == 'BadToken':
			raise BadTokenError
	
	return ret_data
	
def patch( endpoint, data, headers ):
	global app

	ret = app.patch( endpoint, data = json.dumps( data ), headers = headers )
	ret_data = json.loads( ret.data )
	
	assert 'data' in ret_data, 'Patch response missing data section'
	assert 'status' in ret_data, 'Patch response missing status section'
	
	if ret_data['status'] == 'fail':
		if ret_data['data']['code'] == 'BadToken':
			raise BadTokenError
	
	return ret_data
	
def create_user_email( email, password, first_name, last_name, referral_code ):
	data = {
		'email': email,
		'password': password,
		'firstName': first_name,
		'lastName': last_name,
		'referralCode': referral_code
	}
	response = post( '/v0.0.1/users', data, {'Content-Type': 'application/json'} )
	
	if( response.get( 'status', None ) == 'success' ):
		assert response['data'].get( 'email' ) == email, 'Wrong email returned. Got %s, expected %s' % ( response['data'].get( 'email' ), email )
		assert response['data'].get( 'firstName' ) == first_name, 'Wrong first name returned. Got %s, expected %s' % ( response['data'].get( 'firstName' ), first_name )
		assert response['data'].get( 'lastName' ) == last_name, 'Wrong last name returned. Got %s, expected %s' % ( response['data'].get( 'lastName' ), last_name )
		assert response['data'].get( 'accountType' ) == 'Driver', 'Wrong account type returned. Got %s, expected %s' % ( response['data'].get( 'accountType' ), 'Driver' )
		assert response['data'].get( 'authToken' ) is not None, 'Auth token not returned'
		assert response['data'].get( 'firebaseAuthToken' ) is not None, 'Firebase auth token not returned'
		
	return response

def promote_user( auth_token ):
	data = {
		'phoneNumber': '(408)892-9036'
	}
	response = post( '/v0.0.1/users/promote', data, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + auth_token} )
	
	if response.get( 'status', None ) == 'success':
		assert response['data'].get( 'accountType' ) == 'Host', 'Wrong account type returned. Got %s, expected %s' % ( response['data'].get( 'accountType' ), 'host' )
		
	return response

def create_user_vehicle( auth_token, license_plate, color, make, model, year ):
	data = {
		'licensePlateNumber': license_plate,
		'color': color,
		'make': make,
		'model': model,
		'year': year
	}
	response = post( '/v0.0.1/users/vehicles', data, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + auth_token} )
	
	if response.get( 'status', None ) == 'success':
		assert response['data'].get( 'licensePlateNumber' ) == license_plate, 'Wrong license plate number returned. Got %s, expected %s' % ( response['data'].get( 'licensePlateNumber' ), license_plate )
		assert response['data'].get( 'color' ) == color, 'Wrong color returned. Got %s, expected %s' % ( response['data'].get( 'color' ), color )
		assert response['data'].get( 'make' ) == make, 'Wrong make returned. Got %s, expected %s' % ( response['data'].get( 'make' ), make )
		assert response['data'].get( 'model' ) == model, 'Wrong model returned. Got %s, expected %s' % ( response['data'].get( 'model' ), model )
		assert response['data'].get( 'year' ) == year, 'Wrong year returned. Got %s, expected %s' % ( response['data'].get( 'year' ), year )
	
	return response
	
def create_user_card( auth_token ):
	import stripe
	stripe.api_key = tandem.tools.constants.CONST.STRIPE_KEY
	
	token = stripe.Token.create( card = {
		'number': '4000000000000077',
		'exp_month': 12,
		'exp_year': 2017,
		'cvc': '123'
	})
	
	data = {
		'token': token['id']
	}
	
	response = post( '/v0.0.1/users/payments', data, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + auth_token} )
		
	return response
	
def update_user( user_id, auth_token, ops ):
	response = patch( 'v0.0.1/users/' + str( user_id ), ops, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + auth_token} )
	
	if response.get( 'status', None ) == 'success':
		for operation in ops:
			op = operation.get( 'op' )
			path = operation.get( 'path' )
			value = operation.get( 'value' )
			path = path[1:]
			assert response['data'].get( path ) == value, 'User update failed: %s not changed to %s' % ( path, value )
		
	return response
	
def join_referral_program( auth_token, program ):
	data = {
		'program': program
	}
	response = post( '/v0.0.1/users/referral', data, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + auth_token})
	return response
	
def get_user_referral_code( auth_token ):
	response = get( '/v0.0.1/users/referral/code', {'Authorization': 'Bearer ' + auth_token} )
	
	print response
	
	return response
	
def auth_user( email, password ):
	data = {
		'email': email,
		'password': password
	}
	response = get( '/v0.0.1/users/auth?email=%s&password=%s' % ( email, password ), {} )
		
	return response
	
def create_listing( auth_token, rate, description, country, administrative_area, sub_administrative_area, locality, dependent_locality, postal_code, throughfare, premise, sub_premise ):
	data = {
		'rate': rate,
		'description': description,
		'country': country,
		'administrativeArea': administrative_area,
		'subadministrativeArea': sub_administrative_area,
		'locality': locality,
		'dependentLocality': dependent_locality,
		'postalCode': postal_code,
		'throughfare': throughfare,
		'premise': premise,
		'subpremise': sub_premise
	}
	
	response = post( '/v0.0.1/listings', data, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + auth_token} )
		
	if response.get( 'status', None ) == 'success':
		assert response['data'].get( 'rate' ) == rate, 'Wrong rate returned: Got %s, expected %s' % ( response['data'].get( 'rate' ), rate )
		assert response['data'].get( 'description' ) == description, 'Wrong description returned: Got %s, expected %s' % ( response['data'].get( 'description' ), description )
	
	return response
	
def create_listing_whiteon( listing_id, auth_token, start, end, repeating ):
	data = [{
		'start': start,
		'end': end,
		'repeating': repeating
	}]
	response = post( '/v0.0.1/listings/%d/blackouts' % ( listing_id ), data, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + auth_token} )
	
	if response.get( 'status', None ) == 'success':
		assert response['data']['validBlackouts'][0]['timeslice'].get( 'repeating' ) == repeating, 'Wrong repeating returned: Got %s, expected %s' % ( response['data']['validBlackouts'][0]['timeslice'].get( 'repeating' ), repeating )
		assert response['data']['validBlackouts'][0]['timeslice'].get( 'type' ) == 'Whiteon', 'Wrong type returned: Got %s, expected %s' % ( response['data']['validBlackouts'][0]['timeslice'].get( 'type' ), 'Whiteon' )
	
	return response
	
def create_agreement( auth_token, listing_id, vehicle_id, card, start, end ):
	data = {
		'listingId': listing_id,
		'vehicleId': vehicle_id,
		'card': card,
		'start': start,
		'end': end
	}
	response = post( '/v0.0.1/agreements', data, {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + auth_token} )
	
	print response
	
	return response
	
class BaseTest( unittest.TestCase ):
	def __init__( self, test_params, *args, **kwargs ):
		self.test_params = test_params
		super( BaseTest, self ).__init__( *args, **kwargs )
	
class CleanEnvironmentTest( BaseTest ):
	def setUp( self ):
		self.start = time.time()
		print 'Starting %s...' % ( self.id() )
		
	def tearDown( self ):
		t = time.time() - self.start
		print '\n%s Finished: %.3f' % ( self.id(), t )
		
	def runTest( self ):
		import peewee
		import tandem.task.db_models
		
		rel_time = time.time()
		
		print '\nClearing mysql tables...'
		for model_name, model in tandem.task.db_models._model_list.iteritems():
			if model_name in self.test_params.get( 'mysql', [] ) or len( self.test_params.get( 'mysql', [] ) ) == 0:
				print 'Deleting all records and dependencies in %s' % ( model_name, )
				for record in model.select():
					record.delete_instance( recursive = True )
		print 'Cleared mysql tables: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()

class CreateUsersTest( BaseTest ):
	def setUp( self ):
		self.start = time.time()
		print 'Starting %s...' % ( self.id() )
		
	def tearDown( self ):
		t = time.time() - self.start
		print '\n%s Finished: %.3f' % ( self.id(), t )
						
	def runTest( self ):
		global session_data
		
		rel_time = time.time()
		
		print '\nCreating test host user...'
		ret = create_user_email( 'test-host@tandemparking.co', 'password', 'Tandem', 'Host', None )
		assert ret.get( 'status', None ) == 'success', 'Test host account was not successfully created'
		
		if not 'host_data' in session_data:
			session_data.update( {'host_data':{}} )
		session_data['host_data'] = ret['data']
		
		print 'Created test host user: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
		time.sleep( 1 )
		
		print '\nUpdating test host\'s name...'
		ret = update_user( session_data['host_data']['id'], session_data['host_data']['authToken'], [{"op":"replace","path":"/firstName","value":"Host"},{"op":"replace","path":"/lastName","value":"Tandem"}] )
		assert ret.get( 'status', None ) == 'success', 'Unable to update test host\'s name'
		print 'Updated test host\'s name: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
				
		print '\nPromoting test host to Host...'
		ret = promote_user( session_data['host_data']['authToken'] )
		assert ret.get( 'status', None ) == 'success', 'Unable to promote test host to Host'
		print 'Promoted test host to Host: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
	
		print '\nCreating test driver user...'
		ret = create_user_email( 'test-driver@tandemparking.co', 'password', 'Tandem', 'Driver', None )
		assert ret.get( 'status', None ) == 'success', 'Test driver account was not successfully created'
		
		if not 'driver_data' in session_data:
			session_data.update( {'driver_data':{}} )
		session_data['driver_data'] = ret['data']
		
		print 'Created test driver user: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
		time.sleep( 1 )
		
		print '\nCreating vehicle for test driver...'
		ret = create_user_vehicle( session_data['driver_data']['authToken'], 'T35TC4R', 'Blue', 'Toyota', 'Rav4', '1995' )
		assert ret.get( 'status', None ) == 'success', 'Vehicle not created for test driver'
		
		if not 'vehicles' in session_data['driver_data']:
			session_data['driver_data'].update( {'vehicles':[]} )
		session_data['driver_data']['vehicles'].append( ret['data'] )
		
		print 'Created vehicle for test driver: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
				
		print '\nCreating card for test driver...'
		ret = create_user_card( session_data['driver_data']['authToken'] )
		assert ret.get( 'status', None ) == 'success', 'Card not created for test driver'
		
		if not 'cards' in session_data['driver_data']:
			session_data['driver_data'].update( {'cards':[]} )
		session_data['driver_data']['cards'].append( ret['data'] )
		
		print 'Created card for test driver: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()

		print '\nCreating listing for test host...'
		try:
			ret = create_listing( session_data['host_data']['authToken'], 250, 'Test Californian listing', 'US', 'CA', None, 'Isla Vista', None, '93117', '6552 Del Playa Drive', 'Unit 101', None )
		except BadTokenError:
			ret = auth_user( session_data['host_data']['email'], 'password' )
			session_data['host_data']['authToken'] = ret['data']['authToken']
			ret = create_listing( session_data['host_data']['authToken'], 250, 'Test Californian listing', 'US', 'CA', None, 'Isla Vista', None, '93117', '6552 Del Playa Drive', 'Unit 101', None )
		assert ret.get( 'status', None ) == 'success', 'Listing not created for test host'
		
		if not 'listings' in session_data['host_data']:
			session_data['host_data'].update( {'listings':[]} )
		session_data['host_data']['listings'].append( ret['data'] )
		
		print 'Created listing for test host: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
		print '\nCreating whiteon for listing...'
		try:
			ret = create_listing_whiteon( session_data['host_data']['listings'][0]['id'], session_data['host_data']['authToken'], '2016-11-23T09:00Z', '2016-11-23T17:00Z', 'Daily' )
		except BadTokenError:
			ret = auth_user( session_data['host_data']['email'], 'password' )
			session_data['host_data']['authToken'] = ret['data']['authToken']
			ret = create_listing_whiteon( session_data['host_data']['listings'][0]['id'], session_data['host_data']['authToken'], '2016-11-23T09:00Z', '2016-11-23T17:00Z', 'Daily' )
		assert ret.get( 'status', None ) == 'success', 'Whiteon not created for listing'
		
		if not 'whiteons' in session_data['host_data']['listings'][0]:
			session_data['host_data']['listings'][0].update( {'whiteons':[]} )
		session_data['host_data']['listings'][0]['whiteons'].append( ret['data'] )
		
		print 'Created whiteon for listing: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
		print '\nJoining test host into host referral program...'
		try:
			ret = join_referral_program( session_data['host_data']['authToken'], 'host_referral_program' )
		except BadTokenError:
			ret = auth_user( session_data['host_data']['email'], 'password' )
			session_data['host_data']['authToken'] = ret['data']['authToken']
			ret = join_referral_program( session_data['host_data']['authToken'] )
		assert ret.get( 'status', None ) == 'success', 'Could not join test host into host referral program'
		
		session_data['host_data']['referral_code'] = ret['data']
		
		print 'Joined test host into host referral program: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
class CreateReferredHostTest( BaseTest ):
	def setUp( self ):
		self.start = time.time()
		print 'Starting %s...' % ( self.id() )
		
	def tearDown( self ):
		t = time.time() - self.start
		print '\n%s Finished: %.3f' % ( self.id(), t )
						
	def runTest( self ):
		global session_data
		
		rel_time = time.time()
		
		print '\nRetrieving test host\'s referral code...'
		ret = get_user_referral_code( session_data['host_data']['authToken'] )
		assert ret.get( 'status', None ) == 'success', 'Could not retrieve test host\'s referral code'
		code = ret['data']
		print 'Retrieved test host\'s referral code: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
		print '\nCreating referred test host...'
		ret = create_user_email( 'test-referred-host@tandemparking.co', 'password', 'Tandem', 'Referred Host', code )
		assert ret.get( 'status', None ) == 'success', 'Referred host not created'
		
		if not 'referred_host_data' in session_data:
			session_data.update( {'referred_host_data':{}} )
		session_data['referred_host_data'] = ret['data']
		
		print 'Created referred test host: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
		print '\nPromoting referred test host to Host...'
		ret = promote_user( session_data['referred_host_data']['authToken'] )
		assert ret.get( 'status', None ) == 'success', 'Unable to promote referred test host to Host'
		print 'Promoted referred test host to Host: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
		print '\nCreating listing for referred test host...'
		try:
			ret = create_listing( session_data['referred_host_data']['authToken'], 250, 'Test Californian listing', 'US', 'CA', None, 'Isla Vista', None, '93117', '6552 Del Playa Drive', 'Unit 101', None )
		except BadTokenError:
			ret = auth_user( session_data['referred_host_data']['email'], 'password' )
			session_data['referred_host_data']['authToken'] = ret['data']['authToken']
			ret = create_listing( session_data['referred_host_data']['authToken'], 250, 'Test Californian listing', 'US', 'CA', None, 'Isla Vista', None, '93117', '6552 Del Playa Drive', 'Unit 101', None )
		assert ret.get( 'status', None ) == 'success', 'Listing not created for referred test host'
		
		if not 'listings' in session_data['referred_host_data']:
			session_data['referred_host_data'].update( {'listings':[]} )
		session_data['referred_host_data']['listings'].append( ret['data'] )
		
		print 'Created listing for referred test host: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
		print '\nCreating whiteon for listing...'
		try:
			ret = create_listing_whiteon( session_data['referred_host_data']['listings'][0]['id'], session_data['referred_host_data']['authToken'], '2016-11-23T09:00Z', '2016-11-23T17:00Z', 'Daily' )
		except BadTokenError:
			ret = auth_user( session_data['referred_host_data']['email'], 'password' )
			session_data['referred_host_data']['authToken'] = ret['data']['authToken']
			ret = create_listing_whiteon( session_data['referred_host_data']['listings'][0]['id'], session_data['referred_host_data']['authToken'], '2016-11-23T09:00Z', '2016-11-23T17:00Z', 'Daily' )
		assert ret.get( 'status', None ) == 'success', 'Whiteon not created for listing'
		
		if not 'whiteons' in session_data['referred_host_data']['listings'][0]:
			session_data['referred_host_data']['listings'][0].update( {'whiteons':[]} )
		session_data['referred_host_data']['listings'][0]['whiteons'].append( ret['data'] )
		
		print 'Created whiteon for listing: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
class CreateAgreementTest( BaseTest ):
	def setUp( self ):
		self.start = time.time()
		print 'Starting %s...' % ( self.id() )
		
	def tearDown( self ):
		t = time.time() - self.start
		print '\n%s Finished: %.3f' % ( self.id(), t )
						
	def runTest( self ):
		global session_data
		
		rel_time = time.time()
		
		print '\nCreating agreement between test driver and test host...'
		try:
			ret = create_agreement( session_data['driver_data']['authToken'], session_data['referred_host_data']['listings'][0]['id'], session_data['driver_data']['vehicles'][0]['id'], session_data['driver_data']['cards'][0], '2017-12-25T13:00Z', '2017-12-25T16:00Z' )
		except BadTokenError:
			ret = auth_user( session_data['driver_data']['email'], 'password' )
			session_data['driver_data']['authToken'] = ret['data']['authToken']
			ret = create_agreement( session_data['driver_data']['authToken'], session_data['host_data']['listings'][0]['id'], session_data['driver_data']['vehicles'][0]['id'], session_data['driver_data']['cards'][0], '2017-12-25T10:00Z', '2017-12-25T13:00Z' )
		assert ret.get( 'status', None ) == 'success', 'Agreement not created between test host and test driver'
		
		if not 'agreements' in session_data:
			session_data.update( {'agreements':[]} )
		session_data['agreements'].append( ret['data'] )
		
		print 'Created agreement between test driver and test host: %.3f' % ( time.time() - rel_time )
		rel_time = time.time()
		
def input2dict( input ):
	if '{' in input:
		return json.loads( input[input.find( '{' ):] )
	else:
		return {}

def main():
	import sys
	
	global session_data
	
	try:
		with open( 'test_session_data.conf' ) as conf:
			session_data = toml.loads( conf.read() )
	except IOError:
		session_data = {}
	
	if tandem.tools.constants.CONST.DEBUG_MODE is True:
		if len( sys.argv ) > 1:
			test = unittest.TestSuite()
			
			clean_op = [ x for x in sys.argv if x.startswith( 'clean' ) ]
			if len( clean_op ) > 0:
				for op in clean_op:
					test.addTest( CleanEnvironmentTest( input2dict( op ) ) )
					print input2dict( op )
			
			create_users_op = [ x for x in sys.argv if x.startswith( 'create_users' ) ]
			if len( create_users_op ) > 0:
				for op in create_users_op:
					test.addTest( CreateUsersTest( input2dict( op ) ) )
					
			create_agreement_op = [ x for x in sys.argv if x.startswith( 'create_agreement' ) ]
			if len( create_agreement_op ) > 0:
				for op in create_agreement_op:
					test.addTest( CreateAgreementTest( input2dict( op ) ) )
					
			create_referred_host_op = [ x for x in sys.argv if x.startswith( 'create_referred_host' ) ]
			if len( create_referred_host_op ) > 0:
				for op in create_referred_host_op:
					test.addTest( CreateReferredHostTest( input2dict( op ) ) )
			
			unittest.TextTestRunner().run( test )
				
		else:
			print 'Please enter an option'
	else:
		print 'Testing only allowed in a debug environment'
		
	with open( 'test_session_data.conf', 'w+' ) as conf:
		conf.write( toml.dumps( session_data ) )
	
if __name__ == '__main__':
    main()