# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

"""This module contains cronjobs"""
import tandem.task.db_models

def expire_agreements():
	tandem.task.db_models.Agreement.expire_agreements()
	
def start_agreements():
	tandem.task.db_models.Agreement.start_agreements()
	
def referral_reward_hosts():
	tandem.task.db_models.Agreement.referral_reward_hosts()
	