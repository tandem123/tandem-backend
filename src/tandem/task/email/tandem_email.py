# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

import smtplib
import email.mime.multipart
import email.mime.text
import email.utils
import email.header
import os
import traceback


class EmailClient( object ):

	def __init__( self, username=None, password=None ):
		self.server = smtplib.SMTP( 'smtp.gmail.com:587' )
		self.server.ehlo()
		self.server.starttls()
		self.server.login( username, password )

	def close( self ):
		self.server.quit()

	def connect( self ):
		self.server.connect()

	def send( self, fromaddr, toaddr, subject, msg ):
		new_msg = '\r\n'.join( [
			'From: {}'.format( fromaddr ),
			'To: {}'.format( toaddr ),
			'Subject: {}'.format( subject ),
			'',
			msg
		] )
		self.server.sendmail( fromaddr, [toaddr], new_msg )

	def send_template( self, fromaddr, toaddr, subject, template, **kwargs ):
		msg = email.mime.multipart.MIMEMultipart( 'alternative' )
		msg['From'] = email.utils.formataddr(
			(
				str( email.header.Header( 'Tandem Parking', 'utf-8' ) ),
				fromaddr ) )
		msg['To'] = toaddr
		msg['Subject'] = subject
		with open( os.path.join(
			os.path.dirname( os.path.realpath( __file__ ) ),
			''.join( [template, '.html'] ) ) ) as file:
			template_content = file.readlines()

		body = ''.join( template_content ).format( **kwargs )
		body_part = email.mime.text.MIMEText( body, 'html' )
		msg.attach( body_part )

		self.server.sendmail( fromaddr, [toaddr], msg.as_string() )

	def send_error( self, error ):
		msg = '\r\n'.join( [
			'From: {}'.format( 'noreply@tandemparking.co' ),
			'To: {}'.format( 'jacob@planethull.com' ),
			'Subject:'.format( 'Tandem Error' ),
			'',
			traceback.format_exc( error )
		] )
		self.server.sendmail(
			'noreply@tandemparking.co',
			['jacob@planethull.com'],
			msg
		)
