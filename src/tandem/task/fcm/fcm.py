# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

import requests
import tandem.task.fcm.exceptions


class FCMClient( object ):

	def __init__( self, api_key=None, sender_id=None ):
		self.api_key = api_key
		self.sender_id = sender_id

	@staticmethod
	def send_request( url, headers, json, return_response=False ):
		fcm_request = requests.post( url, headers=headers, json=json )

		if fcm_request.status_code == 400:
			raise tandem.task.fcm.exceptions.FCMBadRequestError( fcm_request.text )
		elif fcm_request.status_code == 401:
			raise tandem.task.fcm.exceptions.FCMAuthorizationError(
				'Server rejected your credentials' )

		if return_response:
			try:
				return fcm_request.json()
			except Exception:
				return None

	def form_headers( self, api_key=True, sender_id=False ):
		headers = {
			'Content-Type': 'application/json'
		}

		if self.api_key is None and api_key:
			raise tandem.task.fcm.exceptions.FCMAuthorizationError(
				'API key is not set' )
		elif api_key:
			headers.update( { 'Authorization': 'key=%s' % self.api_key } )

		if self.sender_id is None and sender_id:
			raise tandem.task.fcm.exceptions.FCMAuthorizationError(
				'Sender id is not set' )
		elif sender_id:
			headers.update( { 'project_id': self.sender_id } )

		return headers

	def device_group_create( self, name, ids ):
		headers = self.form_headers( sender_id=True )

		json = {
			'operation': 'create',
			'notification_key_name': name,
			'registration_ids': ids
		}
		return FCMClient.send_request(
			'https://android.googleapis.com/gcm/notification',
			headers,
			json,
			return_response=True )

	def device_group_add( self, name, key, ids ):
		headers = self.form_headers( sender_id=True )
		json = {
			'operation': 'add',
			'notification_key_name': name,
			'notification_key': key,
			'registration_ids': ids
		}
		FCMClient.send_request(
			'https://android.googleapis.com/gcm/notification',
			headers,
			json )

	def device_group_remove( self, name, key, ids ):
		headers = self.form_headers( sender_id=True )
		json = {
			'operation': 'remove',
			'notification_key': key,
			'notification_key_name': name,
			'registration_ids': ids
		}
		FCMClient.send_request(
			'https://android.googleapis.com/gcm/notification',
			headers,
			json )

	def device_group_send( self, name, key, data, body=None, title=None ):
		headers = self.form_headers()
		json = {
			'to': key,
			'priority': 'high',
			'notification': {
				'body': body if body else '',
				'title': title if title else ''
			},
			'data': data if data else {}
		}
		response = FCMClient.send_request(
			'https://fcm.googleapis.com/fcm/send',
			headers,
			json,
			return_response=True )
		if not response:
			raise tandem.task.fcm.exceptions.FCMBadRequestError(
				'This API is shit but my best guess is that your are'
				' trying to send a message to an empty device group' )

		if 'results' in response:
			error_list = []
			for result in response['results']:
				if 'error' in result:
					error_list.append( result['error'] )

			if len( error_list ) > 0:
				raise tandem.task.fcm.exceptions.FCMBadRequestError(
					'API request failed with ' + ', '.join( error_list ) )

		return response
