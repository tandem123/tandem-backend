# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016


class FCMException( Exception ):
	pass


class FCMAuthorizationError( FCMException ):
	pass


class FCMBadRequestError( FCMException ):
	pass
