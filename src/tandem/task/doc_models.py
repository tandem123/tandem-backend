import mongoengine
import datetime
import math
import uuid
import tandem_exceptions

from ..tools import constants, helpers


class MongoTransaction():
	def __init__( self ):
		self.checksum = {}
		self.active = False

	def activate( self ):
		self.active = True

	def deactivate( self ):
		self.active = False

	def register_document( self, document ):
		if self.active is True:
			self.checksum.update(
				{
					document.id:
					{
						'new': True if document._version is 1 else False,
						'document': document, 'fields': {}
					}
				} )
			for key, val in document._fields.iteritems():
				self.checksum[document.id]['fields'].update(
					{
						key: getattr( document, key )
					} )

	def revert( self ):
		for id, info in self.checksum.iteritems():
			if info['new'] is False:
				for key, val in info['document']._fields.iteritems():
					if key == '_id':
						continue

					if key not in info['fields']:
						setattr( info['document'], key, None )
					else:
						setattr( info['document'], key, info['fields'][key] )

				info['document'].save( roll_conditions=True )
			else:
				if id is not None:
					info['document'].delete()

	def clear( self ):
		self.checksum.clear()


_transaction = MongoTransaction()


def _parametrized( dec ):
	def layer( *args, **kwargs ):
		def repl( f ):
			return dec( f, *args, **kwargs )
		return repl
	return layer


@_parametrized
def mongo_transaction( method, retries=1 ):
	def method_wrapper( *args, **kwargs ):
		global _transaction

		_transaction.activate()
		for i in range( retries ):
			try:
				return method( *args, **kwargs )
			except Exception:
				try:
					_transaction.revert()
				except tandem_exceptions.RollbackError:
					pass

				if i == retries - 1:
					_transaction.deactivate()
					_transaction.clear()
					raise

				_transaction.clear()
				continue

			break

		_transaction.deactivate()
		_transaction.clear()

	return method_wrapper


def connect():
	mongoengine.connect(
		constants.CONST.MONGO_DB,
		host=constants.CONST.MONGO_HOST,
		port=constants.CONST.MONGO_PORT,
		username=constants.CONST.MONGO_USER,
		password=constants.CONST.MONGO_PASS )


class CustomDocument( mongoengine.Document ):
	_version = mongoengine.IntField( default=0 )

	def save( self, roll_conditions=False, *args, **kwargs ):
		self._version += 1
		if self._version != 1:
			if roll_conditions is False:
				return super( CustomDocument, self ).save(
					save_condition={'_version': self._version - 1},
					*args,
					**kwargs )
			elif roll_conditions is True:
				self._version -= 1
				try:
					return super( CustomDocument, self ).save(
						save_condition={'_version__gte': self._version},
						*args,
						**kwargs )
				except mongoengine.errors.SaveConditionError:
					raise tandem_exceptions.OlderRollbackError
		else:
			return super( CustomDocument, self ).save( *args, **kwargs )

	@classmethod
	def post_save( cls, sender, document, **kwargs ):
		global _transaction

		_transaction.register_document( document )

	meta = {
		'allow_inheritance': True,
		'abstract': True
	}


class ReferredData( mongoengine.EmbeddedDocument ):
	referrer_id = mongoengine.IntField( required=True )

	meta = {
		'allow_inheritance': True
	}


class DriverReferredProgram( ReferredData ):
	used_ind = mongoengine.BooleanField( required=True )
	valid_ind = mongoengine.BooleanField( required=True )


class HostReferredProgram( ReferredData ):
	end_dtm = mongoengine.DateTimeField( required=True )
	collected_cents = mongoengine.IntField()


class ReferralData( mongoengine.EmbeddedDocument ):
	referral_code = mongoengine.UUIDField(
		required=True,
		binary=True,
		default=uuid.uuid4 )

	def get_slug( self ):
		return uuid2slug( self.referral_code )

	meta = {
		'allow_inheritance': True
	}


class DriverReferralProgram( ReferralData ):
	pass


class HostReferralProgram( ReferralData ):
	pass


def hex2slug( val ):
	return (
		val
		.decode( 'hex' )
		.encode( 'base64' )
		.replace( '\n', '' )
		.replace( '+', '-' )
		.replace( '/', '_' )
		.replace( '=', '' ) )


def slug2hex( val ):
	return (
		val
		.ljust( 24, '=' )[0:24]
		.replace( '-', '+' )
		.replace( '_', '/' )
		.decode( 'base64' )
		.encode( 'hex' ) )


def uuid2slug( uuid ):
	return hex2slug( uuid.hex )


def slug2uuid( slug ):
	return uuid.UUID( slug2hex( slug ) )


class AvailableDate( mongoengine.EmbeddedDocument ):
	repeating_ind = mongoengine.StringField( required=True )
	start_dtm = mongoengine.DateTimeField( required=True )
	end_dtm = mongoengine.DateTimeField( required=True )


class PaymentRule( mongoengine.EmbeddedDocument ):
	available_dates = mongoengine.EmbeddedDocumentListField( AvailableDate )
	rule_id = mongoengine.UUIDField( required=True, binary=True )

	def get_slug( self ):
		return uuid2slug( self.rule_id )

	def gen_plan( self, start, end ):
		raise NotImplementedError(
			'Only subclasses with this method implemented should be making '
			'this call...' )

	def to_dict( self ):
		raise NotImplementedError(
			'Only subclasses with this method implemented should be making '
			'this call...' )

	meta = {
		'allow_inheritance': True,
		'indexes': [
			{
				'fields': ['rule_id'],
				'unique': True
			}
		]
	}


class HourlyPaymentRule( PaymentRule ):
	rate_amt = mongoengine.IntField( required=True )
	title_meta = 'Hourly'

	def gen_plan( self, start, end ):
		"""
		Example:

		{
			'cost': 1200,
			'valid': True,
			'type': 'Hourly Payment'
			'rate': 400,
			'dates': [
				{
					'start': 2016-12-25T12:00Z,
					'end': 2016-12-25T15:00Z
				},
				...
			]
		}

		Every gen_plan for an hourly payment is valid. Validity simply means
		whether a requested time can be used with other rules successfully.
		If no rule for this particular option fits within the requested time then
		start and end will be None.
		"""
		ret = { 'valid': True, 'dates': [] }

		if( len( self.available_dates ) ) < 1:
			return ret

		ret.update( { 'type': 'Hourly Payment', 'rate': self.rate_amt } )

		for date in self.available_dates:
			rule_start = date.start_dtm
			rule_end = date.end_dtm

			units = []

			if date.repeating_ind == 'Daily':
				cur_start = rule_start.replace(
					year=start.year,
					month=start.month,
					day=start.day )
				dt = datetime.timedelta( days=1 )
			elif date.repeating_ind == 'Weekly':
				cur_start = (
					rule_start.replace( year=start.year, month=start.month, day=start.day ) +
					datetime.timedelta(
						days=(
							rule_start.isocalendar()[2] -
							start.isocalendar()[2] ) ) )
				dt = datetime.timedelta( weeks=1 )
			elif date.repeating_ind == 'Yearly':
				cur_start = rule_start.replace( year=start.year )
				dt = datetime.timedelta( years=1 )

			if date.repeating_ind == 'None':
				units.append( ( rule_start, rule_end ) )
			else:
				cur_end = cur_start + ( rule_end - rule_start )

				if cur_start > end:
					cur_start -= dt
					cur_end -= dt

				while cur_start < end:
					units.append( ( cur_start, cur_end ) )
					print units
					cur_start += dt
					cur_end += dt

			for unit in units:
				if start >= unit[0] and end <= unit[1]:
					ret['dates'].append( { 'start': start, 'end': end } )
				elif start >= unit[0] and start < unit[1] and end > unit[1]:
					ret['dates'].append( { 'start': start, 'end': unit[1] } )
				elif start < unit[0] and end > unit[0] and end <= unit[1]:
					ret['dates'].append( { 'start': unit[0], 'end': end } )
				elif start < unit[0] and end > unit[1]:
					ret['dates'].append( { 'start': unit[0], 'end': unit[1] } )

		if len( ret['dates'] ) != 0:
			tot = 0
			for unit in ret['dates']:
				tot += self.calc_cost( unit['start'], unit['end'] )

			ret.update( { 'cost': tot } )

		return ret

	def calc_cost( self, start, end ):
		time_fraction = ( end - start ).total_seconds() / 3600.0
		return self.rate_amt * time_fraction

	def to_dict( self ):
		ret = {
			'type': 'Hourly Payment',
			'rate': self.rate_amt,
			'id': hex2slug( self.rule_id.hex ),
			'dates': []
		}

		if len( self.available_dates ) > 0:
			for date in self.available_dates:
				ret['dates'].append( {
					'start': helpers.datetime_naive2aware( date.start_dtm ).isoformat(),
					'end': helpers.datetime_naive2aware( date.end_dtm ).isoformat(),
					'repeating': date.repeating_ind
				} )

		return ret


class FlatratePaymentRule( PaymentRule ):
	rate_amt = mongoengine.IntField( required=True )
	title_meta = 'Flatrate'

	def gen_plan( self, start, end ):
		ret = { 'valid': True, 'dates': [] }

		if( len( self.available_dates ) ) < 1:
			return ret

		ret.update( { 'type': 'Flatrate Payment', 'rate': self.rate_amt } )

		for date in self.available_dates:
			rule_start = date.start_dtm
			rule_end = date.end_dtm

			units = []

			if date.repeating_ind == 'Daily':
				cur_start = rule_start.replace(
					year=start.year,
					month=start.month,
					day=start.day )
				dt = datetime.timedelta( days=1 )
			elif date.repeating_ind == 'Weekly':
				cur_start = (
					rule_start.replace( year=start.year, month=start.month, day=start.day ) +
					datetime.timedelta( days=(
						rule_start.isocalendar()[2] -
						start.isocalendar()[2] ) ) )
				dt = datetime.timedelta( weeks=1 )
			elif date.repeating_ind == 'Yearly':
				cur_start = rule_start.replace( year=start.year )
				dt = datetime.timedelta( years=1 )
			if date.repeating_ind == 'None':
				units.append( ( rule_start, rule_end ) )
			else:
				cur_end = cur_start + ( rule_end - rule_start )

				if cur_start > end:
					cur_start -= dt
					cur_end -= dt

				while cur_start < end:
					units.append( ( cur_start, cur_end ) )
					cur_start += dt
					cur_end += dt

			if len( units ) != 0:
				ret.update( { 'cost': 0 } )

			for unit in units:
				if start <= unit[0] and end >= unit[1]:
					ret['dates'].append( { 'start': unit[0], 'end': unit[1] } )
				elif start > unit[0] and start < unit[1]:
					ret['valid'] = False
					ret['dates'].append(
						{
							'start': start,
							'end': end if end < unit[1] else unit[1]
						} )
				elif end > unit[0] and end < unit[1]:
					ret['valid'] = False
					ret['dates'].append(
						{
							'start': start if start > unit[0] else unit[0],
							'end': end
						} )
				ret['cost'] += self.rate_amt

		return ret

	def to_dict( self ):
		ret = {
			'type': 'Flatrate Payment',
			'rate': self.rate_amt,
			'id': hex2slug( self.rule_id.hex ),
			'dates': []
		}

		if len( self.available_dates ) > 0:
			for date in self.available_dates:
				ret['dates'].append( {
					'start': helpers.datetime_naive2aware( date.start_dtm ).isoformat(),
					'end': helpers.datetime_naive2aware( date.end_dtm ).isoformat(),
					'repeating': date.repeating_ind
				} )

		return ret


def gen_driver_service_fee( amount ):
	cut = amount * 0.05
	fee = int( math.ceil( ( amount + cut + 30 ) / ( 1 - 0.029 ) - amount ) )
	return fee


def gen_host_service_fee( amount ):
	fee = int( math.floor( amount * 0.1 ) )
	return fee


class ListingData( CustomDocument ):
	listing_id = mongoengine.IntField()
	payment_rules = mongoengine.EmbeddedDocumentListField( PaymentRule )

	def gen_plan( self, start, end ):
		r_min = int( ( end - start ).total_seconds() / 60.0 )
		ret = {'valid': True, 'costs': {'rules': []}, 'total_cost': 0}
		for option in self.payment_rules:
			dat = option.gen_plan( start, end )
			if dat['valid'] is False:
				ret['valid'] = False

			del dat['valid']
			if len( dat['dates'] ) != 0:
				for date in dat['dates']:
					r_min -= int( ( date['end'] - date['start'] ).total_seconds() / 60.0 )

				ret['costs']['rules'].append( dat )
				ret['total_cost'] += dat['cost']

				if( r_min == 0 ):
					fee = gen_driver_service_fee( ret['total_cost'] )
					ret['costs'].update( { 'service_fee': fee } )
					ret['total_cost'] += fee
					return ret

		ret['valid'] = False
		return ret

	def gen_fixed_agreement( self, start, end ):
		r_min = int( ( end - start ).total_seconds() / 60.0 )
		agreement = FixedAgreement(
			base_cost_amt=0,
			driver_service_fee_amt=0,
			host_service_fee_amt=0 )
		for option in self.payment_rules:
			dat = option.gen_plan( start, end )

			if dat['valid'] is not True:
				return None

			if len( dat['dates'] ) != 0:
				for date in dat['dates']:
					agreement.rules.append(
						RuleCost(
							rate_amt=dat['rate'],
							cost_amt=dat['cost'],
							type_ind=dat['type'] ) )
					r_min -= int( ( date['end'] - date['start'] ).total_seconds() / 60.0 )
					agreement.rules[-1].dates.append(
						RuleCostDate(
							start_dtm=date['start'],
							end_dtm=date['end'] ) )

				agreement.base_cost_amt += dat['cost']

				if( r_min == 0 ):
					agreement.base_cost_amt = int( agreement.base_cost_amt )
					agreement.driver_service_fee_amt = gen_driver_service_fee(
						agreement.base_cost_amt )
					agreement.host_service_fee_amt = gen_host_service_fee(
						agreement.base_cost_amt )
					return agreement

		return None

	@staticmethod
	def get( listing_id ):
		return ListingData.objects( listing_id=listing_id ).get()

	@staticmethod
	def get_many( listing_ids ):
		return ListingData.objects( listing_id__in=listing_ids )

	def create_rule( self, type, **kwargs ):
		new_kwargs = {}

		if type not in _payment_plans:
			raise tandem_exceptions.RuleInvalidTypeError

		missing = []
		for key, val in _payment_plans[type]['fields'].iteritems():
			if key not in kwargs:
				missing.append( key )

		if len( missing ) != 0:
			raise tandem_exceptions.RuleMissingAttributes( missing )

		for key, val in kwargs.iteritems():
			if key in _payment_plans[type]['fields']:
				new_kwargs[_payment_plans[type]['fields'][key]] = val

		rule = _payment_plans[type]['class'](
			rule_id=uuid.uuid4(),
			**new_kwargs )
		self.payment_rules.append( rule )

		return rule

	def create_date( self, rule_id, start, end, repeating ):
		try:
			rule = (
				rule for
				rule in self.payment_rules if
				rule.rule_id == rule_id ).next()
		except StopIteration:
			raise tandem_exceptions.RuleInvalidId

		missing = []
		if start is None:
			missing.append( 'start' )

		if end is None:
			missing.append( 'end' )

		if len( missing ) != 0:
			raise tandem_exceptions.RuleDateMissingAttributes( missing )

		if repeating is None:
			repeating = 'None'

		if end < start:
			raise tandem_exceptions.RuleDateInvalidTime

		if repeating not in ['None', 'Daily', 'Weekly', 'Yearly']:
			raise tandem_exceptions.RuleDateInvalidRepeating

		if (
			( repeating == 'Daily' and end - start > datetime.timedelta( days=1 ) ) |
			( repeating == 'Weekly' and end - start > datetime.timedelta( days=7 ) ) |
			( repeating == 'Yearly' and end - start > datetime.timedelta( years=1 ) ) ):
			raise tandem_exceptions.RuleDateInvalidRepeating

		plan = self.gen_plan( start, end )
		if len( plan['costs']['rules'] ) != 0:
			raise tandem_exceptions.RuleDateConflictError

		date = AvailableDate( start_dtm=start, end_dtm=end, repeating_ind=repeating )
		rule.available_dates.append( date )

		return date

	meta = {
		'indexes': [
			{
				'fields': ['listing_id'],
				'unique': True
			}
		],
		'collection': 'listing_data'
	}


_payment_plans = {
	'Hourly': {
		'class': HourlyPaymentRule,
		'fields': {
			'rate': 'rate_amt'
		}
	},
	'Flatrate': {
		'class': FlatratePaymentRule,
		'fields': {
			'rate': 'rate_amt'
		}
	}
}


def create_payment_rule( type='', **kwargs ):
	new_kwargs = {}
	for key, val in _payment_plans[type]['fields'].iteritems():
		if key not in kwargs:
			raise ValueError

	for key, val in kwargs.iteritems():
		if key in _payment_plans[type]['fields']:
			new_kwargs[_payment_plans[type]['fields'][key]] = val

	return _payment_plans[type]['class']( rule_id=uuid.uuid4(), **new_kwargs )


class UserData( CustomDocument ):
	user_id = mongoengine.IntField()
	referred_data = mongoengine.EmbeddedDocumentField( ReferredData )
	referral_data = mongoengine.EmbeddedDocumentField( ReferralData )

	def attach_referred_data( self, program ):
		if self.referred_data is not None:
			raise AttributeError( 'User already has referrer data associated.' )

		self.referred_data = program

	def attach_referral_data( self, program ):
		if self.referral_data is not None:
			raise AttributeError( 'User already belongs in a referral program.' )

		self.referral_data = program

	@staticmethod
	def get_users_with_cents():
		return UserData.objects( __raw__=(
			{
				'referred_data.collected_cents':
				{
					'$ne': 0,
					'$exists': 1
				}
			} ) )

	@staticmethod
	def get_referred_users( user_id ):
		return UserData.objects( referred_data__referrer_id=user_id )

	@staticmethod
	def get_user_by_slug( referral_slug ):
		return UserData.objects.get(
			referral_data__referral_code=slug2uuid( referral_slug ) )

	@staticmethod
	def get( user_id ):
		return UserData.objects( user_id=user_id ).get()

	meta = {
		'indexes': [
			{
				'fields': ['user_id'],
				'unique': True
			},
			'referral_data.referral_code',
			'referred_data.referrer_id'
		],
		'collection': 'user_data'
	}


class AgreementData( CustomDocument ):
	agreement_id = mongoengine.IntField()

	@staticmethod
	def get( agreement_id ):
		return AgreementData.objects( agreement_id=agreement_id ).get()

	@staticmethod
	def get_many( agreement_ids ):
		return AgreementData.objects( agreement_id__in=agreement_ids )

	meta = {
		'indexes': [
			{
				'fields': ['agreement_id'],
				'unique': True
			}
		],
		'allow_inheritance': True
	}


class RuleCostDate( mongoengine.EmbeddedDocument ):
	start_dtm = mongoengine.DateTimeField()
	end_dtm = mongoengine.DateTimeField()


class RuleCost( mongoengine.EmbeddedDocument ):
	rate_amt = mongoengine.IntField()
	cost_amt = mongoengine.IntField()
	type_ind = mongoengine.StringField()
	dates = mongoengine.EmbeddedDocumentListField( RuleCostDate )


class FixedAgreement( AgreementData ):
	base_cost_amt = mongoengine.IntField()
	driver_service_fee_amt = mongoengine.IntField()
	host_service_fee_amt = mongoengine.IntField()
	rules = mongoengine.EmbeddedDocumentListField( RuleCost )

	def to_dict( self, to=None ):
		dict = {
			'baseCost': self.base_cost_amt,
			'driverServiceFee': self.driver_service_fee_amt,
			'hostServiceFee': self.host_service_fee_amt,
			'costBreakDown': [{
				'rate': x.rate_amt,
				'cost': x.cost_amt,
				'type': x.type_ind,
				'dates': [{
					'start': helpers.datetime_naive2aware( y.start_dtm ).isoformat(),
					'end': helpers.datetime_naive2aware( y.end_dtm ).isoformat(),
				} for y in x.dates]
			} for x in self.rules]
		}

		if to == 'Host':
			del dict['driverServiceFee']
		elif to == 'Driver':
			del dict['hostServiceFee']

		return dict


mongoengine.signals.post_save.connect(
	CustomDocument.post_save, sender=ListingData )
mongoengine.signals.post_save.connect(
	CustomDocument.post_save, sender=UserData )
mongoengine.signals.post_save.connect(
	CustomDocument.post_save, sender=AgreementData )
mongoengine.signals.post_save.connect(
	CustomDocument.post_save, sender=FixedAgreement )
