# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

import datetime
import uuid
import math

import requests
import bcrypt
import stripe
import geohash
import peewee
import monthdelta
import boto3

import tandem.task.task_dispatcher
import tandem.task.authorization
import tandem.task.email
import tandem.task.exceptions
import tandem.task.fcm
import tandem.task.doc_models
import tandem.tools.constants
import tandem.tools.helpers
import tandem.tools.distributed_logging
import tandem.task.firebase
import tandem.task.error_responses

tandem.task.doc_models.connect()

logger = tandem.tools.distributed_logging.get_logger( 'tandem.worker.task' )

###################
#  Authorization  #
###################


@tandem.task.task_dispatcher.register_task( 'task_authenticate_user', '1' )
@tandem.task.db_models.mysql_access
def task_authenticate_user( access, auth, fb_token ):
	import requests

	if auth is None:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.AuthHeaderMissing
		)

	logger.info( 'Recieved authorization info as: %s', auth )

	auth_split = auth.split()

	if len( auth_split ) != 2:
		return tandem.task.error_responses.tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.InvalidAuthFormat,
			'Basic'
		)

	auth_type = auth_split[0]
	cred = auth_split[1]
	try:
		cred = cred.decode( 'base64' )
	except ( ValueError, TypeError ):
		return tandem.task.error_responses.tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.InvalidAuthFormat,
			'Basic'
		)

	if auth_type != 'Basic':
		return tandem.task.error_responses.tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.InvalidAuthFormat,
			'Basic'
		)

	cred_split = cred.split( ':' )

	if len( cred_split ) != 2:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.InvalidAuthFormat,
			'Basic'
		)

	email = cred_split[0] if cred_split[0] != '' else None
	password = cred_split[1] if cred_split[1] != '' else None

	if ( fb_token is None ) and ( email is None or password is None ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.FieldsMissing )

	email = email.lower()

	try:
		if fb_token is None:
			user = tandem.task.db_models.User.select(
				tandem.task.db_models.User.password_txt,
				*tandem.task.db_models.User.get_preset_items( 'private', chain=True )
			).where( tandem.task.db_models.User.email_ln == email ).get()
		else:
			headers = {
				'Authorization': 'OAuth {}'.format(
					tandem.tools.constants.CONST.FACEBOOK_APP_OAUTH )
			}
			fb_request = requests.get(
				'https://graph.facebook.com/'
				'v2.7/debug_token?input_token={}'.format( fb_token ),
				headers=headers
			)
			if fb_request.status_code is not 200:
				return tandem.task.error_responses.create_error(
					tandem.task.error_responses.error_types.Unauthorized,
					tandem.task.error_responses.error_codes.BadCredentials )

			json = fb_request.json()['data']
			user = tandem.task.db_models.User.select(
				tandem.task.db_models.User.password_txt,
				*tandem.task.db_models.User.get_preset_items( 'private', chain=True )
			).where( tandem.task.db_models.User.fb_id == json['user_id'] ).get()

	except tandem.task.db_models.User.DoesNotExist:
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.Unauthorized,
				tandem.task.error_responses.error_codes.BadCredentials )

	refresh_pass = '{}%{}'.format(
		tandem.tools.constants.CONST.SECRET_KEY,
		user.password_txt )
	self_ref = 'user:{}'.format( user.user_id )

	user_info = user.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		preset='private' )
	user_info.update(
		{
			'refreshToken': tandem.task.authorization.RefreshToken(
			 	self_ref,
			 	'tandem:',
			 	self_ref
			).dumps( refresh_pass ),
			'accessToken': tandem.task.authorization.AccessToken(
				'user',
				self_ref,
				'tandem:',
				self_ref
			).dumps( tandem.tools.constants.CONST.SECRET_KEY, expires=1800 )
		}
	)

	firebase_auth = (
		tandem.task.firebase.FirebaseClient()
		.create_auth_token( user.user_id ) )
	if firebase_auth is None:
		raise Exception
	user_info.update( { "firebaseAuthToken": firebase_auth } )

	if fb_token is not None:
		return user_info, 200

	if user.verify_password( password ):
		return user_info, 200

	return tandem.task.error_responses.create_error(
		tandem.task.error_responses.error_types.Unauthorized,
		tandem.task.error_responses.error_codes.BadCredentials )


@tandem.task.task_dispatcher.register_task( 'task_authenticate_token', '1' )
@tandem.task.db_models.mysql_access
def task_authenticate_token( access, auth ):
	auth_split = auth.split()

	if len( auth_split ) != 2:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.InvalidAuthFormat,
			'Bearer'
		)

	auth_type = auth_split[0]
	cred = auth_split[1]

	if auth_type != 'Bearer':
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.InvalidAuthFormat,
			'Bearer'
		)

	id = tandem.task.authorization.Token.peek( cred )['sub'].split( ':' )[1]

	user = tandem.task.db_models.User.select(
		tandem.task.db_models.User.user_id,
		tandem.task.db_models.User.password_txt
	).where( tandem.task.db_models.User.user_id == id ).get()

	refresh_pass = '{}%{}'.format(
		tandem.tools.constants.CONST.SECRET_KEY,
		user.password_txt )

	try:
		access = tandem.task.authorization.Token.get_token(
			refresh_pass,
			cred,
			['Refresh'] )
	except (
		tandem.task.authorization.exceptions.TokenExpired,
		tandem.task.authorization.exceptions.TokenInvalid
	):
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.Unauthorized,
				tandem.task.error_responses.error_codes.BadToken )

	firebase_auth = (
		tandem.task.firebase.FirebaseClient()
		.create_auth_token( user.user_id ) )
	if firebase_auth is None:
		raise Exception

	return {
		'accessToken': tandem.task.authorization.AccessToken(
			'user',
			access.sub,
			access.iss,
			access.aud
		).dumps( tandem.tools.constants.CONST.SECRET_KEY, expires=1800 ),
		'firebaseAuthToken': firebase_auth
	}, 200


###########
#  Users  #
###########


@tandem.task.task_dispatcher.register_task( 'task_auth_user', '1' )
@tandem.task.db_models.mysql_access
def task_auth_user( access, auth, fb_token ):
	"""Authenticates a email/password combination or facebook id for an auth token.

	Checks if a email is in the database[tandem\`users`] and verifies the password
	matches the server hashed password. In the case a fb_id is supplied then the
	password is not checked. If all is correct a token is returned allowing the
	server to easily authenticate a user in future transactions.

	Args:
		fb_token (str): A facebook provided access token.
		email (str): The user's email.
		password (str): The user's password.

	Returns:
		str: A JWT token that encrypts the valid users id if the user/pass
			combination was valid. If not returns None.

	"""

	if auth is None:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.AuthHeaderMissing
		)

	logger.info( 'Recieved authorization info as: %s', auth )

	auth_split = auth.split()

	if len( auth_split ) != 2:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.InvalidAuthFormat, 'Basic'
		)

	auth_type = auth_split[0]
	cred = auth_split[1]
	try:
		cred = cred.decode( 'base64' )
	except ValueError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.InvalidAuthFormat,
			'Basic'
		)

	if auth_type != 'Basic':
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.InvalidAuthFormat,
			'Basic'
		)

	cred_split = cred.split( ':' )

	if len( cred_split ) != 2:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.InvalidAuthFormat,
			'Basic'
		)

	email = cred_split[0] if cred_split[0] != '' else None
	password = cred_split[1] if cred_split[1] != '' else None

	if ( fb_token is None ) and ( email is None or password is None ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.FieldsMissing )

	email = email.lower()

	try:
		if fb_token is None:
			user = tandem.task.db_models.User.select(
				tandem.task.db_models.User.password_txt,
				*tandem.task.db_models.User.get_preset_items( 'private', chain=True )
			).where( tandem.task.db_models.User.email_ln == email ).get()
		else:
			headers = {
				'Authorization': 'OAuth {}'.format(
					tandem.tools.constants.CONST.FACEBOOK_APP_OAUTH )
			}
			fb_request = requests.get(
				'https://graph.facebook.com/v2.7/'
				'debug_token?input_token={}'.format( fb_token ),
				headers=headers
			)
			if fb_request.status_code is not 200:
				return tandem.task.error_responses.create_error(
					tandem.task.error_responses.error_types.Unauthorized,
					tandem.task.error_responses.error_codes.BadCredentials )

			json = fb_request.json()['data']
			user = tandem.task.db_models.User.select(
				tandem.task.db_models.User.password_txt,
				*tandem.task.db_models.User.get_preset_items( 'private', chain=True )
			).where( tandem.task.db_models.User.fb_id == json['user_id'] ).get()

	except tandem.task.db_models.User.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.BadCredentials
		)

	self_ref = 'user:{}'.format( user.user_id )

	user_info = user.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		preset='private' )
	user_info.update(
		{
			'authToken': tandem.task.authorization.AccessToken(
				'user',
				self_ref,
				'tandem:',
				self_ref
			).dumps( tandem.tools.constants.CONST.SECRET_KEY, expires=1800 )
		}
	)

	firebase_auth = (
		tandem.task.firebase.FirebaseClient()
		.create_auth_token( user.user_id ) )
	if firebase_auth is None:
		raise Exception
	user_info.update( { "firebaseAuthToken": firebase_auth } )

	if fb_token is not None:
		return user_info, 200

	if user.verify_password( password ):
		return user_info, 200

	return tandem.task.error_responses.create_error(
		tandem.task.error_responses.error_types.Unauthorized,
		tandem.task.error_responses.error_codes.BadCredentials )


@tandem.task.task_dispatcher.register_task( 'task_new_user', '1' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_new_user(
	access,
	email,
	fb_id,
	password,
	fb_token,
	first_name,
	last_name,
	referral_slug ):
	"""Creates a new user in the database.

	Inputs a new user into the MySQL database[tandem\`users`] if the email
	is unique.

	Args:
		email (str): The input email.
		fb_id (str): The input facebook user id.
		password (str): The input password.
		first_name (str): The input first name.
		last_name (str): The input last name.

	Returns:
		tuple: tuple containing:

			msg (dictionary): The message that is sent to the client. If the user
			creation is valid then the valid email is returned. If not an empty
			dictionary is returned.

			code (int): The HTTP Code that should be sent to the client. If the user
			creation is valid then 201 is returned. If not 400 is returned.

	"""

	password = password
	email = email.lower()

	if (
		( fb_id is None or fb_token is None or email is None ) and
		( email is None or password is None ) ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.FieldsMissing )

	if( first_name is None or last_name is None ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.FieldsMissing )

	if fb_id is None:
		if password == '':
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.BadRequest,
				tandem.task.error_responses.error_codes.InvalidPassword )

		query = (
			tandem.task.db_models.User
			.select( tandem.task.db_models.User.user_id )
			.where( tandem.task.db_models.User.email_ln == email ) )
	else:
		headers = {
			'Authorization': 'OAuth {}'.format(
				tandem.tools.constants.CONST.FACEBOOK_APP_OAUTH )
		}
		fb_request = requests.get(
			'https://graph.facebook.com/v2.7/\
			debug_token?input_token={}'.format( fb_token ),
			headers=headers
		)
		if fb_request.status_code not in [200, 201]:
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.BadRequest,
				tandem.task.error_responses.error_codes.InvalidAccessToken
			)

		json = fb_request.json()['data']
		if(
			( json['user_id'] != fb_id ) or
			( json['app_id'] != tandem.tools.constants.CONST.FACEBOOK_APP_ID ) ):
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.BadRequest,
				tandem.task.error_responses.error_codes.InvalidAccessToken
			)

		query = (
			tandem.task.db_models.User
			.select(tandem.task.db_models.User.user_id )
			.where( tandem.task.db_models.User.fb_id == fb_id ) )

	if query.exists():
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.UserExists )

	program = None
	if referral_slug is not None:
		referrer = tandem.task.doc_models.UserData.get_user_by_slug( referral_slug )

		if isinstance(
				referrer.referral_data,
				tandem.task.doc_models.DriverReferralProgram ):
			program = tandem.task.doc_models.DriverReferredProgram(
				referrer_id=referrer.user_id,
				used_ind=False,
				valid_ind=False
			)
		elif isinstance(
				referrer.referral_data,
				tandem.task.doc_models.HostReferralProgram ):
			program = tandem.task.doc_models.HostReferredProgram(
				referrer_id=referrer.user_id,
				end_dtm=datetime.datetime.utcnow() + monthdelta.monthdelta( 1 ),
				collected_cents=0
			)
		else:
			raise ValueError( 'ReferralData has an unknown type' )

	user = tandem.task.db_models.User(
		fb_id=fb_id if fb_id is not None else '',
		email_ln=email if email is not None else '',
		account_ind='Driver',
		first_name_ln=first_name,
		last_name_ln=last_name,
		creation_dtm=datetime.datetime.utcnow(),
		password_txt=bcrypt.hashpw(
			password.encode( 'utf-8' ),
			bcrypt.gensalt( 12 )
		)
	)
	user.save()

	user_data = tandem.task.doc_models.UserData( user_id=user.user_id )
	if program:
		user_data.attach_referred_data( program )

	user_data.save()

	refresh_pass = '{}%{}'.format(
		tandem.tools.constants.CONST.SECRET_KEY,
		user.password_txt )

	self_ref = 'user:{}'.format( user.user_id )

	user_info = user.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		preset='private' )
	user_info.update(
		{
			'refreshToken': tandem.task.authorization.RefreshToken(
			 	self_ref,
			 	'tandem:',
			 	self_ref
			).dumps( refresh_pass ),
			'accessToken': tandem.task.authorization.AccessToken(
				'user',
				self_ref,
				'tandem:',
				self_ref
			).dumps( tandem.tools.constants.CONST.SECRET_KEY, expires=1800 ),
			'authToken': tandem.task.authorization.AccessToken(
				'user',
				self_ref,
				'tandem:',
				self_ref
			).dumps( tandem.tools.constants.CONST.SECRET_KEY, expires=1800 )
		} )

	firebase_auth = (
		tandem.task.firebase.FirebaseClient()
		.create_auth_token(user.user_id) )
	if firebase_auth is None:
		raise Exception( 'Unable to create a firebase token homie' )
	user_info.update( { "firebaseAuthToken": firebase_auth } )

	return user_info, 201


@tandem.task.task_dispatcher.register_task( 'task_update_user', '1' )
@tandem.task.db_models.mysql_access
def task_update_user( access, user_id, patches ):
	""" Updates a user with JSON Patch.

	A user can only update their own profile and this is verified with an
	auth token. Patches only support replace and test operations and can't
	be used on any sub collections.

	Args:
		claim (Claim): The claim the user has supplied.
		user_id (int): The user to be affected by the request.
		patch (list): The collection of JSON Patch operations to be applied.

	Returns:
		tuple::

			dict: A dictionary of key and value attributes affected by the update
				if successful.

			int: 200 if successful.

	"""

	try:
		patch = tandem.task.db_models.User.Patch( patches )
	except tandem.task.exceptions.PatchAttributeError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidPatch )
	except tandem.task.exceptions.PatchValueError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions
		)

	if not patch.verify_patch( access, 'user.patch', user_id ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions
		)

	try:
		user = tandem.task.db_models.User.select(
			tandem.task.db_models.User.user_id,
			*patch.select_list()
		).where( tandem.task.db_models.User.user_id == user_id ).for_update().get()
	except tandem.task.db_models.User.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.NotFound,
			tandem.task.error_responses.error_codes.UserDoesNotExist )

	try:
		patch.apply_patch( user )
	except tandem.task.exceptions.PatchTestError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.PatchFailedTest )

	user.save()

	ret = patch.dict_list()
	if 'password_txt' in ret:
		del ret['password_txt']

	print ret

	return user.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		**ret ), 200


@tandem.task.task_dispatcher.register_task( 'task_get_user', '1', 'user.get' )
@tandem.task.db_models.mysql_access
def task_get_user( access, user_id ):
	"""Returns id, email, and password of user with id.

	When client wants to grab the information of another user they call this to
	grab all their info.

	To Do:
		Not return password and other sensitive information.

	Args:
		id (int): The user id to grab.

	Returns:
		tuple: tuple containing:

			msg (dictionary): A dictionary of the users id, email, and password if
				the id is valid. If not returns empty.

			code (int): The HTTP Code. Returns 200 if the user is found with id.
				If not returns 404.

	"""

	if user_id is -1:
		users = []
		my_user = (
			tandem.task.db_models.User
			.get_preset_query( 'private' )
			.where( tandem.task.db_models.User.user_id == access.get_subject_id() ) )
		other_users = (
			tandem.task.db_models.User
			.get_preset_query( 'private' )
			.where( tandem.task.db_models.User.user_id != access.get_subject_id() ) )
		for user in ( my_user | other_users ):
			preset = 'private' if user.user_id == access.get_subject_id() else 'public'
			users.append(
				user.to_dict(
					tandem.tools.constants.CONST.DICT_FORMAT_JSON,
					preset=preset
				)
			)
		return users, 200
	elif user_id is 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidUserId )

	preset = 'private' if user_id == access.get_subject_id() else 'public'

	try:
		user = (
			tandem.task.db_models.User
			.get_preset_query( preset )
			.where( tandem.task.db_models.User.user_id == user_id )
			.get() )
	except tandem.task.db_models.User.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.NotFound,
			tandem.task.error_responses.error_codes.UserDoesNotExist )

	return user.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		preset=preset
	), 200


@tandem.task.task_dispatcher.register_task( 'task_get_user_listing', '1' )
@tandem.task.db_models.mysql_access
def task_get_user_listing( access, user_id, listing_id ):
	if user_id <= 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidUserId )

	if not (
			tandem.task.db_models.User.select()
			.where( tandem.task.db_models.User.user_id == user_id )
			.exists() ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.NotFound,
			tandem.task.error_responses.error_codes.UserDoesNotExist )

	if listing_id is -1:
		listings = []
		for listing in tandem.task.db_models.Listing.get_exists(
			'public-w/o-host',
			[tandem.task.db_models.Listing.host == user_id] ):
			listings.append(
				listing.to_dict(
					tandem.tools.constants.CONST.DICT_FORMAT_JSON,
					preset='public-w/o-host' ) )
		return listings, 200
	elif listing_id is 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidListingId )

	try:
		listing = tandem.task.db_models.Listing.get_exists(
			'public-w/o-host',
			[
				tandem.task.db_models.Listing.host == user_id,
				tandem.task.db_models.Listing.listing_id == listing_id
			],
			get=True )
		return listing.to_dict(
			tandem.tools.constants.CONST.DICT_FORMAT_JSON,
			preset='public-w/o-host'
		), 200
	except tandem.task.db_models.Listing.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.NotFound,
			tandem.task.error_responses.error_codes.ListingDoesNotExist )


@tandem.task.task_dispatcher.register_task(
	'task_promote_user',
	'1',
	'user.promote' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_promote_user(
	access,
	phone_number,
	country,
	administrative_area,
	sub_administrative_area,
	locality,
	dependent_locality,
	postal_code,
	throughfare,
	premise,
	sub_premise ):

	user = tandem.task.db_models.User.select(
		tandem.task.db_models.User.user_id,
		tandem.task.db_models.User.account_ind,
		tandem.task.db_models.User.email_ln
	).where( tandem.task.db_models.User.user_id == access.get_subject_id() ).get()

	if user.account_ind != 'Driver':
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserNotDriver )

	if phone_number is None:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.FieldsMissing )

	if phone_number[0] != '+':
		phone_number = ''.join( ['+1', phone_number] )

	user.phone_number = phone_number
	if user.validate_phone_number() is False:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidPhoneNumber )

	connect_account = stripe.Account.create(
		managed=False,
		country=country,
		email=user.email_ln
	)

	user.account_ind = 'Host'
	user.stripe_connect_id = connect_account['id']
	user.save()

	dict = user.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		account_ind=True,
		phone_number=True
	)
	return dict, 200


@tandem.task.task_dispatcher.register_task( 'task_user_reset_password', '1' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_user_reset_password( access, input_email ):
	if input_email is None:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.FieldsMissing )

	try:
		user = ( tandem.task.db_models.User.select(
			tandem.task.db_models.User.last_name_ln,
			tandem.task.db_models.User.first_name_ln,
			tandem.task.db_models.User.email_ln,
			tandem.task.db_models.User.user_id )
			.where( tandem.task.db_models.User.email_ln == input_email.lower() )
			.get() )
	except tandem.task.db_models.User.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.UserDoesNotExist )

	self_ref = 'user:{}'.format( user.user_id )

	token = tandem.task.authorization.SingleUseAccessToken(
		'user.patch.password_txt',
		self_ref,
		'tandem:',
		self_ref
	).dumps( tandem.tools.constants.CONST.SECRET_KEY, 172800 )

	name = '{} {}'.format( user.first_name_ln, user.last_name_ln )
	link = (
		'https://www.tandemparking.co/'
		'reset_password?token={}&user_id={}'.format( token, user.user_id ) )

	client = tandem.task.email.EmailClient(
		tandem.tools.constants.CONST.GMAIL_USER,
		tandem.tools.constants.CONST.GMAIL_PASS
	)
	client.send_template(
		'noreply@tandemparking.co',
		user.email_ln,
		'Password Reset Requested',
		'password_reset',
		name=name,
		link=link
	)

	ret = user.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		last_name_ln=True,
		first_name_ln=True,
		email_ln=True
	)
	return ret, 201


##############
#  Referral  #
##############


@tandem.task.task_dispatcher.register_task(
	'task_get_user_referral_code',
	'1',
	'user.referral.code' )
@tandem.task.db_models.mysql_access
def task_get_user_referral_code( access ):
	user_data = tandem.task.doc_models.UserData.get( access.get_subject_id() )

	if user_data.referral_data is None:
		if not tandem.task.db_models.User.select().where(
			tandem.task.db_models.User.user_id == access.get_subject_id(),
			tandem.task.db_models.User.account_ind == 'Host' ).exists():
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.BadRequest,
				tandem.task.error_responses.error_codes.InvalidReferralProgram
			)

		user_data.attach_referral_data(
			tandem.task.doc_models.HostReferralProgram() )

		user_data.save()

	return user_data.referral_data.get_slug(), 200


###################
#  User Vehicles  #
###################


@tandem.task.task_dispatcher.register_task(
	'task_new_user_vehicle',
	'1',
	'user.vehicles.post' )
@tandem.task.db_models.mysql_access
def task_new_user_vehicle( access, license_plate, color, make, model, year ):
	if (
		license_plate is None or
		color is None or
		make is None or
		model is None or
		year is None ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.FieldsMissing )

	vehicle = tandem.task.db_models.Vehicle(
		owner=access.get_subject_id(),
		license_plate_ln=license_plate,
		color_ln=color,
		make_ln=make,
		model_ln=model,
		year_ln=year )
	vehicle.save()

	return vehicle.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		preset='public'
	), 201


@tandem.task.task_dispatcher.register_task(
	'task_get_user_vehicles',
	'1',
	'user.vehicles.get' )
@tandem.task.db_models.mysql_access
def task_get_user_vehicles( access ):
	vehicles = (
		tandem.task.db_models.Vehicle
		.get_preset_query( 'public' )
		.where( tandem.task.db_models.Vehicle.owner == access.get_subject_id() ) )
	vehicle_list = []

	for vehicle in vehicles:
		vehicle_list.append(
			vehicle.to_dict(
				tandem.tools.constants.CONST.DICT_FORMAT_JSON,
				preset='public' ) )

	return vehicle_list, 200


@tandem.task.task_dispatcher.register_task( 'task_update_user_vehicle', '1' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_update_user_vehicle( access, vehicle_id, patches ):
	if patches is None or vehicle_id is None:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.FieldsMissing )

	try:
		patch = tandem.task.db_models.Vehicle.Patch( patches )
	except tandem.task.exceptions.PatchAttributeError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidPatch )
	except tandem.task.exceptions.PatchValueError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions
		)

	try:
		vehicle = (
			tandem.task.db_models.Vehicle.select(
				tandem.task.db_models.Vehicle.vehicle_id,
				tandem.task.db_models.Vehicle.owner,
				*patch.select_list() )
			.where( tandem.task.db_models.Vehicle.vehicle_id == vehicle_id )
			.for_update()
			.get() )
	except tandem.task.db_models.Vehicle.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.VehicleDoesNotExist
		)

	if not patch.verify_claim( access, 'user.vehicles.patch', vehicle.owner_id ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions
		)

	try:
		patch.apply_patch( vehicle )
	except tandem.task.exceptions.PatchTestError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.PatchFailedTest
		)

	vehicle.save()

	return vehicle.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		**patch.dict_list()
	), 200


###################
#  User Payments  #
###################


@tandem.task.task_dispatcher.register_task(
	'task_new_user_payment_method',
	'1',
	'user.payments.post' )
@tandem.task.db_models.mysql_access
def task_new_user_payment_method( access, token ):
	if token is None:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.FieldsMissing )

	user = tandem.task.db_models.User.select(
		tandem.task.db_models.User.user_id,
		tandem.task.db_models.User.stripe_customer_id,
		tandem.task.db_models.User.first_name_ln,
		tandem.task.db_models.User.last_name_ln
	).where( tandem.task.db_models.User.user_id == access.get_subject_id() ).get()

	if user.stripe_customer_id == '':
		try:
			customer = stripe.Customer.create(
				source=token,
				description='{} {} - id {}'.format(
					user.first_name_ln,
					user.last_name_ln,
					user.user_id
				)
			)
		except stripe.error.InvalidRequestError:
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.BadRequest,
				tandem.task.error_responses.error_codes.InvalidStripeToken
			)

		user.stripe_customer_id = customer.id
		user.save()
		card_id = customer['sources']['data'][0].id
	else:
		customer = stripe.Customer.retrieve( user.stripe_customer_id )
		try:
			card = customer.sources.create( source=token )
		except stripe.error.InvalidRequestError:
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.BadRequest,
				tandem.task.error_responses.error_codes.InvalidStripeToken
			)

		card_id = card.id

	return card_id, 201


@tandem.task.task_dispatcher.register_task(
	'task_get_user_payment_methods',
	'1',
	'user.payments.get' )
@tandem.task.db_models.mysql_access
def task_get_user_payment_methods( access ):
	user = (
		tandem.task.db_models.User
		.select( tandem.task.db_models.User.stripe_customer_id )
		.where( tandem.task.db_models.User.user_id == access.get_subject_id() )
		.get() )

	if user.stripe_customer_id == '':
		return [], 200

	customer = stripe.Customer.retrieve( user.stripe_customer_id )
	card_list = customer.sources.all( object="card" )

	return {
		'defaultCardId': customer.default_source,
		'cards': card_list['data']
	}, 200


@tandem.task.task_dispatcher.register_task(
	'task_get_user_default_payment_method',
	'1',
	'user.payments.default.get' )
@tandem.task.db_models.mysql_access
def task_get_user_default_payment_method( access ):
	user = (
		tandem.task.db_models.User
		.select( tandem.task.db_models.User.stripe_customer_id )
		.where( tandem.task.db_models.User.user_id == access.get_subject_id() )
		.get() )

	if user.stripe_customer_id == '':
		# if user has not registered with stripe, there is no default payment method
		return {}, 200

	try:
		customer = stripe.Customer.retrieve( user.stripe_customer_id )
		card_id = customer.default_source
		if card_id is None:
			# user has no default payment method
			return {}, 200

		default_payment_method = customer.sources.retrieve( card_id )
	except stripe.error.StripeError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.InternalServerError,
			tandem.task.error_responses.error_codes.SomethingWentWrong
		)

	return default_payment_method, 200


@tandem.task.task_dispatcher.register_task(
	'task_update_user_default_payment_method',
	'1',
	'user.payments.default.patch'
)
@tandem.task.db_models.mysql_access
def task_update_user_default_payment_method( access, card_id ):
	user = (
		tandem.task.db_models.User
		.select( tandem.task.db_models.User.stripe_customer_id )
		.where( tandem.task.db_models.User.user_id == access.get_subject_id() )
		.get() )

	if user.stripe_customer_id == '':
		# if user has not registered with stripe, bail early
		return {}, 200

	try:
		customer = stripe.Customer.retrieve( user.stripe_customer_id )
		customer.default_source = card_id
		customer.save()
	except stripe.error.StripeError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.InternalServerError,
			tandem.task.error_responses.error_codes.SomethingWentWrong
		)

	return { 'cardId': customer.default_source }, 200


@tandem.task.task_dispatcher.register_task(
	'task_delete_user_payment_method',
	'1',
	'user.payments.delete' )
@tandem.task.db_models.mysql_access
def task_delete_user_payment_method( access, card_id ):
	user = (
		tandem.task.db_models.User.select(
			tandem.task.db_models.User.stripe_customer_id )
		.where( tandem.task.db_models.User.user_id == access.get_subject_id() )
		.get() )

	customer = stripe.Customer.retrieve( user.stripe_customer_id )
	result = customer.sources.retrieve( card_id ).delete()

	return result, 200


#####################
#  Users - Devices  #
#####################


@tandem.task.task_dispatcher.register_task(
	'task_user_add_device',
	'1',
	'user.devices.post' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_user_add_device( access, token ):
	if token is None:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.FieldsMissing )

	user = tandem.task.db_models.User.select(
		tandem.task.db_models.User.user_id,
		tandem.task.db_models.User.fcm_device_group_key,
		tandem.task.db_models.User.fcm_device_group_name,
		tandem.task.db_models.User.email_ln
	).where( tandem.task.db_models.User.user_id == access.get_subject_id() ).get()

	if (
			tandem.task.db_models.FCMDevice
			.select( tandem.task.db_models.FCMDevice.fcm_id )
			.where(
				( tandem.task.db_models.FCMDevice.fcm_id == token ) &
				( tandem.task.db_models.FCMDevice.user == user.user_id ) )
			.exists() ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.TokenAlreadyRegistered )

	fcm_client = tandem.task.fcm.FCMClient(
		api_key=tandem.tools.constants.CONST.FCM_API_KEY,
		sender_id=tandem.tools.constants.CONST.FCM_SENDER_ID )

	device_group_name = user.email_ln + datetime.datetime.utcnow().isoformat()
	if user.fcm_device_group_key == '':
		try:
			result = fcm_client.device_group_create( device_group_name, [token] )
		except tandem.task.fcm.exceptions.FCMBadRequestError:
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.BadRequest,
				tandem.task.error_responses.error_codes.InvalidDeviceToken
			)

		user.update(
			fcm_device_group_key=result['notification_key'],
			fcm_device_group_name=device_group_name
		).where( tandem.task.db_models.User.user_id == user.user_id ).execute()
	else:
		try:
			fcm_client.device_group_add(
				user.fcm_device_group_name,
				user.fcm_device_group_key, [token]
			)
		except tandem.task.fcm.exceptions.FCMBadRequestError:
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.BadRequest,
				tandem.task.error_responses.error_codes.InvalidDeviceToken
			)

	tandem.task.db_models.FCMDevice(
		user=user.user_id,
		fcm_id=token ).save( force_insert=True )

	return token, 200


@tandem.task.task_dispatcher.register_task(
	'task_user_delete_device',
	'1',
	'user.devices.delete' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_user_delete_device( access, token ):
	if token is None:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.FieldsMissing )

	user = tandem.task.db_models.User.select(
		tandem.task.db_models.User.user_id,
		tandem.task.db_models.User.fcm_device_group_key,
		tandem.task.db_models.User.fcm_device_group_name,
		tandem.task.db_models.User.email_ln
	).where( tandem.task.db_models.User.user_id == access.get_subject_id() ).get()

	exists = tandem.task.db_models.FCMDevice.select().where(
		( tandem.task.db_models.FCMDevice.fcm_id == token ) &
		( tandem.task.db_models.FCMDevice.user == user.user_id )
	).exists()

	if not exists or user.fcm_device_group_key == '':
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.TokenNotRegistered
		)

	try:
		ret = tandem.task.db_models.FCMDevice.hard_delete(
			[token],
			user.user_id,
			user.fcm_device_group_name,
			user.fcm_device_group_key
		)
	except tandem.task.fcm.exceptions.FCMBadRequestError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidDeviceToken )

	return ret, 200


####################
#  Users - Images  #
####################


@tandem.task.db_models.mysql_access
@tandem.task.task_dispatcher.register_task(
	'task_user_add_image',
	'1',
	'user.images.post' )
def task_user_add_image( access, file_type ):
	s3client = boto3.client( 's3' )

	try:
		old_image = (
			tandem.task.db_models.UserImage
			.select(
				tandem.task.db_models.UserImage.user_image_id,
				tandem.task.db_models.UserImage.key_ln )
			.where( tandem.task.db_models.UserImage.user == access.get_subject_id() )
			.get() )
		s3client.delete_object(
			Bucket='tandem-user-images',
			Key=old_image.key_ln )
		old_image.delete_instance()
	except tandem.task.db_models.UserImage.DoesNotExist:
		pass

	unique_id = ''.join( ['img_', uuid.uuid4().hex] )
	presigned_post = s3client.generate_presigned_post(
		Bucket='tandem-user-images',
		Key=unique_id,
		Fields={
			'acl': 'public-read',
			'Content-Type': file_type },
		Conditions=[
			{ 'acl': 'public-read' },
			{ 'Content-Type': file_type },
			['content-length-range', 0, 5242880]
		],
		ExpiresIn=300 )

	image = tandem.task.db_models.UserImage(
		user=access.get_subject_id(),
		key_ln=unique_id )
	image.save()

	url = 'https://{}.s3.amazonaws.com/{}'

	return {
		'postData': presigned_post,
		'finalUrl': url.format( 'tandem-user-images', unique_id )
	}, 200


##############
#  Listings  #
##############


@tandem.task.task_dispatcher.register_task(
	'task_new_listing',
	'1',
	'user.listings.post' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_new_listing(
	access,
	description,
	rules,
	country,
	administrative_area,
	sub_administrative_area,
	locality,
	dependent_locality,
	postal_code,
	throughfare,
	premise,
	sub_premise ):
	"""Creates a new listing in the database.

	Args:
		current_user (dict): The information of user making the request.

	Returns:
		tuple: tuple containing:

			msg (dictionary): The message that is sent to the client. If the
				listing creation is valid then relevant information is returned.
				If not an empty dictionary is returned.

			code (int): The HTTP Code that should be sent to the client. If the
				listing creation is valid then 201 is returned. If not 400 is
				returned.

	"""
	user = (
		tandem.task.db_models.User
		.select(
			tandem.task.db_models.User.user_id,
			tandem.task.db_models.User.account_ind )
		.where( tandem.task.db_models.User.user_id == access.get_subject_id() )
		.get() )

	if user.account_ind != "Host":
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserNotHost )

	address = tandem.task.db_models.Address(
		country_sn=country if country is not None else '',
		administrative_sn=(
			administrative_area if
			administrative_area is not None else
			''
		),
		sub_administrative_ln=(
			sub_administrative_area if
			sub_administrative_area is not None else
			''
		),
		locality_ln=locality if locality is not None else '',
		dependent_locality_ln=(
			dependent_locality if
			dependent_locality is not None else
			''
		),
		postal_code_ln=postal_code if postal_code is not None else '',
		throughfare_ln=throughfare if throughfare is not None else '',
		premise_ln=premise if premise is not None else '',
		sub_premise_ln=sub_premise if sub_premise is not None else '' )

	if not address.check():
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.IncompleteAddress )

	if not address.verify():
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidAddress )

	address.save()

	geohash_pos = geohash.encode( address.latitude_pos, address.longitude_pos, 6 )

	listing = tandem.task.db_models.Listing(
		host=user.user_id,
		address=address.address_id,
		description_txt=description,
		status_ind='Open',
		geohash_pos=geohash_pos
	)

	listing.save()
	listing.address = address

	listing_data = tandem.task.doc_models.ListingData(
		listing_id=listing.listing_id )

	if rules is None:
		rules = []

	for rule in rules:
		try:
			type = rule.pop( 'type' )
		except KeyError:
			type = None

		try:
			new_rule = listing_data.create_rule( type, **rule )
			rule.update( { 'type': type } )
		except tandem.task.exceptions.RuleInvalidTypeError:
			rule.update(
				{
					'status':
					{
						'accepted': False,
						'reason':
						'Rule type is not valid'
					}
				}
			)
			continue
		except tandem.task.exceptions.RuleMissingAttributes as e:
			rule.update(
				{
					'status':
					{
						'accepted': False,
						'reason': 'Rule missing parameters {}'.format( ' '.join( e.missing ) )
					}
				}
			)
			continue

		rule.update( { 'status': { 'accepted': True }, 'id': new_rule.get_slug() } )

		for date in rule.get( 'dates', [] ):
			try:
				start = tandem.tools.helpers.datetime_str2naive(
					date.get( 'start' )
				)
				date['start'] = tandem.tools.helpers.datetime_naive2aware( start ).isoformat()
				end = tandem.tools.helpers.datetime_str2naive(
					date.get( 'end' )
				)
				date['end'] = tandem.tools.helpers.datetime_naive2aware( end ).isoformat()
			except ValueError:
				date.update(
					{
						'status':
						{
							'accepted': False,
							'reason':
							'Time field not in correct yyyy-mm-ddThh:mmZ format'
						}
					}
				)
				continue

			try:
				listing_data.create_date(
					new_rule.rule_id,
					start,
					end,
					date.get( 'repeating' )
				)
			except tandem.task.exceptions.RuleDateMissingAttributes as e:
				date.update(
					{
						'status':
						{
							'accepted': False,
							'reason': 'Some fields are missing {}'.format( ' '.join( e.missing ) )
						}
					}
				)
				continue
			except tandem.task.exceptions.RuleDateInvalidTime:
				date.update(
					{
						'status':
						{
							'accepted': False,
							'reason': 'Times are invalid or ill-formed'
						}
					}
				)
				continue
			except tandem.task.exceptions.RuleDateInvalidRepeating:
				date.update(
					{
						'status':
						{
							'accepted': False,
							'reason': 'Repeating is invalid or doesn\'t match time length'
						}
					}
				)
				continue
			except tandem.task.exceptions.RuleDateConflictError:
				date.update(
					{
						'status':
						{
							'accepted': False,
							'reason': 'Conflicts with existing date'
						}
					}
				)
				continue

			date.update( { 'status': { 'accepted': True } } )

	listing_data.save()

	return listing.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		preset='public'
	), 201


@tandem.task.task_dispatcher.register_task( 'task_search_listings', '1' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_search_listings( access, latitude, longitude, radius, filters ):
	price_where_clause = []
	price_order_clause = []
	start = None
	end = None

	eq_comp = None
	neq_comp = None
	asc = False
	desc = False
	gt_comp = None
	lt_comp = None
	gte_comp = None
	lte_comp = None

	if filters is not None:
		filters = filters.split( ',' )
		for filter in filters:
			if '==' in filter and filter.startswith( 'price', 0, 5 ):
				eq_comp = int( filter[7:] )
			elif '!=' in filter and filter.startswith( 'price', 0, 5 ):
				neq_comp = int( filter[7:] )
			elif '<<' in filter and filter.startswith( 'price', 0, 5 ):
				desc = True
			elif '>>' in filter and filter.startswith( 'price', 0, 5 ):
				asc = True
			elif '>=' in filter and filter.startswith( 'price', 0, 5 ):
				gte_comp = int( filter[7:] )
			elif '>' in filter and filter.startswith( 'price', 0, 5 ):
				gt_comp = int( filter[6:] )
			elif '<=' in filter and filter.startswith( 'price', 0, 5 ):
				lte_comp = int( filter[7:] )
			elif '<' in filter and filter.startswith( 'price', 0, 5 ):
				lt_comp = int( filter[6:] )

			elif '==' in filter and filter.startswith( 'start', 0, 5 ):
				try:
					start = tandem.tools.helpers.datetime_str2naive( filter[7:] )
				except ValueError:
					start = None

			elif '==' in filter and filter.startswith( 'end', 0, 3 ):
				try:
					end = tandem.tools.helpers.datetime_str2naive( filter[5:] )
				except ValueError:
					end = None

	if asc or desc:
		pass

	geohash_origin = geohash.encode( float( latitude ), float( longitude ), 6 )

	radius = float( radius )
	radius = radius if radius < 50.0 else 50
	lat_cell_count = int( math.ceil( radius / 0.61 ) ) - 1
	long_cell_count = int( math.ceil( radius / 1.22 ) ) - 1

	cell_search_bin = []

	neighbors = geohash.neighbors( geohash_origin )

	cell_search_bin.append(
		[
			neighbors['northwest'],
			neighbors['north'],
			neighbors['northeast']
		]
	)
	cell_search_bin.append(
		[
			neighbors['west'],
			geohash_origin,
			neighbors['east']
		]
	)
	cell_search_bin.append(
		[
			neighbors['southwest'],
			neighbors['south'],
			neighbors['southeast']
		]
	)

	for i in range( lat_cell_count ):
		top_scouted = geohash.neighbors( cell_search_bin[0][1] )
		cell_search_bin.insert(
			0,
			[
				top_scouted['northwest'],
				top_scouted['north'],
				top_scouted['northeast']
			]
		)

		bot_scouted = geohash.neighbors( cell_search_bin[-1][1] )
		cell_search_bin.append(
			[
				bot_scouted['southwest'],
				bot_scouted['south'],
				bot_scouted['southeast']
			]
		)

	for i in range( long_cell_count ):
		northsouth_range = range( len( cell_search_bin ) )
		for pos in northsouth_range[::2]:
			west_partial_scouted = geohash.neighbors( cell_search_bin[pos][0] )
			east_partial_scouted = geohash.neighbors( cell_search_bin[pos][-1] )
			cell_search_bin[pos].insert( 0, west_partial_scouted['west'] )
			cell_search_bin[pos].append( east_partial_scouted['east'] )
			if pos + 1 in northsouth_range:
				cell_search_bin[pos + 1].insert( 0, west_partial_scouted['southwest'] )
				cell_search_bin[pos + 1].append( east_partial_scouted['southeast'] )

	final_cells = []
	for group in cell_search_bin:
		final_cells.extend( group )

	where_clause = [( tandem.task.db_models.Listing.geohash_pos << final_cells )]
	where_clause += price_where_clause

	listings = tandem.task.db_models.Listing.get_open(
		'system',
		where_clause,
		price_order_clause )

	listings_dict = []
	if start is not None and end is not None:
		available, listings = tandem.task.db_models.Listing.many_available(
			listings,
			start,
			end )
		for listing in listings:
			if available[listing.listing_id] is not None:
				if eq_comp is not None:
					if available[listing.listing_id].base_cost_amt != eq_comp:
						continue
				if neq_comp is not None:
					if available[listing.listing_id].base_cost_amt == neq_comp:
						continue
				if gt_comp is not None:
					if available[listing.listing_id].base_cost_amt <= gt_comp:
						continue
				if lt_comp is not None:
					if available[listing.listing_id].base_cost_amt >= lt_comp:
						continue
				if gte_comp is not None:
					if available[listing.listing_id].base_cost_amt < gte_comp:
						continue
				if lte_comp is not None:
					if available[listing.listing_id].base_cost_amt > lte_comp:
						continue
				listings_dict.append( listing.to_dict(
					tandem.tools.constants.CONST.DICT_FORMAT_JSON,
					preset='public'
				) )
				listings_dict[-1].update(
					{
						'cost': available[listing.listing_id].to_dict( 'Driver' )
					}
				)
	else:
		for listing in listings:
			listings_dict.append( listing.to_dict(
				tandem.tools.constants.CONST.DICT_FORMAT_JSON,
				preset='public'
			) )

	return listings_dict, 200


@tandem.task.task_dispatcher.register_task( 'task_update_listing', '1' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_update_listing( access, listing_id, patches ):
	if patches is None or listing_id is None:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Unauthorized,
			tandem.task.error_responses.error_codes.FieldsMissing )

	try:
		patch = tandem.task.db_models.Listing.Patch( patches )
	except tandem.task.exceptions.PatchAttributeError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidPatch )
	except tandem.task.exceptions.PatchValueError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions
		)

	try:
		listing = (
			tandem.task.db_models.Listing.select(
				tandem.task.db_models.Listing.listing_id,
				tandem.task.db_models.Listing.host,
				*patch.select_list() )
			.where( tandem.task.db_models.Listing.listing_id == listing_id )
			.for_update()
			.get() )
	except tandem.task.db_models.Listing.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.VehicleDoesNotExist
		)

	if not patch.verify_patch( access, 'user.vehicles.patch', listing.host_id ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions
		)

	try:
		patch.apply_patch( listing )
	except tandem.task.exceptions.PatchTestError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.PatchFailedTest
		)

	listing.save()

	return listing.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		**patch.dict_list()
	), 200


@tandem.task.task_dispatcher.register_task(
	'task_get_listing',
	'1',
	'user.listing.get' )
@tandem.task.db_models.mysql_access
def task_get_listing( access, listing_id ):
	"""Returns listing_id, host_id, and status_ind of listing with id.

	When client wants to grab the information of a listing they call this.

	To Do:
		Evaluate what information to release.

	Args:
		id (int): The listing id to grab.

	Returns:
		tuple: tuple containing:

			msg (dictionary): A dictionary of the listing_id, host_id, and status_ind
				if the id is valid. If not returns empty.

			code (int): The HTTP Code. Returns 200 if the listing exists. If not
				returns 404.

	"""

	if listing_id is -1:
		listings = []
		for listing in tandem.task.db_models.Listing.get_exists( 'public' ):
			listings.append( listing.to_dict(
				tandem.tools.constants.CONST.DICT_FORMAT_JSON,
				preset='public' ) )
		return listings, 200
	elif listing_id is 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidListingId )

	try:
		listing = tandem.task.db_models.Listing.get_exists(
			'public',
			[tandem.task.db_models.Listing.listing_id == listing_id],
			get=True )
		return listing.to_dict(
			tandem.tools.constants.CONST.DICT_FORMAT_JSON,
			preset='public' ), 200
	except tandem.task.db_models.Listing.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.NotFound,
			tandem.task.error_responses.error_codes.ListingDoesNotExist )


@tandem.task.task_dispatcher.register_task(
	'task_delete_listing',
	'1',
	'user.listing.delete' )
@tandem.task.db_models.mysql_access
def task_delete_listing( access, listing_id ):
	"""Deletes a listing of listing_id

	Args:
		lisitng_id (int): The listing id to delete

	Returns:
		tuple: tuple containing:

			msg (dictionary): A dictionary of the deleted listing.

			code (int): The HTTP Code. Returns 200 if the listing is deleted properly.
				If not returns 404.

	"""

	if not tandem.task.db_models.Listing.get_exists(
			'delete',
			[
				tandem.task.db_models.Listing.listing_id == listing_id,
				tandem.task.db_models.Listing.host == access.get_subject_id()
			],
			exists=True ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions )

	try:
		tandem.task.db_models.Listing.soft_delete( listing_id )
	except tandem.task.exceptions.ActiveAgreementsError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.ListingHasActiveAgreements )

	return listing_id, 200


#######################
#  Listings - Images  #
#######################


@tandem.task.task_dispatcher.register_task(
	'task_listing_add_image',
	'1',
	'user.listing.images.post' )
def task_listing_add_image( access, listing_id, file_types ):
	if not tandem.task.db_models.Listing.get_exists(
			'public',
			[
				tandem.task.db_models.Listing.host == access.get_subject_id(),
				tandem.task.db_models.Listing.listing_id == listing_id
			],
			exists=True ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions )

	ret_list = []

	for file_type in file_types:
		unique_id = ''.join( ['img_', uuid.uuid4().hex] )
		s3client = boto3.client( 's3' )
		presigned_post = s3client.generate_presigned_post(
			Bucket='tandem-listing-images',
			Key=unique_id,
			Fields={
				'acl': 'public-read',
				'Content-Type': file_type },
			Conditions=[
				{ 'acl': 'public-read' },
				{ 'Content-Type': file_type },
				['content-length-range', 0, 5242880] ],
			ExpiresIn=300 )

		image = tandem.task.db_models.ListingImage(
			listing=listing_id,
			key_ln=unique_id )
		image.save()

		url = 'https://{}.s3.amazonaws.com/{}'
		final_url = url.format( 'tandem-listing-images', unique_id )

		ret_list.append( { 'postData': presigned_post, 'finalUrl': final_url } )
	return ret_list, 200


@tandem.task.task_dispatcher.register_task(
	'task_listing_delete_image',
	'1',
	'user.listing.images.delete' )
def task_listing_delete_image( access, listing_id, image_id ):
	if not (
			tandem.task.db_models.Listing
			.select()
			.where(
				( tandem.task.db_models.Listing.listing_id == listing_id ) &
				( tandem.task.db_models.Listing.host == access.get_subject_id() ) )
			.exists() ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions
		)

	try:
		image = (
			tandem.task.db_models.ListingImage
			.get_preset_query( 'delete' )
			.where(
				( tandem.task.db_models.ListingImage.listing == listing_id ) &
				( tandem.task.db_models.ListingImage.key_ln == image_id ) )
			.get() )
	except tandem.task.db_models.ListingImage.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.ListingImageDoesNotExist
		)

	tandem.task.db_models.ListingImage.hard_delete( image.listing_image_id )

	return { 'deletedImage': image.key_ln }, 200


######################
#  Listings - Rules  #
######################


@tandem.task.task_dispatcher.register_task(
	'task_new_listing_rule',
	'1',
	'user.listings.rules.post' )
@tandem.task.db_models.mysql_access
def task_new_listing_rule( access, listing_id, rules ):
	if not tandem.task.db_models.Listing.select().where(
		( tandem.task.db_models.Listing.host == access.get_subject_id() ) &
		( tandem.task.db_models.Listing.listing_id == listing_id ) ).exists():
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions
		)

	listing_data = tandem.task.doc_models.ListingData.get( listing_id )

	for rule in rules:
		try:
			type = rule.pop( 'type' )
		except KeyError:
			type = None

		try:
			new_rule = listing_data.create_rule( type, **rule )
			rule.update( { 'type': type } )
		except tandem.task.exceptions.RuleInvalidTypeError:
			rule.update(
				{
					'status':
					{
						'accepted': False,
						'reason': 'Rule type is not valid'
					}
				}
			)
			continue
		except tandem.task.exceptions.RuleMissingAttributes as e:
			rule.update(
				{
					'status':
					{
						'accepted': False,
						'reason': 'Rule missing parameters {}'.format( ' '.join( e.missing ) )
					}
				}
			)
			continue

		rule.update( { 'status': { 'accepted': True }, 'id': new_rule.get_slug() } )

		for date in rule.get( 'dates', [] ):
			try:
				start = tandem.tools.helpers.datetime_str2naive(
					date.get( 'start' )
				)
				date['start'] = tandem.tools.helpers.datetime_naive2aware( start ).isoformat()
				end = tandem.tools.helpers.datetime_str2naive(
					date.get( 'end' )
				)
				date['end'] = tandem.tools.helpers.datetime_naive2aware( end ).isoformat()
			except ValueError:
				date.update(
					{
						'status':
						{
							'accepted': False,
							'reason': 'Time field not in correct yyyy-mm-ddThh:mmZ format'
						}
					}
				)
				continue

			try:
				listing_data.create_date(
					new_rule.rule_id,
					start,
					end,
					date.get( 'repeating' )
				)
			except tandem.task.exceptions.RuleDateMissingAttributes as e:
				date.update(
					{
						'status':
						{
							'accepted': False,
							'reason': 'Some fields are missing {}'.format( ' '.join( e.missing ) )
						}
					}
				)
				continue
			except tandem.task.exceptions.RuleDateInvalidTime:
				date.update(
					{
						'status':
						{
							'accepted': False,
							'reason': 'Times are invalid or ill-formed'
						}
					}
				)
				continue
			except tandem.task.exceptions.RuleDateInvalidRepeating:
				date.update(
					{
						'status':
						{
							'accepted': False,
							'reason': 'Repeating is invalid or doesn\'t match time length'
						}
					}
				)
				continue
			except tandem.task.exceptions.RuleDateConflictError:
				date.update(
					{
						'status':
						{
							'accepted': False,
							'reason': 'Conflicts with existing date'
						}
					}
				)
				continue

			date.update( { 'status': { 'accepted': True } } )

	listing_data.save()

	return rules, 201


@tandem.task.task_dispatcher.register_task(
	'task_get_all_listing_rules',
	'1',
	'user.listings.rules.get' )
@tandem.task.db_models.mysql_access
def task_get_all_listing_rules( access, listing_id ):
	if not tandem.task.db_models.Listing.select().where(
		( tandem.task.db_models.Listing.host == access.get_subject_id() ) &
		( tandem.task.db_models.Listing.listing_id == listing_id ) ).exists():
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions
		)

	listing_data = tandem.task.doc_models.ListingData.get( listing_id )

	returns = []

	if len( listing_data.payment_rules ) > 0:
		for rule in listing_data.payment_rules:
			returns.append( rule.to_dict() )

	return returns, 200


@tandem.task.task_dispatcher.register_task(
	'task_delete_listing_rule',
	'1',
	'user.listings.rules.delete' )
@tandem.task.db_models.mysql_access
def task_delete_listing_rule( access, listing_id, rule_id ):
	if not tandem.task.db_models.Listing.select().where(
			( tandem.task.db_models.Listing.host == access.get_subject_id() ) &
			( tandem.task.db_models.Listing.listing_id == listing_id ) ).exists():
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions )

	listing_data = tandem.task.doc_models.ListingData.get( listing_id )
	try:
		rule = (
			rule for rule in listing_data.payment_rules if
			rule.rule_id == tandem.task.doc_models.slug2uuid( rule_id )
		).next()
	except StopIteration:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.RuleDoesNotExist )

	listing_data.payment_rules.remove( rule )

	listing_data.save()

	return rule_id, 200


@tandem.task.task_dispatcher.register_task(
	'task_new_listing_rule_date',
	'1',
	'user.listings.rules.dates.post' )
@tandem.task.db_models.mysql_access
def task_new_listing_rule_date( access, listing_id, rule_id, dates ):
	if not tandem.task.db_models.Listing.select().where(
		( tandem.task.db_models.Listing.host == access.get_subject_id() ) &
		( tandem.task.db_models.Listing.listing_id == listing_id ) ).exists():
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions )

	listing_data = tandem.task.doc_models.ListingData.get( listing_id )

	for date in dates:
		try:
			start = tandem.tools.helpers.datetime_str2naive(
				date.get( 'start' )
			)
			date['start'] = tandem.tools.helpers.datetime_naive2aware( start ).isoformat()
			end = tandem.tools.helpers.datetime_str2naive(
				date.get( 'end' )
			)
			date['end'] = tandem.tools.helpers.datetime_naive2aware( end ).isoformat()
		except ValueError:
			date.update(
				{
					'status':
					{
						'accepted': False,
						'reason': 'Time field not in correct yyyy-mm-ddThh:mmZ format'
					}
				}
			)
			continue

		try:
			listing_data.create_date(
				rule_id,
				start,
				end,
				date.get( 'repeating' )
			)
		except tandem.task.exceptions.RuleDateMissingAttributes as e:
			date.update(
				{
					'status':
					{
						'accepted': False,
						'reason': 'Some fields are missing {}'.format( ' '.join( e.missing ) )
					}
				}
			)
			continue
		except tandem.task.exceptions.RuleDateInvalidTime:
			date.update(
				{
					'status':
					{
						'accepted': False,
						'reason': 'Times are invalid or ill-formed'
					}
				}
			)
			continue
		except tandem.task.exceptions.RuleDateInvalidRepeating:
			date.update(
				{
					'status':
					{
						'accepted': False,
						'reason': 'Repeating is invalid or doesn\'t match time length'
					}
				}
			)
			continue
		except tandem.task.exceptions.RuleDateConflictError:
			date.update(
				{
					'status':
					{
						'accepted': False,
						'reason': 'Conflicts with existing date'
					}
				}
			)
			continue

		date.update( { 'status': { 'accepted': True } } )

	listing_data.save()

	return dates, 201


@tandem.task.task_dispatcher.register_task(
	'task_delete_listing_rule_date',
	'1',
	'user.listings.rules.dates.delete' )
@tandem.task.db_models.mysql_access
def task_delete_listing_rule_date( access, listing_id, rule_id, start ):
	if not tandem.task.db_models.Listing.select().where(
		( tandem.task.db_models.Listing.host == access.get_subject_id() ) &
		( tandem.task.db_models.Listing.listing_id == listing_id ) ).exists():
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserMissingPermissions
		)

	try:
		n_start = tandem.tools.helpers.datetime_str2naive( start )
	except ValueError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidRuleTime )

	listing_data = tandem.task.doc_models.ListingData.get( listing_id )
	try:
		rule = (
			rule for rule in listing_data.payment_rules if
			rule.rule_id == tandem.task.doc_models.slug2uuid( rule_id )
		).next()
	except StopIteration:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.RuleDoesNotExist
		)

	try:
		date = (
			date for date in rule.available_dates if
			date.start_dtm == n_start
		).next()
	except StopIteration:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.DateDoesNotExist )

	rule.available_dates.remove( date )

	listing_data.save()

	return start, 200


################
#  Agreements  #
################


@tandem.task.task_dispatcher.register_task(
	'task_new_agreement',
	'1',
	'user.agreements.post' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_new_agreement(
	access,
	listing_id,
	vehicle_id,
	card_id,
	start_time,
	end_time ):
	user = tandem.task.db_models.User.select(
		tandem.task.db_models.User.user_id,
		tandem.task.db_models.User.stripe_customer_id,
		tandem.task.db_models.User.first_name_ln,
		tandem.task.db_models.User.last_name_ln
	).where( tandem.task.db_models.User.user_id == access.get_subject_id() ).get()

	if vehicle_id <= 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidVehicleId )

	try:
		vehicle = (
			tandem.task.db_models.Vehicle
			.select()
			.where( tandem.task.db_models.Vehicle.vehicle_id == vehicle_id )
			.get() )
	except tandem.task.db_models.Vehicle.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.VehicleDoesNotExist
		)

	if vehicle.owner_id != user.user_id:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.MismatchedVehicle
		)

	if listing_id <= 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidListingId
		)

	try:
		listing = tandem.task.db_models.Listing.select(
			tandem.task.db_models.Listing.listing_id,
			tandem.task.db_models.Listing.status_ind,
			tandem.task.db_models.Listing.host,
			tandem.task.db_models.Address
		).join(
			tandem.task.db_models.Address,
			on=(
				tandem.task.db_models.Listing.address ==
				tandem.task.db_models.Address.address_id )
		).where( tandem.task.db_models.Listing.listing_id == listing_id ).get()
	except tandem.task.db_models.Listing.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.ListingDoesNotExist
		)

	host = (
		tandem.task.db_models.User
		.select( tandem.task.db_models.User.stripe_connect_id )
		.where( tandem.task.db_models.User.user_id == listing.host_id )
		.get() )

	now = datetime.datetime.utcnow().replace( second=0, microsecond=0 )

	min_time = datetime.timedelta( hours=1 )

	try:
		save_the_date = tandem.tools.helpers.datetime_str2naive( start_time )
	except ValueError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidAgreementTime
		)

	agreement_status = 'Open'

	if save_the_date < now:
		if now - save_the_date > min_time:
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.BadRequest,
				tandem.task.error_responses.error_codes.InvalidAgreementTime
			)
		agreement_status = 'Ongoing'

	try:
		end_the_date = tandem.tools.helpers.datetime_str2naive( end_time )
	except ValueError:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidAgreementTime
		)

	if end_the_date - save_the_date < min_time:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidAgreementLength
		)

	if end_the_date <= save_the_date:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidAgreementTime
		)

	plan = tandem.task.db_models.Listing.available(
		listing_id,
		save_the_date,
		end_the_date )
	if plan is None:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.ListingBusy )

	concurrent_vehicle_agreement = (
		tandem.task.db_models.Agreement.select( tandem.task.db_models.Agreement )
		.where(
			(
				( tandem.task.db_models.Agreement.start_dtm < end_the_date ) &
				( tandem.task.db_models.Agreement.start_dtm >= save_the_date )
			) |
			(
				( tandem.task.db_models.Agreement.start_dtm <= save_the_date ) &
				( tandem.task.db_models.Agreement.end_dtm >= end_the_date )
			) |
			(
				( tandem.task.db_models.Agreement.end_dtm > save_the_date ) &
				( tandem.task.db_models.Agreement.end_dtm <= end_the_date )
			) |
			(
				( tandem.task.db_models.Agreement.start_dtm >= save_the_date ) &
				( tandem.task.db_models.Agreement.end_dtm <= end_the_date )
			)
		)
		.where(
			tandem.task.db_models.Agreement.driver_vehicle == vehicle_id,
			tandem.task.db_models.Agreement.status_ind != 'Closed'
		)
		.limit( 1 ) )
	if concurrent_vehicle_agreement.exists():
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.VehicleBusy
		)

	if user.stripe_customer_id == '':
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.UserMissingPaymentMethod
		)

	if card_id is None:
		customer = stripe.Customer.retrieve( user.stripe_customer_id )
		if customer['default_source'] is None:
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.BadRequest,
				tandem.task.error_responses.error_codes.UserMissingPaymentMethod
			)

		card_id = customer['default_source']

	charge = stripe.Charge.create(
		amount=plan.base_cost_amt + plan.driver_service_fee_amt,
		currency='usd',
		source=card_id,
		customer=user.stripe_customer_id,
		destination={
			'account': host.stripe_connect_id,
			'amount': plan.base_cost_amt - plan.host_service_fee_amt
		},
		description='Leasing listing {} from {} to {}'.format(
			listing.listing_id,
			save_the_date.isoformat(),
			end_the_date.isoformat()
		)
	)

	agreement = tandem.task.db_models.Agreement(
		host=listing.host_id,
		driver=user.user_id,
		listing=listing.listing_id,
		cost_amt=plan.base_cost_amt,
		driver_vehicle=vehicle.vehicle_id,
		status_ind=agreement_status,
		creation_dtm=datetime.datetime.utcnow(),
		start_dtm=save_the_date,
		end_dtm=end_the_date
	)
	agreement.save()
	plan.agreement_id = agreement.agreement_id
	plan.save()

	transaction = tandem.task.db_models.TransactionLog(
		user=user.user_id,
		resource_id=agreement.agreement_id,
		stripe_id=charge['id'],
		type_ind='Charge',
		reason_ind='Agreement',
		transaction_amt=charge['amount'],
		creation_dtm=datetime.datetime.utcnow()
	)
	transaction.save()

	ret = agreement.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		preset='private'
	)
	ret['info'] = plan.to_dict( 'Driver' )

	host = tandem.task.db_models.User.select(
		tandem.task.db_models.User.email_ln,
		tandem.task.db_models.User.user_id,
		tandem.task.db_models.User.fcm_device_group_key,
		tandem.task.db_models.User.fcm_device_group_name
	).where( tandem.task.db_models.User.user_id == listing.host_id ).get()
	data = {
		'messageType': 'ListingReserved',
		'agreementId': agreement.agreement_id
	}

	try:
		host.push_message(
			'Listing Reserved',
			'{} {} has reserved your listing at {}'.format(
				user.first_name_ln,
				user.last_name_ln,
				listing.address.to_str()
			),
			data
		)
	except Exception as e:
		client = tandem.task.email.EmailClient(
			tandem.tools.constants.CONST.GMAIL_USER,
			tandem.tools.constants.CONST.GMAIL_PASS
		)
		client.send_error( e )

	return ret, 201


@tandem.task.task_dispatcher.register_task(
	'task_new_agreement_message',
	'1',
	'user.agreements.messages.post' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_new_agreement_message( access, agreement_id, message ):
	if agreement_id <= 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidAgreementId )

	try:
		agreement = (
			tandem.task.db_models.Agreement
			.get_preset_query( 'private-message' )
			.where( tandem.task.db_models.Agreement.agreement_id == agreement_id )
			.get() )

		sender = None
		receiver = None
		author = None
		if(
			access.get_subject_id() != agreement.host_id and
			access.get_subject_id() != agreement.driver_id ):
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.Forbidden,
				tandem.task.error_responses.error_codes.UserNotInAgreement )

		if access.get_subject_id() == agreement.host_id:
			author = 'Host'
			sender = agreement.host
			receiver = agreement.driver
		else:
			author = 'Driver'
			sender = agreement.driver
			receiver = agreement.host

		chat_message = tandem.task.db_models.ChatMessage(
			message_txt=message,
			agreement=agreement.agreement_id,
			status_ind='Sent',
			author_sn=author,
			creation_dtm=datetime.datetime.utcnow()
		)
		chat_message.save()

		data = {
			'messageType': 'NewMessage',
			'agreementId': agreement_id,
			'chatMessageId': chat_message.chat_message_id,
			'author': author,
			'authorId': sender.user_id
		}
		receiver.push_message(
			'New Message',
			'{} {} sent you a message'.format(
				sender.first_name_ln,
				sender.last_name_ln ),
			data )

		return chat_message.to_dict(
			tandem.tools.constants.CONST.DICT_FORMAT_JSON,
			all=True ), 201
	except tandem.task.db_models.Agreement.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.NotFound,
			tandem.task.error_responses.error_codes.AgreementDoesNotExist )


@tandem.task.task_dispatcher.register_task(
	'task_get_agreement',
	'1',
	'user.agreements.get' )
@tandem.task.db_models.mysql_access
def task_get_agreement( access, agreement_id ):
	"""Returns agreement_id, host_id, and driver_id of listing with id.

	When client wants to grab the information of an agreement they call this.

	To Do:
		Evaluate what information to release.

	Args:
		current_user (dict): The information of user making the request.
		agreement_id (int): ID of the agreement to grab.

	Returns:
		tuple: tuple containing:

			msg (dictionary): A dictionary of the agreement_id, host_id,
			and driver_id if the id is valid. If not returns empty.

			code (int): The HTTP Code. Returns 200 if the listing exists.
			 If not returns 404. Can also return 401 if the user requesting
			 the agreement is not a driver or host

	"""
	if agreement_id <= 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidAgreementId )

	agreements = (
		tandem.task.db_models.Agreement.get_preset_query( 'private' )
		.where( tandem.task.db_models.Agreement.agreement_id == agreement_id ) )

	chat_messages = (
		tandem.task.db_models.ChatMessage
		.get_preset_query( 'private' )
		.order_by( tandem.task.db_models.ChatMessage.creation_dtm.desc() )
		.limit( 5 ) )

	agreements_with_messages = peewee.prefetch( agreements, chat_messages )

	if len( agreements_with_messages ) == 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.NotFound,
			tandem.task.error_responses.error_codes.AgreementDoesNotExist )

	agreement = agreements_with_messages[0]

	if(
			access.get_subject_id() != agreement.host_id and
			access.get_subject_id() != agreement.driver_id ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserNotInAgreement )

	agreement_data = tandem.task.doc_models.AgreementData.get( agreement_id )

	agreement_dict = agreement.to_dict(
		tandem.tools.constants.CONST.DICT_FORMAT_JSON,
		preset='private' )

	if agreement.host_id == access.get_subject_id():
		agreement_dict['info'] = agreement_data.to_dict( 'Host' )

	if agreement.driver_id == access.get_subject_id():
		agreement_dict['info'] = agreement_data.to_dict( 'Driver' )

	agreement_dict.update( { 'messages': [] } )

	return agreement_dict, 200


@tandem.task.task_dispatcher.register_task(
	'task_cancel_agreement',
	'1',
	'user.agreements.cancel' )
@tandem.task.db_models.mysql_access
@tandem.task.db_models.mysql_transaction
def task_cancel_agreement( access, agreement_id ):
	"""Cancels either an Ongoing or Open agreement.

	This method cancels an agreement between two users. The effects of
	 cancellation depends on who, the driver or the host, is attempting
	 to cancel the agreement and where along the agreements time boundaries
	 the cancel is requested. These are as follow:

	Open::

		The agreement has been created but isn't currently in effect.

		Driver - The driver will receive a complete refund and the host will
		 not gain any credit from this agreement.

		Host - If the cancel is issued less than 7 days before the start
		 date/time of the agreement then the host will be billed with a
		 small penalty. This penalty will first be deducted from their credit
		 and any remainder will be billed to their card. The Driver will receive
		 a full refund.

	Ongoing::

		The agreement has started and the driver is legally allowed to be
		 parked in the stop

		Driver - The Driver will receive a partial refund for the hours they
		 have not used up. The Host will be credited for any hours used by the
		 Driver.

		Host - The host will face a large penalty. The Driver will receive a
		 partial refund for any hours they used.

	Args:
		current_user (dict): The information of user making the request.
		agreement_id (int): ID of the agreement to grab.

	Returns:
		tuple: tuple containing:

			msg (dictionary): A dictionary of the agreement_id, host_id,
			and driver_id if the id is valid. If not returns empty.

			code (int): The HTTP Code. Returns 200 if the listing exists. If not
			 returns 404. Can also return 401 if the user requesting the agreement
			 is not a driver or host

	"""
	returns = {}

	if agreement_id <= 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidAgreementId )

	try:
		agreement = (
			tandem.task.db_models.Agreement
			.get_preset_query( 'private' )
			.where( tandem.task.db_models.Agreement.agreement_id == agreement_id )
			.get() )
	except tandem.task.db_models.Agreement.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.NotFound,
			tandem.task.error_responses.error_codes.AgreementDoesNotExist )

	if(
			access.get_subject_id() != agreement.host_id and
			access.get_subject_id() != agreement.driver_id ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserNotInAgreement )

	if agreement.status_ind == 'Disputed':
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.AgreementDisputed )

	if agreement.status_ind == 'Open':
		# No matter what the driver receives a complete refund
		old_transaction = (
			tandem.task.db_models.TransactionLog
			.select( tandem.task.db_models.TransactionLog.stripe_id )
			.where(
				( tandem.task.db_models.TransactionLog.user == agreement.driver ) &
				( tandem.task.db_models.TransactionLog.resource_id == agreement_id ) &
				( tandem.task.db_models.TransactionLog.type_ind == 'Charge' ) &
				( tandem.task.db_models.TransactionLog.reason_ind == 'Agreement' ) )
			.get() )

		# Run the refund through stripe
		driver_refund = stripe.Refund.create(
			charge=old_transaction.stripe_id )

		# Log this transaction
		new_transaction = tandem.task.db_models.TransactionLog(
			user=agreement.driver_id,
			resource_id=agreement_id,
			stripe_id=driver_refund['id'],
			type_ind='Refund',
			reason_ind='Agreement Cancel',
			transaction_amt=driver_refund['amount'],
			creation_dtm=datetime.datetime.utcnow() )
		new_transaction.save()

		if access.get_subject_id() == agreement.driver_id:
			# I'm the driver so the driver refund is my refund
			returns.update( { 'refund': driver_refund['amount'] } )

			# Since the driver is cancelling the host faces no penalty hence nothing her
		elif access.get_subject_id() == agreement.host_id:
			# I'm the host and I'm the one cancelling so let's do this
			min_cancel_period = datetime.timedelta( days=7 )
			now = datetime.datetime.utcnow().replace( second=0, microsecond=0 )

			if agreement.start_dtm - now < min_cancel_period:
				# I'm cancelling too close to the start date so I face a penalty
				penalty_amount = int( agreement.cost_amt * 0.2 )

				# Log this transaction
				host_transaction = tandem.task.db_models.TransactionLog(
					user=agreement.host_id,
					resource_id=agreement_id,
					stripe_id='',
					type_ind='Penalty',
					reason_ind='Agreement Cancel',
					transaction_amt=penalty_amount,
					creation_dtm=datetime.datetime.utcnow()
				)
				host_transaction.save()

				returns.update( { 'penalty': penalty_amount } )
			else:
				returns.update( { 'penalty': 0 } )

	elif agreement.status_ind == 'Ongoing':
		if access.get_subject_id() == agreement.driver_id:
			# The driver can't be refunded once the agreement is in effect.
			returns.update( { 'refund': 0 } )
		elif access.get_subject_id() == agreement.host_id:
			# Host can't cancel an active agreement
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.Forbidden,
				tandem.task.error_responses.error_codes.AgreementInEffect )
	else:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.AgreementNotOpen )

	agreement.status_ind = 'Closed'
	agreement.save()

	return returns, 200


@tandem.task.task_dispatcher.register_task(
	'task_get_agreement_messages',
	'1',
	'user.agreements.messages.get' )
@tandem.task.db_models.mysql_access
def task_get_agreement_messages( access, agreement_id ):
	"""Returns a collection of all the messages between host and driver
	 in an agreement.

	Args:
		current_user (dict): The information of user making the request.
		agreement_id (int): ID of the agreement to grab.

	Returns:
		tuple: tuple containing:

			msg (dictionary): A dictionary the messages.

			code (int): The HTTP Code. Returns 200 if the listing exists. If
				 not returns 404.
			Can also return 401 if the user requesting the agreement is not a
				 host or driver

	"""

	if agreement_id <= 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.InvalidAgreementId )

	agreements = (
		tandem.task.db_models.Agreement
		.select(
			tandem.task.db_models.Agreement.agreement_id,
			tandem.task.db_models.Agreement.host,
			tandem.task.db_models.Agreement.driver )
		.where( tandem.task.db_models.Agreement.agreement_id == agreement_id ) )

	chat_messages = (
		tandem.task.db_models.ChatMessage
		.get_preset_query( 'private' )
		.order_by( tandem.task.db_models.ChatMessage.creation_dtm.desc() ) )

	agreements_with_messages = peewee.prefetch( agreements, chat_messages )

	if len( agreements_with_messages ) == 0:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.NotFound,
			tandem.task.error_responses.error_codes.AgreementDoesNotExist )

	agreement = agreements_with_messages[0]

	if(
			access.get_subject_id() != agreement.host_id and
			access.get_subject_id() != agreement.driver_id ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserNotInAgreement )

	messages = []
	for message in agreement.chat_messages_prefetch:
		messages.append( message.to_dict(
			tandem.tools.constants.CONST.DICT_FORMAT_JSON, preset='private' ) )

	return messages, 200


@tandem.task.task_dispatcher.register_task(
	'task_get_cur_agreement',
	'1',
	'user.agreements.current' )
@tandem.task.db_models.mysql_access
def task_get_cur_agreement( access ):
	"""Returns all the agreements currently open associated with current_user.

	Returns either a list of agreements for hosts or a single agreement for
	 drivers.

	Args:
		current_user (dict): The information of user making the request.

	Returns:
		tuple:

			msg (dict): A dictionary of the

			code (int): The associated HTTP code with msg. 200 if an agreement is
				 found. 404 if the user has no open agreements.

	"""

	agreements = tandem.task.db_models.Agreement.get_preset_models(
		'private',
		[
			( tandem.task.db_models.Agreement.driver == access.get_subject_id() ) |
			( tandem.task.db_models.Agreement.host == access.get_subject_id() )
		] )

	dict_agreements = {
		'hostAgreements': [],
		'driverAgreements': []
	}

	agreements_list = []

	for agreement in agreements:
		agreements_list.append( agreement.agreement_id )
		if agreement.host_id == access.get_subject_id():
			dict_agreements['hostAgreements'].append(
				agreement.to_dict(
					tandem.tools.constants.CONST.DICT_FORMAT_JSON,
					preset='private' ) )

			dict_agreements['hostAgreements'][-1].update( { 'messages': [] } )
		else:
			dict_agreements['driverAgreements'].append(
				agreement.to_dict(
					tandem.tools.constants.CONST.DICT_FORMAT_JSON,
					preset='private' ) )

			dict_agreements['driverAgreements'][-1].update( { 'messages': [] } )

	agreement_datas = tandem.task.doc_models.AgreementData.get_many(
		agreements_list )

	for dict_agreement in dict_agreements['hostAgreements']:
		agreement_data = (
			data for
			data in agreement_datas if
			data.agreement_id == dict_agreement['id'] ).next()
		dict_agreement['info'] = agreement_data.to_dict( 'Host' )

	for dict_agreement in dict_agreements['driverAgreements']:
		agreement_data = (
			data for
			data in agreement_datas if
			data.agreement_id == dict_agreement['id']).next()
		dict_agreement['info'] = agreement_data.to_dict( 'Driver' )

	return dict_agreements, 200


@tandem.task.task_dispatcher.register_task(
	'task_file_agreement_dispute',
	'1',
	'user.agreements.disputes.post' )
@tandem.task.db_models.mysql_access
def task_file_agreement_dispute(
		access,
		agreement_id,
		dispute_type,
		file_type ):
	try:
		agreement = (
			tandem.task.db_models.Agreement
			.select(
				tandem.task.db_models.Agreement.agreement_id,
				tandem.task.db_models.Agreement.host,
				tandem.task.db_models.Agreement.driver,
				tandem.task.db_models.Agreement.status_ind )
			.where( tandem.task.db_models.Agreement.agreement_id == agreement_id )
			.get() )
	except tandem.task.db_models.Agreement.DoesNotExist:
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.AgreementDoesNotExist )

	if(
			access.get_subject_id() != agreement.host_id and
			access.get_subject_id() != agreement.driver_id ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.Forbidden,
			tandem.task.error_responses.error_codes.UserNotInAgreement )

	if(
			tandem.task.db_models.Dispute
			.select()
			.where(
				( tandem.task.db_models.Dispute.agreement == agreement_id ) &
				( tandem.task.db_models.Dispute.status_ind == 'Open' ) )
			.exists() ):
		return tandem.task.error_responses.create_error(
			tandem.task.error_responses.error_types.BadRequest,
			tandem.task.error_responses.error_codes.AgreementAlreadyInDispute )

	unique_id = ''.join( ['img_', uuid.uuid4().hex] )
	s3client = boto3.client( 's3' )
	presigned_post = s3client.generate_presigned_post(
		Bucket='tandem-dispute-images',
		Key=unique_id,
		Fields={
			'acl': 'public-read',
			'Content-Type': file_type },
		Conditions=[
			{ 'acl': 'public-read' },
			{ 'Content-Type': file_type },
			['content-length-range', 0, 5242880] ],
		ExpiresIn=300 )

	dispute = tandem.task.db_models.Dispute(
		agreement=agreement_id,
		claimant=access.get_subject_id(),
		respondent=(
			agreement.host_id if
			access.get_subject_id() == agreement.driver_id else
			agreement.driver_id ),
		dispute_ind=dispute_type,
		status_ind='Open',
		prev_agreement_status_ind=agreement.status_ind,
		creation_dtm=datetime.datetime.utcnow() )
	dispute.save()

	image = tandem.task.db_models.DisputeImage(
		dispute=dispute.dispute_id,
		party='Claimant',
		key_ln=unique_id )
	image.save()

	agreement.status_ind = 'Disputed'
	agreement.save()

	return
	{
		'dispute': dispute.to_dict(
			tandem.tools.constants.CONST.DICT_FORMAT_JSON,
			preset='private' ),
		'postData': presigned_post,
		'finalUrl': 'https://{}.s3.amazonaws.com/{}'.format(
			'tandem-user-images',
			unique_id )
	}, 200
