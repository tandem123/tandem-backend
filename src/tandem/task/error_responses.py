# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

_error_type_dictionary = {
	'BadRequest': 400,
	'Unauthorized': 401,
	'Forbidden': 403,
	'NotFound': 404,
	'InternalServerError': 500
}

_error_code_dictionary = {
	# Server Error #
	'SomethingWentWrong': 'Unfortunately something went wrong. Please try again.',
	'VersionNotSupported': 'This version or action is not supported.',
	# Patching #
	'InvalidPatch': 'The patch was not formed correctly.',
	'PatchFailedValue': 'The patch failed to apply a value.',
	'PatchFailedTest': 'The failed a test operation.',
	# Forbidden #
	'UserNotDriver': 'User must only be a driver.',
	'UserNotHost': 'User does not have host permissions.',
	'UserMissingPermissions': 'User does have permissions to do this.',
	'UserInOpenAgreement': 'User is already in an open agreement.',
	'UserNotInAgreement': 'User not involved in this agreement.',
	# Unauthorized #
	'AuthHeaderMissing': 'Authorization header not supplied.',
	'InvalidAuthFormat': '{} format not supplied in authorization header.',
	'BadToken': 'Bad token supplied.',
	'BadCredentials': 'Credentials supplied not valid.',
	# General #
	'FieldsMissing': 'Some or all fields empty.',
	# Referral #
	'UserAlreadyInReferralProgram': 'This user already belongs to a referral program and can not join another.',
	'InvalidReferralProgram': 'Referral program supplied does not exist or is no longer valid.',
	'UserNotInReferralProgram': 'This user does not belong to a referral program.',
	# Listing #
	'ListingDoesNotExist': 'Listing does not exist.',
	'ListingBusy': 'Listing is already occupied for part of that time slot.',
	'InvalidListingRate': 'Listing rate is invalid.',
	'InvalidListingId': 'Listing id supplied invalid.',
	'IncompleteAddress': 'Address supplied does not have enough fields filled to be complete.',
	'InvalidBookingDate': 'Date is invalid.',
	'InvalidAddress': 'Address supplied is invalid.',
	# Listing Image #
	'ListingImageDoesNotExist': 'Listing image does not exist.',
	# Listind Deletion #
	'ListingHasActiveAgreements': 'Listing has active agreements and can\'t be deleted at this time.',
	# User #
	'UserDoesNotExist': 'User does not exist.',
	'UserExists': 'User already exists with supplied credentials.',
	'InvalidUserId': 'User id supplied invalid.',
	'InvalidPassword': 'The supplied password is invalid.',
	'InvalidAccessToken': 'The supplied facebook access token is invalid.',
	# User Devices #
	'TokenAlreadyRegistered': 'This token is already registered to this user.',
	'TokenNotRegistered': 'This token is not registered to thise user.',
	'InvalidDeviceToken': 'This token is invalid.',
	# Image #
	'InvalidImageToken': 'Image token is not valid.',
	# User Promotion #
	'InvalidPhoneNumber': 'The supplied phone number is invalid.',
	'RequirePaymentMethod': 'User requires atleast 1 payment method before they can become a Host.',
	# User Payments #
	'InvalidStripeToken': 'Charge token supplied invalid.',
	'NoPaymentMethod': 'Supplied card id not valid or user missing default card.',
	'UserMissingPaymentMethod': 'This user has no payment methods.',
	'InvalidAmount': 'The amount supplied is either larger than user credit or an invalid number.',
	# Vehicle #
	'VehicleDoesNotExist': 'Vehicle does not exist.',
	'VehicleBusy': 'Vehicle is already busy for that time slot.',
	'InvalidVehicleId': 'Vehicle id supplied invalid.',
	'MismatchedVehicle': 'Vehicle does not belong to supplied driver.',
	# Agreement #
	'AgreementDoesNotExist': 'Agreement does not exist.',
	'InvalidPaymentMethod': 'The supplied payment method is invalid',
	'InvalidAgreementTime': 'Specified agreement time not valid.',
	'InvalidAgreementLength': 'Agreement length is invalid.',
	'InvalidAgreementId': 'Agreement id supplied invalid.',
	'AgreementNotOpen': 'This agreement is currently not open or in effect.',
	'AgreementInEffect': 'This agreement is in effect and can\'t be cancelled.',
	'TransactionTooSmall': 'The total transaction amount is too small.',
	'AgreementAlreadyInDispute': 'This agreement is already in an open dispute.',
	'AgreementDisputed': 'This agreement is being disputed and can\'t  be cancelled.',
	# Blackout #
	'BlackoutDoesNotExist': 'Blackout does not exist.',
	'InvalidBlackoutTime': 'The submitted blackout time is invalid',
	'InvalidRuleTime': 'The submitted rule time is invalid',
	'PlanTypeNotValid': 'The requested plan type is not valid',
	'RuleDoesNotExist': 'The requested rule does not exist',
	'DateDoesNotExist': 'The requested date does not exist',
	'InvalidRepeater': 'The submitted repeater is either invalid or unsuitable for the submitted start/end times',
	'DateConflicts': 'The date conflicts with an existing date',
}

def _enum( *sequential, **named ):
    enums = dict( zip( sequential, range( len( sequential ) ) ), **named )
    reverse = dict( ( value, key ) for key, value in enums.iteritems() )
    enums['reverse_mapping'] = reverse
    return type( 'Enum', (), enums )

_args = []
for key, value in _error_type_dictionary.iteritems():
	_args.append( key )
error_types = _enum( *_args )

_args = []
for key, value in _error_code_dictionary.iteritems():
	_args.append( key )
error_codes = _enum( *_args )

def create_error( error_type, error_code, *args ):
	return {
		'type': error_types.reverse_mapping[error_type],
		'code': error_codes.reverse_mapping[error_code],
		'message': _error_code_dictionary[error_codes.reverse_mapping[error_code]].format( *args )
	}, _error_type_dictionary[error_types.reverse_mapping[error_type]]