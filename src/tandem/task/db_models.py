# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

"""
Attributes:
	database (peewee.MySQLDatabase): Connection to MySQL Database that houses all stored information.
	
"""

# Import ORM for the MySQL DB
import peewee

# Import hashing algorithm bcrypt for password hasing
import bcrypt

# Import tools for JWT serialization to manage connections
from itsdangerous import( TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired )

# Import convenient constants
from ..tools import constants, helpers

import datetime

import fcm

from tandem_exceptions import PatchAttributeError, PatchValueError, PatchTestError

from pyfcm import FCMNotification

import doc_models

database = peewee.MySQLDatabase(
	constants.CONST.MYSQL_DB,
	host=constants.CONST.MYSQL_HOST,
	user=constants.CONST.MYSQL_USER,
	passwd=constants.CONST.MYSQL_PASS )
fcm_client = fcm.FCMClient(
	api_key=constants.CONST.FCM_API_KEY,
	sender_id=constants.CONST.FCM_SENDER_ID )

##################
#  MySQL Helper  #
##################


def mysql_access( method ):
	def method_wrapper( *args, **kwargs ):
		database.connect()
		try:
			return method( *args, **kwargs )
		except Exception:
			raise
		finally:
			if not database.is_closed():
				database.close()

	return method_wrapper


def mysql_transaction( method ):
	def method_wrapper( *args, **kwargs ):
		database.set_autocommit( False )
		database.begin()
		try:
			ret = method( *args, **kwargs )
		except Exception:
			database.rollback()
			database.set_autocommit( True )
			raise
		else:
			try:
				database.commit()
				database.set_autocommit( True )
				return ret
			except Exception:
				database.rollback()
				database.set_autocommit( True )
				raise

	return method_wrapper

##################
#  Base Classes  #
##################


class BaseMetaMetaClass( type ):
	""" Base metaclass for base meta. Manipulates meta information about the
	 models.

	"""

	pass


class BaseMeta():
	""" Base meta that all meta classes in a database model should inherit
	 from. Mostly deals with manipulating meta information.

	"""

	__metaclass__ = BaseMetaMetaClass


class BasePatch( object ):
	def __init__( self, cls, patches=None, permission=None ):
		self.my_cls = cls
		self.replaces = {}
		self.tests = {}
		self.permission = (
			constants.CONST.PATCH_PERMISSION_USER if
			permission is None else
			permission )

		if patches is not None:
			self.loads( patches )

	def loads( self, patches ):
		self.replaces = {}
		self.tests = {}

		for operation in patches:
			op = operation.get( 'op', None )
			if op is None:
				raise PatchAttributeError( 'Op not supplied.' )

			path = operation.get( 'path', None )
			if path is None:
				raise PatchAttributeError( 'Path not supplied.' )

			if path == '':
				raise PatchAttributeError( 'Path is empty.' )

			path = path[1:]
			if path == '':
				raise PatchAttributeError( 'Path is empty.' )

			key = self.my_cls.key_convert(
				path,
				constants.CONST.DICT_FORMAT_JSON,
				constants.CONST.DICT_FORMAT_PYTHON )
			if key is None:
				raise PatchAttributeError(
					'Path does not correspond to any model field: {}'.format( path ) )

			value = operation.get( 'value', None )
			if value is None:
				raise PatchAttributeError( 'Value not found.' )

			if op == 'replace':
				if self.my_cls._meta.patch_permissions[key] < self.permission:
					raise PatchValueError(
						'Supplied permissions insufficient to replace model\'s {}.'.format(
							path ) )
				self.replaces.update( { key: value } )
			elif op == 'test':
				self.tests.update( { key: value }  )
			else:
				raise PatchAttributeError( 'Invalid operation %s' % op )

	def apply_patch( self, model ):
		for k, v in self.tests.iteritems():
			if getattr( model, k ) != v:
				raise PatchTestError( 'Test %s=%s failed' % ( k, v ) )

		for k, v in self.replaces.iteritems():
			setattr( model, k, v )

	def verify_patch( self, access, root, target ):
		if access.get_subject_id() != target:
			return False

		for k, v in self.replaces.iteritems():
			if not '{}.{}'.format( root, k ) in access:
				return False

		return True

	def select_list( self ):
		ret = []
		for k, v in self.replaces.iteritems():
			ret.append( getattr( self.my_cls, k ) )

		for k, v in self.tests.iteritems():
			attr = getattr( self.my_cls, k )
			if attr not in ret:
				ret.append( attr )

		return ret

	def dict_list( self ):
		ret = {}
		for k, v in self.replaces.iteritems():
			ret.update( { k: True } )

		return ret


class BaseModel( peewee.Model ):
	_patch_spec = BasePatch

	@classmethod
	def Patch( cls, *args, **kwargs ):
		return cls._patch_spec( cls, *args, **kwargs )

	class Meta:
		database = database

	def save( self, attributes=None, **kwargs ):
		if attributes is None:
			return super( BaseModel, self ).save( only=self.dirty_fields, **kwargs )
		else:
			return super( BaseModel, self ).save( only=attributes, **kwargs )

	@classmethod
	def get_preset_items( cls, preset, chain=False ):
		if not preset in cls._meta.dict_presets:
			raise ValueError( 'Preset not found in class dict_presets' )

		item_list = []
		for item in cls._meta.dict_presets[preset]['fields']:
			item_list.append( getattr( cls, item ) )

		if chain:
			for expansion in cls._meta.dict_presets[preset]['expansions']:
				if expansion.get( 'alias', False ) is not True:
					item_list += expansion['join_case'][0].get_preset_items( expansion['form'][2], chain = True )
				else:
					for item in expansion['join_case'][0]._meta.dict_presets[expansion['form'][2]]['fields']:
						item_list.append( getattr( expansion['join_case'][0], item ) )

		return item_list

	@classmethod
	def add_preset_joins( cls, preset, select_query, chain = False ):
		if not preset in cls._meta.dict_presets:
			raise ValueError( 'Preset not found in class dict_presets' )
			
		for expansion in cls._meta.dict_presets[preset]['expansions']:
			select_query = select_query.switch( cls )
			select_query = select_query.join( expansion['join_case'][0], on = ( getattr( cls, expansion['form'][0] ) == getattr( expansion['join_case'][0], expansion['join_case'][1] ) ).alias( expansion['form'][0] ) )
			if chain:
				select_query = expansion['join_case'][0].add_preset_joins( expansion['form'][2], select_query, chain = True )
			
		return select_query.switch( cls )
		
	@classmethod
	def get_preset_query( cls, preset, chain = True ):
		if not preset in cls._meta.dict_presets:
			raise ValueError( 'Preset not found in class dict_presets' )
			
		select_query = cls.select( *cls.get_preset_items( preset, chain = chain ) )
		select_query = cls.add_preset_joins( preset, select_query, chain = chain )
		return select_query
		
	@classmethod
	def get_preset_prefetches( cls, preset, instances ):
		if not preset in cls._meta.dict_presets:
			raise ValueError( 'Preset not found in class dict_presets' )
			
		id_list = []
		for k, v in instances.iteritems():
			id_list.append( k )
			
		if len( id_list ) == 0:
			return
			
		prefetches = cls._meta.dict_presets[preset].get( 'prefetches', [] )
		for prefetch in prefetches:
			model = _model_list[prefetch.get( 'model' )]
			key = prefetch.get( 'key' )
			field = prefetch.get( 'field' )
			sub_preset = prefetch.get('preset' )
			
			if model is None or key is None or field is None or sub_preset is None:
				break
			
			select_query = model.get_preset_models( sub_preset, [getattr( model, field ) << id_list] )
			for obj in select_query:
				id = getattr( obj, ''.join( [field, '_id'] ) )
				if not hasattr( instances[id], key ):
					setattr( instances[id], key, [] )
				getattr( instances[id], key ).append( obj )
				
		for expansion in cls._meta.dict_presets[preset]['expansions']:
			sub_instances = {}
			for k, v in instances.iteritems():
				sub_instances.update( { getattr( getattr( v, expansion['form'][0] ), expansion['join_case'][1] ): getattr( v, expansion['form'][0] ) } )
			
			expansion['join_case'][0].get_preset_prefetches( expansion['form'][2], sub_instances )
		
	@classmethod
	def get_preset_models( cls, preset, where_clauses = None, order_clauses = None, **kwargs ):
		if not where_clauses:
			where_clauses = []
			
		if not order_clauses:
			order_clauses = []
			
		if kwargs.get( 'exists', False ):
			return cls.get_preset_query( preset, False ).where( *where_clauses ).exists()
			
		select_query = cls.get_preset_query( preset, True )
		select_query = select_query.where( *where_clauses )
		select_query = select_query.order_by( *order_clauses )
		
		kwargs_limit = kwargs.get( 'limit', None )
		if kwargs_limit:
			select_query = select_query.limit( kwargs_limit )
			
		if kwargs.get( 'get', False ):
			select_query = select_query.limit( 1 )
			
		instances = {}
		if cls._meta.dict_presets[preset].get( 'prefetch-origin' ) is not None:
			for obj in select_query:
				instances.update( { getattr( obj, cls._meta.dict_presets[preset].get( 'prefetch-origin' ) ): obj } )
					
		cls.get_preset_prefetches( preset, instances )
				
		if kwargs.get( 'get', False ):
			return select_query[0]
			
		return select_query
	
	def to_dict( self, format, expand = [], preset = None, **kwargs ):
		"""Returns a dictionary of all of this models fields in the specified format.
		
		Fields to be collected are specified in kwargs by setting the python form of the field to true. all=True can be used to collect all fields.
		
		Examples::
		
			>>>User( first_name_ln = 'first', last_name_ln = 'last' ).to_dict( constants.CONST.DICT_FORMAT_JSON, first_name_ln=True )
			{ 'fistName': 'first' }
				
			>>>User( first_name_ln = 'first', last_name_ln = 'last' ).to_dict( constants.CONST.DICT_FORMAT_PYTHON, all=True )
			{ 'first_name_ln': 'first', 'last_name_ln': 'last' }
			
		Args:
			format (constants.CONST): The format the dictionary keys should be in.
			expand (list, optional): List of tuples that explains attributes to be expanded from key id's to the actual object's data::
			
				key (str): The attribute to be expanded.
				kwargs (dict): The kwargs to be passed to the to_dict call of the expanded. Defaults to { all: True }.
				
				
			**kwargs: Which fields to be collected and returned.
			
		Returns
			dict: A dictionary of all the fields collected.
		
		"""
		
		import decimal
		
		dict = {}
		for ( k, v ) in vars(self)['_data'].iteritems():
			if not k in kwargs:
				kwargs.update( { k: False } )
			
		if 'all' in kwargs:
			all = kwargs['all']
			del kwargs['all']
		else:
			all = False
		
		transform_method = None
		if preset in self._meta.dict_presets:
			kwargs = {}
			for key in self._meta.dict_presets[preset]['fields']:
				kwargs.update( { key: True } )
				
			expand = []
			for expansion in self._meta.dict_presets[preset]['expansions']:
				expand.append( expansion['form'] )
				
			transform_method = self._meta.dict_presets[preset]['transform_method']
		
		for ( k, v ) in kwargs.iteritems():
			if ( v or all ) and k in vars(self)['_data']:
				if isinstance( vars(self)['_data'][k], ( datetime.datetime, datetime.date ) ):
					dict.update( { self._meta.dict_format[format][k]: helpers.datetime_naive2aware( vars(self)['_data'][k] ).isoformat() if vars(self)['_data'][k] is not None else None } )
				elif isinstance( vars(self)['_data'][k], ( decimal.Decimal ) ):
					dict.update( { self._meta.dict_format[format][k]: float( vars(self)['_data'][k] ) } )
				else:
					dict.update( { self._meta.dict_format[format][k]: vars(self)['_data'][k] } )
					
		for expansion in expand:
			key = expansion[0]
			if key is None:
				continue
				
			sub_kwargs = expansion[1] if len( expansion ) > 1 else { 'all': True }
			preset = expansion[2] if len( expansion ) > 2 else None
			dict.pop( self._meta.dict_format[format][key] )
			dict[self._meta.dict_format[format]['@%s' % key]] = getattr( self, key ).to_dict( format, preset = preset, **sub_kwargs )
			
		if transform_method is not None:
			dict = getattr( self, transform_method )( dict )
			
		return dict
		
	@classmethod
	def dict_key_convert( cls, dict, format_from, format_to ):
		""" Converts all the keys in the dict from format_from to format_to.
		
		Args:
			dict (dict): Dictionary that has keys in format format_from.
			format_from (constants.CONST): The old format that dict keys are currently in.
			format_to (constants.CONST): New format that dict keys should be transformed into.
			
		Returns:
			dict: A dictionary with all keys transformed from format_from to format_to.
		
		"""
		ret_dict = {}
		from_normalized = { v: k for k, v in cls._meta.dict_format[format_from].iteritems() }
		for( k, v ) in dict.iteritems():
			if k in from_normalized:
				ret_dict.update( { cls._meta.dict_format[format_to][from_normalized[k]]: dict[k] } )
			else:
				return None
				
		return ret_dict
		
	@classmethod
	def key_convert( cls, key, format_from, format_to ):
		""" Converts a string key from one format key to another format key.
		
		Args:
			key (str): The key in the old format to be changed.
			format_from (constants.CONST): The old format that the key exists in.
			format_to (constants.CONST): The new format the key should be changed to.
			
		Returns:
			str: The new key of the format_to.
		
		"""
		from_normalized = { v: k for k, v in cls._meta.dict_format[format_from].iteritems() }
		if key in from_normalized:
			return cls._meta.dict_format[format_to][from_normalized[key]]
		else:
			return None
		
	"""def patch( self, patch, permission = constants.CONST.PATCH_PERMISSION_ALL ):
		"" Applies a JSON Patch to the model.
		
		Applies a JSON Patch( only replace and test operation supported ) to the model. If any operation fails then the entire patch
		fails. Sub collections can not be affected. If the supplied permission is too low to affect an attribute then the patch fails.
		field permissions should be defined in each models class Meta in patch_permissions.
		
		Args:
			patch (list): A list of dictionary operations to be executed on the model.
			permission (optional, constants.CONST): The permissions of this patch.
			
		Returns:
			dict: Keys and new values of attributes changed in the model if successful. Returns None otherwise.
			
		Raises:
			PatchAttributeError: If there's an issue with the patch attributes.
			PatchValueError: If there's an issue replacing the model value with the patch value.
			PatchTestError: If a test operation fails.
			
		""
		
		import decimal
				
		new_vars = {}
		kwargs = {}
		for( k, v ) in vars(self)['_data'].iteritems():
			new_vars.update( { k: v } )
		
		for operation in patch:
			op = operation.get( 'op', None )
			if op is None:
				raise PatchAttributeError( 'Op not supplied.' )
			
			path = operation.get( 'path', None )
			if path is None:
				raise PatchAttributeError( 'Path not supplied.' )
				
			if path == '':
				raise PatchAttributeError( 'Path is empty.' )
					
			path = path[1:]
			if path == '':
				raise PatchAttributeError( 'Path is empty.' )
				
			path = self.key_convert( path, constants.CONST.DICT_FORMAT_JSON, constants.CONST.DICT_FORMAT_PYTHON )
			
			if not path in new_vars:
					raise PatchAttributeError( 'Path does not exist.' )
					
			value = operation.get( 'value', None )
			if value is None:
					raise PatchAttributeError( 'Value not found.' )
					
			if op == 'replace':
				if self._meta.patch_permissions[path] < permission:
					raise PatchValueError( 'Supplied permissions insufficient to replace model\'s path %s.' % path )
					
				try:
					new_vars[path] = type( new_vars[path] )( value )
				except ( TypeError, ValueError, decimal.DecimalException ):
					raise PatchValueError( 'Could not typecast %s to model\'s path %s.' % ( value, path ) )
				kwargs.update( { path: True } )
			elif op == 'test':
				if new_vars[path] != value:
					raise PatchTestError( 'Test %s=%s failed.' % ( path, value ) )
			else:
				raise PatchAttributeError( 'Invalid operation %s' % op )
							
		for( k, v ) in new_vars.iteritems():
			setattr( self, k, v )
			
		self.save()
		return self.to_dict( constants.CONST.DICT_FORMAT_PYTHON, **kwargs )"""
		
########################################################
####												####
####				Model Classes					####
####												####
########################################################

class User( BaseModel ):
	""" Class model for User in MySQL Database.
	
	Attributes:
		user_id (int): User Id.
		fcm_device_id (str): Firebase Cloud Messaging device registration token.
		fb_id (int): Facebook Id.
		email_ln (str): User's email address.
		password_txt (str): User's encrypted password.
		account_ind (int): Indicator of the user's account type.
		first_name_ln (str): User's first name.
		last_name_ln (str): User's last name.
		creation_dtm (datetime): Date the user created their account.
		
	"""
	
	class UserPatch( BasePatch ):
		def __init__( self, *args, **kwargs ):
			super( User.UserPatch, self ).__init__( *args, **kwargs )
			
		def apply_patch( self, user ):
			if 'password_txt' in self.replaces:
				self.replaces['password_txt'] = bcrypt.hashpw( self.replaces['password_txt'].encode( 'utf-8' ), bcrypt.gensalt( 12 ) )
				
			return super( User.UserPatch, self ).apply_patch( user )
			
	_patch_spec = UserPatch
	
	class Meta( BaseMeta ):
		db_table = 'user'
		patch_permissions = {
			'user_id': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'fcm_device_group_key': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'fcm_device_group_name': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'stripe_customer_id': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'stripe_connect_id': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'fb_id': constants.CONST.PATCH_PERMISSION_USER,
			'email_ln': constants.CONST.PATCH_PERMISSION_USER,
			'password_txt': constants.CONST.PATCH_PERMISSION_USER,
			'account_ind': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'first_name_ln': constants.CONST.PATCH_PERMISSION_USER,
			'last_name_ln': constants.CONST.PATCH_PERMISSION_USER,
			'phone_number': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'creation_dtm': constants.CONST.PATCH_PERMISSION_SYSTEM
		}
		
		dict_format = {
			constants.CONST.DICT_FORMAT_PYTHON: {
				'user_id': 'user_id',
				'fcm_device_group_key': 'fcm_device_group_key',
				'fcm_device_group_name': 'fcm_device_group_name',
				'stripe_customer_id': 'stripe_customer_id',
				'stripe_connect_id': 'stripe_connect_id',
				'fb_id': 'fb_id',
				'email_ln': 'email_ln',
				'password_txt': 'password_txt',
				'account_ind': 'account_ind',
				'first_name_ln': 'first_name_ln',
				'last_name_ln': 'last_name_ln',
				'phone_number': 'phone_number',
				'creation_dtm': 'creation_dtm'
			},
			constants.CONST.DICT_FORMAT_JSON: {
				'user_id': 'id',
				'fcm_device_group_key': 'deviceGroupKey',
				'fcm_device_group_name': 'deviceGroupName',
				'stripe_customer_id': 'customerId',
				'stripe_connect_id': 'connectId',
				'fb_id': 'facebookId',
				'email_ln': 'email',
				'password_txt': 'password',
				'account_ind': 'accountType',
				'first_name_ln': 'firstName',
				'last_name_ln': 'lastName',
				'phone_number': 'phoneNumber',
				'creation_dtm': 'creationDate'
			}
		}
		
		dict_presets = {
			'public': {
				'fields': [
					'user_id',
					'account_ind',
					'first_name_ln',
					'last_name_ln',
					'creation_dtm'
				],
				'expansions': [],
				'transform_method': 'transform_preset_public'
			},
			'private': {
				'fields': [
					'user_id',
					'fb_id',
					'email_ln',
					'account_ind',
					'first_name_ln',
					'last_name_ln',
					'phone_number',
					'creation_dtm',
				],
				'expansions': [],
				'transform_method': 'transform_preset_public'
			},
			'private-technical': {
				'fields': [
					'user_id',
					'first_name_ln',
					'last_name_ln',
					'fcm_device_group_key',
					'fcm_device_group_name',
					'stripe_customer_id',
					'stripe_connect_id',
					'email_ln'
				],
				'expansions': [],
			}
		}
	
	user_id = peewee.PrimaryKeyField()
	fcm_device_group_key = peewee.CharField( default = '' )
	fcm_device_group_name = peewee.CharField( default = '' )
	stripe_customer_id = peewee.CharField( default = '' )
	stripe_connect_id = peewee.CharField( default = '' )
	fb_id = peewee.CharField( default = '' )
	email_ln = peewee.CharField( max_length = 254 )
	password_txt = peewee.CharField( max_length = 128, default = '' )
	account_ind = peewee.CharField( max_length = 10 )
	first_name_ln = peewee.CharField( max_length = 255 )
	last_name_ln = peewee.CharField()
	phone_number = peewee.CharField( default = '' )
	creation_dtm = peewee.DateTimeField()
		
	def transform_preset_public( self, dict ):			
		try:
			image = UserImage.select( UserImage.key_ln ).where( UserImage.user == self.user_id ).get()
		except UserImage.DoesNotExist:
			pass
		else:
			dict.update( { 'profilePicture': image.get_url() } )
		finally:
			return dict
	
	def validate_phone_number( self ):
		import phonenumbers
		
		try:
			phonenumbers.parse( self.phone_number )
		except Exception:
			return False
			
		return True
		
	def verify_password( self, input_password ):
		""" Compares an input password to the hashed password of the user.
		
		Args:
			input_password (str): The plaintext password to be verified.
			
		Returns:
			bool: True if the passwords matches and False if not.
			
		"""
		
		return bcrypt.hashpw( input_password.encode( 'utf-8' ), self.password_txt.encode( 'utf-8' ) ) == self.password_txt
	
	def generate_auth_token( self, expiration = 3600 ):
		""" Generates an auth token for the user.
		
		The auth token is a JWT token and encodes the users id. This token is used for access to
		protected areas so the user does not have to resend login credentials.
		
		Args:
			expiration (int, optional): The time in seconds before the token expires. Defaults to 60 minutes.
			
		Returns:
			str: The plain text token for the user.
			
		"""
		
		s = Serializer( constants.CONST.SECRET_KEY, expires_in = expiration )
		return s.dumps( { 'id': self.user_id } )
		
	@staticmethod
	def verify_auth_token_select( token, select_list ):
		""" Verifies whether the submitted auth token is valid and returns the attributes defined in select_list.
		
		Args:
			token (str): The token that is to be verified.
			select_list (list): List of User attributes to be retrieved from database. If empty, retrieves all user attributes.
			
		Returns:
			Users: The user with the encoded id if valid or None if the token is invalid or expired.
			
		"""
	
		s = Serializer( constants.CONST.SECRET_KEY )
		try:
			data = s.loads( token )
		except SignatureExpired:
			return None
		except BadSignature:
			return None
			
		return User.select( *select_list ).where( User.user_id == data['id'] ).get()
		
	@staticmethod
	def verify_auth_token_exists( token ):
		""" Verifies whether the submitted auth token is valid.
		
		Args:
			token (str): The token that is to be verified.
			
		Returns:
			bool: Whether the token is valid.
		
		"""
		
		s = Serializer( constants.CONST.SECRET_KEY )
		try:
			data = s.loads( token )
		except SignatureExpired:
			return False
		except BadSignature:
			return False
			
		return True
		
	def push_message( self, message_title, message_body, data ):
		if self.fcm_device_group_key == '':
			return
			
		try:
			result = fcm_client.device_group_send( self.fcm_device_group_name, self.fcm_device_group_key, data, body = message_body, title = message_title )
		except fcm.exceptions.FCMBadRequestError:
			return
			
		if 'failed_registration_ids' in result:
			FCMDevice.hard_delete( result['failed_registration_ids'], self.user_id, self.fcm_device_group_name, self.fcm_device_group_key )
		
Host = User.alias()
Driver = User.alias()
Claimant = User.alias()
Respondent = User.alias()

class FCMDevice( BaseModel ):
	user = peewee.ForeignKeyField( User, related_name = 'devices' )
	fcm_id = peewee.CharField( index = True )
	
	class Meta( BaseMeta ):
		db_table = 'fcmdevice'
		primary_key = peewee.CompositeKey( 'user', 'fcm_id' )
		
		dict_format = {
			constants.CONST.DICT_FORMAT_PYTHON: {
				'user': 'user',
				'fcm_id': 'fcm_id'
			},
			constants.CONST.DICT_FORMAT_JSON: {
				'user': 'userId',
				'@user': 'user',
				'fcm_id': 'registrationId'
			}
		}
		
		dict_presets = {
			'delete': {
				'fields': [
					'user',
					'fcm_id'
				],
				'expansions': [],
				'transform_method': None
			}
		}
		
	@staticmethod
	def hard_delete( fcm_ids, user_id, name, key ):
		fcmdevices = FCMDevice.get_preset_query( 'delete' ).where( ( FCMDevice.fcm_id << fcm_ids ) & ( FCMDevice.user == user_id ) )
		ret = {}
		valid_fcm_ids = []
		
		for fcmdevice in fcmdevices:
			valid_fcm_ids.append( fcmdevice.fcm_id )
			fcm_ids.remove( fcmdevice.fcm_id )
			
		for invalid_device in fcm_ids:
			ret.update( {
				invalid_device: {
					'deleted': False,
					'reason': 'Token not registered.'
				}
			} )
			
		for valid_device in valid_fcm_ids:
			ret.update( {
				valid_device: {
					'deleted': True
				}
			} )
			
		
		fcm_client.device_group_remove( name, key, valid_fcm_ids )
		FCMDevice.delete().where( ( FCMDevice.fcm_id << valid_fcm_ids ) & ( FCMDevice.user == user_id ) ).execute()
		
		if not FCMDevice.select().where( FCMDevice.user == user_id ).exists():
			user = User.get_preset_query( 'private-technical' ).for_update().where( User.user_id == user_id ).get()
			user.fcm_device_group_key = ''
			user.fcm_device_group_name = ''
			user.save()
			
		return ret
		
		"""
		fcmdevices = FCMDevice.get_preset_query( 'delete' ).where( FCMDevice.fcm_id << fcm_ids )
		ret = {}
		
		master_user_id = fcmdevices[0].user_id
		for fcmdevice in fcmdevices:
			if fcmdevice.user_id != master_user_id:
				ret.update( {
					fcmdevice.fcm_id: {
						'deleted': False,
						'reason': 'Token does not belong to same user in initial supplied token.'
					}
				} )
				continue
				
			fcm_id = fcmdevice.fcm_id
			
			try:
				fcm_client.device_group_remove( name, key, [fcm_id, 'ABCD123'] )
			except fcm.exceptions.FCMBadRequestError:
				ret.update( {
					fcm_id: {
						'deleted': False,
						'reason': 'Invalid device token.'
					}
				} )
				continue
				
			fcmdevice.delete_instance()
			
			ret.update( {
				fcm_id: {
					'deleted': True
				}
			} )
			
		if not FCMDevice.select().where( FCMDevice.user == master_user_id ).exists():
			user = User.get_preset_query( 'private-technical' ).for_update().where( User.user_id == master_user_id ).get()
			user.fcm_device_group_key = ''
			user.fcm_device_group_name = ''
			user.save()
			
		return ret
		"""
		
class Address( BaseModel ):
	class Meta( BaseMeta ):
		db_table = 'address'
		
		patch_permissions = {
			'address_id': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'country_sn': constants.CONST.PATCH_PERMISSION_USER,
			'administrative_sn': constants.CONST.PATCH_PERMISSION_USER,
			'sub_administrative_ln': constants.CONST.PATCH_PERMISSION_USER,
			'locality_ln': constants.CONST.PATCH_PERMISSION_USER,
			'dependent_locality_ln': constants.CONST.PATCH_PERMISSION_USER,
			'postal_code_ln': constants.CONST.PATCH_PERMISSION_USER,
			'throughfare_ln': constants.CONST.PATCH_PERMISSION_USER,
			'premise_ln': constants.CONST.PATCH_PERMISSION_USER,
			'sub_premise_ln': constants.CONST.PATCH_PERMISSION_USER,
			'latitude_pos': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'longitude_pos': constants.CONST.PATCH_PERMISSION_SYSTEM
		}
		
		dict_format = {
			constants.CONST.DICT_FORMAT_PYTHON: {
				'address_id': 'address_id',
				'country_sn': 'country_sn',
				'administrative_sn': 'administrative_sn',
				'sub_administrative_ln': 'sub_administrative_ln',
				'locality_ln': 'locality_ln',
				'dependent_locality_ln': 'dependent_locality_ln',
				'postal_code_ln': 'postal_code_ln',
				'throughfare_ln': 'throughfare_ln',
				'premise_ln': 'premise_ln',
				'sub_premise_ln': 'sub_premise_ln',
				'latitude_pos': 'latitude_pos',
				'longitude_pos': 'longitude_pos'
			},
			constants.CONST.DICT_FORMAT_JSON: {
				'address_id': 'id',
				'country_sn': 'country',
				'administrative_sn': 'administrativeArea',
				'sub_administrative_ln': 'subadministrativeArea',
				'locality_ln': 'locality',
				'dependent_locality_ln': 'dependentLocality',
				'postal_code_ln': 'postalCode',
				'throughfare_ln': 'throughfare',
				'premise_ln': 'premise',
				'sub_premise_ln': 'subpremise',
				'latitude_pos': 'latitude',
				'longitude_pos': 'longitude'
			}
		}
		
		dict_country_formats = {
			'US': [
				( 'throughfare_ln', [ 'required', 'street_number+ +route' ] ),
				( 'premise_ln', [ 'optional', '#+subpremise' ] ),
				( ',', [ 'text' ] ),
				( 'locality_ln', [ 'required', 'locality' ] ),
				( ',', [ 'text' ] ),
				( 'administrative_sn', [ 'required', 'administrative_area_level_1' ] ),
				( 'postal_code_ln', [ 'required', 'postal_code' ] ),
				( ',', [ 'text' ] ),
				( 'country_sn', [ 'required', 'country' ] ) ],
		}
		
		dict_presets = {
			'public': {
				'fields': [
					'address_id',
					'country_sn',
					'administrative_sn',
					'sub_administrative_ln',
					'locality_ln',
					'dependent_locality_ln',
					'postal_code_ln',
					'throughfare_ln',
					'premise_ln',
					'sub_premise_ln',
					'latitude_pos',
					'longitude_pos'
				],
				'expansions': [],
				'transform_method': 'transform_preset_public'
			}
		}
		
	address_id = peewee.PrimaryKeyField()
	country_sn = peewee.CharField( default = '' )
	administrative_sn = peewee.CharField( default = '' )
	sub_administrative_ln = peewee.CharField( default = '' )
	locality_ln = peewee.CharField( default = '' )
	dependent_locality_ln = peewee.CharField( default = '' )
	postal_code_ln = peewee.CharField( default = '' )
	throughfare_ln = peewee.CharField( default = '' )
	premise_ln = peewee.CharField( default = '' )
	sub_premise_ln = peewee.CharField( default = '' )
	latitude_pos = peewee.DecimalField( default = 0 )
	longitude_pos = peewee.DecimalField( default = 0 )
	
	@classmethod
	def get_public_select_items( cls ):
		return [
			cls.address_id,
			cls.country_sn,
			cls.administrative_sn,
			cls.sub_administrative_ln,
			cls.locality_ln,
			cls.dependent_locality_ln,
			cls.postal_code_ln,
			cls.throughfare_ln,
			cls.premise_ln,
			cls.sub_premise_ln,
			cls.latitude_pos,
			cls.longitude_pos ]
	
	@classmethod
	def get_public_select_query( cls ):
		return( cls.select( *cls.get_public_select_items() ) )
			
	def transform_preset_public( self, dict ):
		select_kwargs = { 'agreement_id': True, 'latitude_pos': True, 'longitude_pos': True }
		for tuple in self._meta.dict_country_formats[self.country_sn]:
			k, v = tuple[0], tuple[1][0]
			if v != 'text':
				select_kwargs.update( { k: True } )
				
		new_dict = self.to_dict( constants.CONST.DICT_FORMAT_JSON, **select_kwargs )
		new_dict.update( { 'fullAddress': self.to_str() } )
		return new_dict
		
	def check( self ):
		if not self.country_sn in self._meta.dict_country_formats:
			return False
		
		for tuple in self._meta.dict_country_formats[self.country_sn]:
			k, v = tuple[0], tuple[1][0]
			if v == 'required' and vars(self)['_data'][k] == '':
				return False
				
		return True
		
	def verify( self ):
		from geopy.geocoders import GoogleV3
		geolocator = GoogleV3()
		location = geolocator.geocode( self.to_str() )
		if location is None:
			return False
			
		raw_data = {
			'subpremise': '',
			'street_number': '',
			'route': '',
			'locality': '',
			'administrative_area_level_1': '',
			'postal_code': '',
			'country': ''
		}
			
		for component in location.raw['address_components']:
			for type in component['types']:
				if type in raw_data:
					raw_data[type] = component['short_name']
					
		for tuple in self._meta.dict_country_formats[self.country_sn]:
			k, v = tuple[0], tuple[1][0]
			if v == 'text':
				continue
			
			v = tuple[1][1]
			elements = v.split( '+' )
			build_list = []
			id = False
			for element in elements:
				if element in raw_data:
					build_list.append( raw_data[element] )
					if raw_data[element] != '':
						id = True
				else:
					build_list.append( element )
					
			if not id:
				continue
					
			setattr( self, k, ''.join( build_list ) )
			
		self.latitude_pos = location.latitude
		self.longitude_pos = location.longitude
			
		return True
		
	def to_str( self ):	
		str_list = []
		for tuple in self._meta.dict_country_formats[self.country_sn]:
			k, v = tuple[0], tuple[1][0]
			if v == 'text':
				if len( str_list ) != 0:
					str_list.pop()
					
				str_list.append( k )
				str_list.append( ' ' )
			else:
				if vars(self)['_data'][k] != '':
					str_list.append( vars(self)['_data'][k] )
					str_list.append( ' ' )
					
		if str_list[-1] == ' ':
			str_list.pop()
				
		return ''.join( str_list )
		
class Listing( BaseModel ):
	""" Class model for Listing in MySQL Database.
	
	The geohash defines an area of 1.22km x 0.61km( width x height ) while the latitude and longitude
	combination give an accuracy of about 1m x 1m( width x height ). The latitude and longitude position define
	the exact location of the listing while the geohash outlines a broad area the listing exists.
	
	Attributes:
		listing_id (int): Listing Id.
		host (User): User who owns the listing.
		status_ind (int): The current status of this listing.
		description_txt (str): A description of the listing.
		rate_amt (float): The amount per hour it costs to lease this listing in USD.
		latitude_pos (float): Latitude position of this listing.
		longitude_pos (float): Longitude position of this listing.
		geohash_pos (str): A geohash of the general location of the listing.
		
	"""
	
	class Meta( BaseMeta ):
		db_table = 'listing'
		patch_permissions = {
			'listing_id': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'host': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'status_ind': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'description_txt': constants.CONST.PATCH_PERMISSION_USER,
			'latitude_pos': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'longitude_pos': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'geohash_pos': constants.CONST.PATCH_PERMISSION_SYSTEM
		}
		
		dict_format = {
			constants.CONST.DICT_FORMAT_PYTHON: {
				'listing_id': 'listing_id',
				'host': 'host',
				'address': 'address',
				'status_ind': 'status_ind',
				'description_txt': 'description_txt',
				'geohash_pos': 'geohash_pos'
			},
			constants.CONST.DICT_FORMAT_JSON: {
				'listing_id': 'id',
				'host': 'hostId',
				'@host': 'host',
				'address': 'addressId',
				'@address': 'address',
				'status_ind': 'status',
				'description_txt': 'description',
				'geohash_pos': 'geohash'
			}
		}
		
		dict_presets = {
			'public': {
				'fields': [
					'listing_id',
					'host',
					'address',
					'status_ind',
					'description_txt'
				],
				'expansions': [
					{
						'form': ('host', {}, 'public'),
						'join_case': (User, 'user_id')
					},
					{
						'form': ('address', {}, 'public'),
						'join_case': (Address, 'address_id')
					}
				],
				'prefetch-origin': 'listing_id',
				'prefetches': [
					{
						'model': 'ListingImage',
						'key': 'prefetch_images',
						'field': 'listing',
						'preset': 'public'
					}
				],
				'transform_method': 'transform_preset_public'
			},
			'public-w/o-host': {
				'fields': [
					'listing_id',
					'address',
					'status_ind',
					'description_txt'
				],
				'expansions': [
					{
						'form': ('address', {}, 'public'),
						'join_case': (Address, 'address_id')
					}
				],
				'prefetch-origin': 'listing_id',
				'prefetches': [
					{
						'model': 'ListingImage',
						'key': 'prefetch_images',
						'field': 'listing',
						'preset': 'public'
					}
				],
				'transform_method': 'transform_preset_public'
			},
			'system': {
				'fields': [
					'listing_id',
					'host',
					'address',
					'status_ind',
					'description_txt',
					'geohash_pos'
				],
				'expansions': [
					{
						'form': ('host', {}, 'public'),
						'join_case': (User, 'user_id')
					},
					{
						'form': ('address', {}, 'public'),
						'join_case': (Address, 'address_id')
					}
				],
				'prefetch-origin': 'listing_id',
				'prefetches': [
					{
						'model': 'ListingImage',
						'key': 'prefetch_images',
						'field': 'listing',
						'preset': 'public'
					}
				],
				'transform_method': None
			},
			'delete': {
				'fields': [
					'listing_id',
				],
				'expansions': [],
				'transform_method': None
			},
		}
		
	listing_id = peewee.PrimaryKeyField()
	host = peewee.ForeignKeyField( User, related_name = 'listings' )
	address = peewee.ForeignKeyField( Address, related_name = 'listings' )
	status_ind = peewee.CharField()
	description_txt = peewee.TextField()
	geohash_pos = peewee.CharField()
	
	@staticmethod
	def get_exists( preset, where_clause = None, order_clause = None, **kwargs ):
		if not where_clause:
			where_clause = []
			
		where_clause.append( Listing.status_ind << ['Open', 'Closed'] )
			
		if not order_clause:
			order_clause = []
			
		return ( Listing
			.get_preset_models( preset, where_clause, order_clause, **kwargs )
		)
		
	@staticmethod
	def get_open( preset, where_clause = None, order_clause = None, **kwargs ):
		if not where_clause:
			where_clause = []
			
		where_clause.append( Listing.status_ind == 'Open' )
		
		if not order_clause:
			order_clause = []
			
		return ( Listing
			.get_preset_models( preset, where_clause, order_clause, **kwargs )
		)
		
	def transform_preset_public( self, dict ):
		dict.update( { 'images': [] } )
		if hasattr( self, 'prefetch_images' ):
			for image in self.prefetch_images:
				dict['images'].append( image.get_url() )
			
		return dict
		
class Vehicle( BaseModel ):
	""" Class model for vehicles of the users in database.
	
	Users own multiple vehicles, but one must be specified in the paramaters of an agreement. This allows the host to make sure only registered vehicles are parked in their venues.
	
	Attributes:
		vehicle_id (int): Vehicle Id.
		owner (User): The owner.
		license_plate_ln (str): The license plate number on the car.
		color_ln (str): Color of the car
		make_ln (str): Make of the car
		model_ln (str): Model of the car
		year_ln (str): Year of the car.
		
	"""
	
	class Meta( BaseMeta ):
		db_table = 'vehicle'
		patch_permissions = {
			'vehicle_id': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'owner': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'license_plate_ln': constants.CONST.PATCH_PERMISSION_USER,
			'color_ln': constants.CONST.PATCH_PERMISSION_USER,
			'make_ln': constants.CONST.PATCH_PERMISSION_USER,
			'model_ln': constants.CONST.PATCH_PERMISSION_USER,
			'year_ln': constants.CONST.PATCH_PERMISSION_USER
		}
		
		dict_format = {
			constants.CONST.DICT_FORMAT_PYTHON: {
				'vehicle_id': 'vehicle_id',
				'owner': 'owner',
				'license_plate_ln': 'license_plate_ln',
				'color_ln': 'color_ln',
				'make_ln': 'make_ln',
				'model_ln': 'model_ln',
				'year_ln': 'year_ln'
			},
			constants.CONST.DICT_FORMAT_JSON: {
				'vehicle_id': 'id',
				'owner': 'ownerId',
				'license_plate_ln': 'licensePlateNumber',
				'color_ln': 'color',
				'make_ln': 'make',
				'model_ln': 'model',
				'year_ln': 'year'
			}
		}
		
		dict_presets = {
			'public': {
				'fields': [
					'vehicle_id',
					'license_plate_ln',
					'color_ln',
					'make_ln',
					'model_ln',
					'year_ln',
				],
				'expansions': [],
				'transform_method': None
			}
		}
		
	vehicle_id = peewee.PrimaryKeyField()
	owner = peewee.ForeignKeyField( User, related_name = 'vehicles' )
	license_plate_ln = peewee.CharField( max_length = 64 )
	color_ln = peewee.CharField( max_length = 64 )
	make_ln = peewee.CharField( max_length = 64)
	model_ln = peewee.CharField( max_length = 64 )
	year_ln = peewee.CharField( max_length = 4 )

class Agreement( BaseModel ):
	""" Class model for Agreement in MySQL Database.
	
	And Agreement is a relational class the interacts two users, a host and a driver along with a
	listing which the host is owner of. This will house relevant chat information and other vital
	information for the two users to successfully interact
	
	Attributes:
		agreement_id (int): Agreement ID.
		host_id (int): Id of the host.
		driver_id (int): Id of the driver.
		status_ind (int): The current status of this agreement( Running, Completed, etc.. )
		creation_dtm (datetime): The date and time this agreement was formed
		finish_dtm (datetime): The date and time this agreement ended and listing was made available again
		
	"""
	
	class Meta( BaseMeta ):
		db_table = 'agreement'
		patch_permissions = {
			'agreement_id': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'host': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'driver': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'listing': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'driver_vehicle': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'cost_amt': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'status_ind': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'creation_dtm': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'end': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'start_dtm': constants.CONST.PATCH_PERMISSION_SYSTEM
		}
		
		dict_format = {
			constants.CONST.DICT_FORMAT_PYTHON: {
				'agreement_id': 'agreement_id',
				'host': 'host',
				'driver': 'driver',
				'listing': 'listing',
				'driver_vehicle': 'driver_vehicle',
				'cost_amt': 'cost_amt',
				'status_ind': 'status_ind',
				'creation_dtm': 'creation_dtm',
				'end_dtm': 'end_dtm',
				'start_dtm': 'start_dtm'
			},
			constants.CONST.DICT_FORMAT_JSON: {
				'agreement_id': 'id',
				'host': 'hostId',
				'@host': 'host',
				'driver': 'driverId',
				'@driver': 'driver',
				'listing': 'listingId',
				'@listing': 'listing',
				'driver_vehicle': 'vehicleId',
				'@driver_vehicle': 'vehicle',
				'cost_amt': 'driverCost',
				'status_ind': 'status',
				'creation_dtm': 'creationDate',
				'end_dtm': 'endDate',
				'start_dtm': 'startDate'
			}
		}
		
		dict_presets = {
			'private': {
				'fields': [
					'agreement_id',
					'host',
					'driver',
					'listing',
					'driver_vehicle',
					'status_ind',
					'creation_dtm',
					'start_dtm',
					'end_dtm'
				],
				'expansions': [
					{
						'form': ('host', {}, 'public'),
						'join_case': (Host, 'user_id'),
						'alias': True
					},
					{
						'form': ('driver', {}, 'public'),
						'join_case': (Driver, 'user_id'),
						'alias': True
					},
					{
						'form': ('listing', {}, 'public-w/o-host'),
						'join_case': (Listing, 'listing_id')
					},
					{
						'form': ('driver_vehicle', {}, 'public'),
						'join_case': (Vehicle, 'vehicle_id')
					}
				],
				'prefetch-origin': 'agreement_id',
				'transform_method': 'transform_preset_private'
			},
			'private-message': {
				'fields': [
					'agreement_id',
					'host',
					'driver'
				],
				'expansions': [
					{
						'form': ('host', {}, 'private-technical'),
						'join_case': (Host, 'user_id'),
						'alias': True
					},
					{
						'form': ('driver', {}, 'private-technical'),
						'join_case': (Driver, 'user_id'),
						'alias': True
					}
				]
			}
		}
	
	agreement_id = peewee.PrimaryKeyField()
	host = peewee.ForeignKeyField( User, related_name = 'host_agreements' )
	driver = peewee.ForeignKeyField( User, related_name = 'driver_agreements' )
	listing = peewee.ForeignKeyField( Listing, related_name = 'listing_agreements' )
	driver_vehicle = peewee.ForeignKeyField( Vehicle, related_name = 'vehicle_agreements' )
	start_dtm = peewee.DateTimeField()
	end_dtm = peewee.DateTimeField()
	cost_amt = peewee.IntegerField()
	status_ind = peewee.CharField( max_length = 10 )
	creation_dtm = peewee.DateTimeField()
	end_dtm = peewee.DateTimeField( null = True )
	
	@staticmethod
	def get_active( preset, where_clause = None, order_clause = None ):
		if not where_clause:
			where_clause = []
			
		where_clause.append( Agreement.status_ind << ['Open', 'Ongoing'] )
			
		if not order_clause:
			order_clause = []
			
		return Agreement.get_preset_models( preset, where_clause, order_clause )
	
	@staticmethod
	def start_agreements():
		unstarted_agreements = ( Agreement
			.select(
				Agreement.agreement_id,
				Agreement.start_dtm )
			.where( ( Agreement.status_ind == 'Open' ) & ( Agreement.start_dtm < datetime.datetime.utcnow() ) ) )
			
		for agreement in unstarted_agreements:
			agreement.status_ind = 'Ongoing'
			agreement.save()
			
	@staticmethod
	def referral_reward_hosts():
		import doc_models
		import stripe
		
		doc_models.connect()
		
		stripe.api_key = constants.CONST.STRIPE_KEY
		
		for user in doc_models.UserData.get_users_with_cents():
			referred = User.select( User.first_name_ln, User.last_name_ln ).where( User.user_id == user.user_id ).get()
			referrer = User.select( User.stripe_connect_id ).where( User.user_id == user.referred_data.referrer_id ).get()
			
			transfer = stripe.Transfer.create(
				amount = user.referred_data.collected_cents,
				currency = 'usd',
				destination = referrer.stripe_connect_id,
				description = 'Host Referral Program: Earnings from host %s %s' % ( referred.first_name_ln, referred.last_name_ln )
			)
			
			user.referred_data.collected_cents = 0
			user.save()
		
	@staticmethod
	def expire_agreements():
		"""Marks all agreements that are Ongoing, but past their end date as Closed"""
		import doc_models
		
		doc_models.connect()
		
		Host = User.alias()
		expired_agreements = ( Agreement
			.select(
				Agreement.cost_amt, Agreement.agreement_id, Agreement.end_dtm, Agreement.start_dtm,
				Host.user_id )
			.join( Host, on = ( Agreement.host == Host.user_id ).alias( 'host' ) )
			.switch( Agreement )
			.where( ( Agreement.status_ind == 'Ongoing' ) & ( Agreement.end_dtm < datetime.datetime.utcnow() ) ) )
			
		for agreement in expired_agreements: #expired_agreements:
			agreement.status_ind = 'Closed'
			agreement.save()
			
			host_data = doc_models.UserData.get( agreement.host.user_id )
			
			refer_credit = False
			if isinstance( host_data.referred_data, doc_models.HostReferredProgram ):
				if agreement.start_dtm < host_data.referred_data.end_dtm:
					refer_credit = True
			
			if refer_credit is True:
				our_cut = int( agreement.cost_amt * 0.25 )
				host_data.referred_data.collected_cents += int( ( our_cut - 30 - total_charge*0.029 ) / 2 )
				host_data.save()
			
	def transform_preset_private( self, dict ):
		if self.status_ind == 'Disputed':
			dispute = Dispute.get_preset_query( 'private' ).where( Dispute.agreement == self.agreement_id ).get()
			dict.update( {
				'dispute': dispute.to_dict( constants.CONST.DICT_FORMAT_JSON, preset = 'private-agreement' )
			} )
			
		return dict
		
class ChatMessage( BaseModel ):
	""" Class model for ChatMessage in MySQL Database.
	
	A chat message is an individual message between two users in an agreement. The next chronological
	message is found at next_message_id thus this operates similar to a linked list.
	
	Attributes:
		chat_message_id (int): Chat Message ID.
		next_message_id (int): The id of the next message. 0 if there is no next message.
		message_txt (str): The contents of the message.
		author_sn (str): Name of author who done said.
		status_ind (int): The current status of this message( Sent, Received, Read, etc... )
		creation_dtm (datetime): The date and time this agreement was formed
		
	"""
	
	class Meta( BaseMeta ):
		db_table = 'chat_message'
		patch_permissions = {
			'chat_message_id': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'agreement': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'message_txt': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'status_ind': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'author_sn': constants.CONST.PATCH_PERMISSION_SYSTEM,
			'creation_dtm': constants.CONST.PATCH_PERMISSION_SYSTEM
		}
		
		dict_format = {
			constants.CONST.DICT_FORMAT_PYTHON: {
				'chat_message_id': 'chat_message_id',
				'agreement': 'agreement',
				'message_txt': 'message_txt',
				'status_ind': 'status_ind',
				'author_sn': 'author_sn',
				'creation_dtm': 'creation_dtm'
			},
			constants.CONST.DICT_FORMAT_JSON: {
				'chat_message_id': 'id',
				'agreement': 'agreementId',
				'message_txt': 'messageContents',
				'status_ind': 'status',
				'author_sn': 'author',
				'creation_dtm': 'creationDate'
			}
		}
		
		dict_presets = {
			'private': {
				'fields': [
					'chat_message_id',
					'agreement',
					'message_txt',
					'status_ind',
					'creation_dtm'
				],
				'expansions': [],
				'transform_method': 'transform_preset_private'
			}
		}
		
	def transform_preset_private( self, dict ):
		del dict['agreementId']
		return dict
		
	chat_message_id = peewee.PrimaryKeyField()
	agreement = peewee.ForeignKeyField( Agreement, related_name = 'chat_messages' )
	message_txt = peewee.TextField()
	status_ind = peewee.CharField()
	author_sn = peewee.CharField()
	creation_dtm = peewee.DateTimeField()
	
class Dispute( BaseModel ):
	
	class Meta( BaseMeta ):
		dict_format = {
			constants.CONST.DICT_FORMAT_PYTHON: {
				'dispute_id': 'dispute_id',
				'agreement': 'agreement',
				'claimant': 'claimant',
				'respondent': 'respondent',
				'dispute_ind': 'dispute_ind',
				'status_ind': 'status_ind',
				'creation_dtm': 'creation_dtm'
			},
			constants.CONST.DICT_FORMAT_JSON: {
				'dispute_id': 'id',
				'agreement': 'agreementId',
				'@agreement': 'agreement',
				'claimant': 'claimantId',
				'@claimant': 'claimant',
				'respondent': 'respondentId',
				'@respondent': 'respondent',
				'dispute_ind': 'disputeType',
				'status_ind': 'status',
				'creation_dtm': 'creationDate'
			}
		}
		
		dict_presets = {
			'private': {
				'fields': [
					'dispute_id',
					'agreement',
					'claimant',
					'respondent',
					'dispute_ind',
					'status_ind',
					'creation_dtm'
				],
				'expansions': [
					{
						'form': ('agreement', {}, 'private'),
						'join_case': (Agreement, 'agreement_id'),
						'alias': True
					},
					{
						'form': ('claimant', {}, 'public'),
						'join_case': (Claimant, 'user_id'),
						'alias': True
					},
					{
						'form': ('respondent', {}, 'public'),
						'join_case': (Respondent, 'user_id'),
						'alias': True
					}
				],
				'transform_method': None
			},
			'private-agreement': {
				'fields': [
					'dispute_id',
					'claimant',
					'respondent',
					'dispute_ind',
					'status_ind',
					'creation_dtm'
				],
				'expansions': [
					{
						'form': ('claimant', {}, 'public'),
						'join_case': (Claimant, 'user_id'),
						'alias': True
					},
					{
						'form': ('respondent', {}, 'public'),
						'join_case': (Respondent, 'user_id'),
						'alias': True
					}
				],
				'transform_method': None
			}
		}
	
	dispute_id = peewee.PrimaryKeyField()
	agreement = peewee.ForeignKeyField( Agreement, related_name = 'dispute' )
	claimant = peewee.ForeignKeyField( User, related_name = 'claimant_disputes' )
	respondent = peewee.ForeignKeyField( User, related_name = 'respondent_disputes' )
	dispute_ind = peewee.CharField()
	status_ind = peewee.CharField()
	prev_agreement_status_ind = peewee.CharField()
	creation_dtm = peewee.DateTimeField()
			
class TransactionLog( BaseModel ):
	""" This class forms the ability to log all transactions that users make. This will include billings, credits, refunds, and edge case uses.
	
	"""
	
	transaction_log_id = peewee.PrimaryKeyField()
	user = peewee.ForeignKeyField( User, related_name = 'transactions' )
	resource_id = peewee.IntegerField( default = 0 )
	stripe_id = peewee.CharField( default = '' )
	type_ind = peewee.CharField()
	reason_ind = peewee.CharField()
	transaction_amt = peewee.IntegerField()
	creation_dtm = peewee.DateTimeField()
			
class UserImage( BaseModel ):
	""" This class relates S3 links to images to users.
	
	"""
	
	user_image_id = peewee.PrimaryKeyField()
	user = peewee.ForeignKeyField( User, related_name = 'profile_images' )
	key_ln = peewee.CharField()
	
	def get_url( self ):
		return 'https://%s.s3.amazonaws.com/%s' % ( 'tandem-user-images', self.key_ln )
	
class ListingImage( BaseModel ):
	""" This class relates S3 links to images to listings.
	
	"""
	
	listing_image_id = peewee.PrimaryKeyField()
	listing = peewee.ForeignKeyField( Listing, related_name = 'images' )
	key_ln = peewee.CharField()
	
	class Meta( BaseMeta ):
		dict_format = {
			constants.CONST.DICT_FORMAT_PYTHON: {
				'listing_image_id': 'listing_image_id',
				'listing': 'listing',
				'key_ln': 'key_ln'
			},
			constants.CONST.DICT_FORMAT_JSON: {
				'listing_image_id': 'id',
				'listing': 'listingId',
				'@listing': 'listing',
				'key_ln': 'key'
			}
		}
		
		dict_presets = {
			'delete': {
				'fields': [
					'listing_image_id',
					'listing',
					'key_ln'
				],
				'expansions': [],
				'transform_method': None
			},
			'public': {
				'fields': [
					'listing',
					'key_ln'
				],
				'expansions': [],
				'transform_method': None
			},
		}
	
	def get_url( self ):
		return 'https://%s.s3.amazonaws.com/%s' % ( 'tandem-listing-images', self.key_ln )
		
	@staticmethod
	def hard_delete( image_id ):
		import boto3
		
		image = ListingImage.get_preset_query( 'delete' ).where( ListingImage.listing_image_id == image_id ).get()
		
		s3client = boto3.client( 's3' )
		s3client.delete_object( Bucket = 'tandem-listing-images', Key = image.key_ln )
		
		ret = image.key_ln

		image.delete_instance()
		
		return ret
		
class DisputeImage( BaseModel ):
	""" This class relates S3 links to images to disputes.
	
	"""
	
	dispute_image_id = peewee.PrimaryKeyField()
	dispute = peewee.ForeignKeyField( Dispute, related_name = 'images' )
	party = peewee.CharField()
	key_ln = peewee.CharField()
	
	def get_url( self ):
		return 'https://%s.s3.amazonaws.com/%s' % ( 'tandem-dispute-images', self.key_ln )
		
########################################################
####												####
####			Lazy Load Class Methods				####
####												####
########################################################

def listing_available( cls, listing_id, start, end ):
	if ( Agreement
		.select( Agreement.start_dtm, Agreement.end_dtm )
		.where(
			( ( Agreement.start_dtm < end ) & ( Agreement.start_dtm >= start ) ) |
			( ( Agreement.start_dtm <= start ) & ( Agreement.end_dtm >= end ) ) |
			( ( Agreement.end_dtm > start ) & ( Agreement.end_dtm <= end ) ) |
			( ( Agreement.start_dtm >= start ) & ( Agreement.end_dtm <= end ) )
		)
		.where( Agreement.listing == listing_id, Agreement.status_ind != 'Closed' )
		.exists()
	):
		return None
	
	plan = doc_models.ListingData.get( listing_id ).gen_fixed_agreement( start, end )
	return plan
	
def listing_many_avaiable( cls, listings, start, end ):
	returns = {}
	
	agreements = ( Agreement
		.select( Agreement.start_dtm, Agreement.end_dtm, Agreement.listing )
		.where(
			( ( Agreement.start_dtm < end ) & ( Agreement.start_dtm >= start ) ) |
			( ( Agreement.start_dtm <= start ) & ( Agreement.end_dtm >= end ) ) |
			( ( Agreement.end_dtm > start ) & ( Agreement.end_dtm <= end ) ) |
			( ( Agreement.start_dtm >= start ) & ( Agreement.end_dtm <= end ) )
		)
		.where( Agreement.status_ind != 'Closed' ) )
		
	listings_w_prefetch = peewee.prefetch( listings, agreements )
	ids = [listing.listing_id for listing in listings_w_prefetch]
	datas = doc_models.ListingData.get_many( ids )
	for listing in listings_w_prefetch:
		available = None
		if len( listing.listing_agreements_prefetch ) == 0:
			available = {}
			
		if available is not None:
			try:
				available = (data for data in datas if data.listing_id == listing.listing_id).next().gen_fixed_agreement( start, end )
			except StopIteration:
				available = None
					
		returns.update( {
			listing.listing_id: available
		} )
		
	return returns, listings_w_prefetch
	
def listing_soft_delete( cls, listing_id ):
	from tandem_exceptions import ActiveAgreementsError
	
	listings = Listing.get_exists( 'delete', [Listing.listing_id == listing_id] )
	images = ListingImage.get_preset_query( 'delete' ).where( ListingImage.listing == listing_id )
	"""
	blackouts = Blackout.get_preset_query( 'delete' ).where( Blackout.listing == listing_id )
	"""
	
	listing = peewee.prefetch( listings, images )[0]
	
	for image in listing.images_prefetch:
		ListingImage.hard_delete( image.listing_image_id )
		
	listing_data = doc_models.ListingData.get( listing_id )
	listing_data.delete()
		
	listing.status_ind = 'Deleted'
	listing.save()
		
	return listing_id
	
Listing.available = classmethod( listing_available )
Listing.many_available = classmethod( listing_many_avaiable )
Listing.soft_delete = classmethod( listing_soft_delete )

########################################################
####												####
####					Dictionaries				####
####												####
########################################################

import collections

_model_list = collections.OrderedDict()
_model_list['User'] = User
_model_list['FCMDevice'] = FCMDevice
_model_list['Address'] = Address
_model_list['Listing'] = Listing
_model_list['Vehicle'] = Vehicle
_model_list['Agreement'] = Agreement
_model_list['ChatMessage'] = ChatMessage
_model_list['Dispute'] = Dispute
_model_list['TransactionLog'] = TransactionLog
_model_list['UserImage'] = UserImage
_model_list['ListingImage'] = ListingImage
_model_list['DisputeImage'] = DisputeImage
