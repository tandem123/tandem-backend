# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016


class ClaimException( Exception ):
	""" Claim Error

	"""

	pass


class TokenException( Exception ):
	""" Token Error

	"""

	pass


class TokenExpired( TokenException ):
	""" The token is expired

	"""

	pass


class TokenInvalid( TokenException ):
	""" The token is invalid

	"""

	pass
