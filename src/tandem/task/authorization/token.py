# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, April 2017

import itsdangerous
import json
import redis

import tandem.task.authorization.exceptions


class Token( object ):
	@staticmethod
	def get_token( key, token, accept=None ):
		payload = token.split( '.' )[1]
		while len( payload ) % 4 != 0:
			payload += b'='

		data = json.loads( payload.decode( 'base64' ) )
		type = data.get( 'typ', None )

		if accept is None:
			accept = ['Refresh', 'Access', 'SingleAccess']

		if type not in accept:
			raise tandem.task.authorization.exceptions.TokenInvalid(
				'Token\'s type rejected' )

		if type is None:
			raise tandem.task.authorization.exceptions.TokenInvalid(
				'Token\'s type not specified' )

		if 'id' in data:
			raise tandem.task.authorization.exceptions.TokenExpired(
				'Token has expired and is no longer valid.' )

		r_type = {
			'Refresh': RefreshToken,
			'Access': AccessToken,
			'SingleAccess': SingleUseAccessToken
		}[type]

		return r_type.loads( key, token )

	@staticmethod
	def peek( token ):
		payload = token.split( '.' )[1]
		while len( payload ) % 4 != 0:
			payload += b'='

		return json.loads( payload.decode( 'base64' ) )

	@classmethod
	def loads( cls, key, token ):
		try:
			data = cls.serializer( key ).loads( token )
		except itsdangerous.SignatureExpired:
			raise tandem.task.authorization.exceptions.TokenExpired(
				'Token has expired and is no longer valid.' )
		except itsdangerous.BadSignature:
			raise tandem.task.authorization.exceptions.TokenInvalid(
				'Token is not valid.' )

		del data['typ']

		return cls( **data )


class RefreshToken( Token ):
	serializer = itsdangerous.JSONWebSignatureSerializer

	def __init__( self, sub=None, iss=None, aud=None ):
		self.typ = 'Refresh'
		self.sub = sub
		self.iss = iss
		self.aud = aud

	def dumps( self, key ):
		return self.serializer( key ).dumps(
			{
				'typ': self.typ,
				'sub': self.sub,
				'iss': self.iss,
				'aud': self.aud
			}
		)


class AccessToken( Token ):
	serializer = itsdangerous.TimedJSONWebSignatureSerializer

	def __init__( self, role=None, sub=None, iss=None, aud=None ):
		self.typ = 'Access'
		self.role = role  # The claim or action e.g user.patch.password
		self.sub = sub  # The subject that the claim effects e.g the user id
		self.iss = iss  # The issuer, who issued this claim
		self.aud = aud  # The audience

	def dumps( self, key, expires=86400 ):
		return self.serializer( key, expires_in=expires ).dumps(
			{
				'typ': self.typ,
				'role': self.role,
				'sub': self.sub,
				'iss': self.iss,
				'aud': self.aud
			}
		)

	def matches( self, key ):
		key_s = key.split( '.' )
		role_s = self.role.split( '.' )
		i = 0
		while( key_s[i] == role_s[i] ):
			i += 1
			if i == len( role_s ):
				return True

			if i == len( key_s ):
				break

		return False

	def get_subject_id( self ):
		return int( self.sub.split( ':' )[1] )

	def __contains__( self, key ):
		return self.matches( key )


class SingleUseAccessToken( AccessToken ):
	def __init__( self, *args, **kwargs ):
		super( SingleUseAccessToken, self ).__init__( *args, **kwargs )
		self.typ = 'SingleAccess'

	def dumps( self, key, expires=86400 ):
		return self.serializer( key, expires_in=expires ).dumps(
			{
				'typ': self.typ,
				'role': self.role,
				'sub': self.sub,
				'iss': self.iss,
				'aud': self.aud
			}
		)

	@classmethod
	def loads( cls, key, token ):
		try:
			data = cls.serializer( key ).loads( token, return_header=True )
		except itsdangerous.SignatureExpired:
			raise tandem.task.authorization.exceptions.TokenExpired(
				'Token has expired and is no longer valid.' )
		except itsdangerous.BadSignature:
			raise tandem.task.authorization.exceptions.TokenInvalid(
				'Token is not valid.' )

		redis_db = redis.Redis( host='localhost', port=6379 )

		if redis_db.getset(
				'singleuse_token_bl:{}'.format( token ),
				1 ) is not None:
			raise tandem.task.authorization.exceptions.TokenExpired(
				'Token has expired and is no longer valid.' )

		redis_db.expireat(
			'singleuse_token_bl:{}'.format( token ),
			data[1]['exp'] + 2 )

		ret_data = data[0]
		del ret_data['typ']

		return cls( **ret_data )
