# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016


class TandemError( Exception ):
	""" Tandem Error

	"""

	pass


class MongoError( TandemError ):
	""" Some error working with mongo :(

	"""

	pass


class RollbackError( MongoError ):
	""" Some error in rolling back with mongo... uh oh

	"""

	pass


class OlderRollbackError( RollbackError ):
	""" We tried rolling back, but someone else with older data also rolled back,
		the assumption is our data is now completely invalid

	"""

	pass


class RuleError( TandemError ):
	""" Some error relating to of rules

	"""

	pass


class RuleInvalidId( RuleError ):
	""" Invalid id for rule

	"""

	pass


class RuleMissingAttributes( RuleError ):
	""" Rule is missing one or more attributes

	"""

	def __init__( self, missing ):
		self.missing = missing


class RuleInvalidTypeError( RuleError ):
	""" Rule has an invalid type

	"""

	pass


class RuleDateError( RuleError ):
	""" Some error relation to of rule dates

	"""

	pass


class RuleDateMissingAttributes( RuleDateError ):
	""" Rule date is missing one or more attributes

	"""

	def __init__( self, missing ):
		self.missing = missing


class RuleDateInvalidTime( RuleDateError ):
	""" Rule date time is invalid

	"""

	pass


class RuleDateInvalidRepeating( RuleDateError ):
	""" Repeating indicator invalid

	"""

	pass


class RuleDateConflictError( RuleDateError ):
	""" Rule date conflicts somewhere

	"""

	pass


class PatchError( TandemError ):
	""" An attempt to patch a model failed.

	"""

	pass


class PatchTestError( TandemError ):
	""" A test operation failed in a set of patches.

	"""

	pass


class PatchAttributeError( PatchError ):
	""" A patch is formed incorrectly or missing information.

	"""

	pass


class PatchValueError( PatchError ):
	""" A value could not be added to a model. Possibly because it can't be
	 typecasted to the model's needs or not enough permissions.

	"""

	pass


class ListingDeleteError( TandemError ):
	""" An error occured while trying to delete a listing.

	"""

	pass


class ActiveAgreementsError( ListingDeleteError ):
	""" A listing could not be deleted because it has active agreements

	"""

	pass
