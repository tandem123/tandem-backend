# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

""" Handles assigning method handles to method_names and version.

This file is to simplify legacy support by providing a dictionary for
 matching method_names and versions to a valid task method and their user
 requirements if applicable.

Attributes:
	_versioned_method_mapping (dict): A dictionary that maps versions to a
	 dictionary of method names mapped to method handles. versioned_method_user
	 requirements: A dictionary that maps versions to a dictionary of method
	 names mapped to a list of User attributes.

"""

_versioned_method_mapping = {
}


def _parametrized( dec ):
	def layer( *args, **kwargs ):
		def repl( f ):
			return dec( f, *args, **kwargs )
		return repl
	return layer


@_parametrized
def register_task( method, name=None, version=None, permissions=None ):
	def method_wrapper( *args, **kwargs ):
		return method( *args, **kwargs )

	if name is not None:
		if version is not None:
			if version not in _versioned_method_mapping:
				_versioned_method_mapping.update( { version: {} } )

			if name not in _versioned_method_mapping[version]:
				_versioned_method_mapping[version].update( { name: (method,permissions) } )

	return method_wrapper


def get_versioned_method( method_name, version ):
	""" Returns a method handle of the method that corresponds to the
	 method_name and version.

	Args:
		method_name (str): The name of the method to be looked up.
		version (str): The version of the method to be looked up.

	Returns:
		method: The method corresponding to the method_name and version.
			 Returns None otherwise.

	"""
	if version not in _versioned_method_mapping:
		return None, None

	if method_name not in _versioned_method_mapping[version]:
		return None, None

	return _versioned_method_mapping[version][method_name]
