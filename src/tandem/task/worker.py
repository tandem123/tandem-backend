# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

"""Tandem Server Worker. Processes all tasks in the rabbitmq queue.

This module contains the collection of all server side asynchronous processes
 through Celery. This includes user management and authorization. Client
 adds to a rabbitmq server for the queue of tasks to be done.

Attributes:
	tandem_celery_api (Celery): Handle for celery API.

.. _Used Modules:
	celery, http://www.celeryproject.org/
	bcrypt, http://www.mindrot.org/projects/py-bcrypt/
	itsdangerous, http://pythonhosted.org/itsdangerous/
	peewee, http://docs.peewee-orm.com/en/latest/

"""

import celery
import stripe

import tandem.task.authorization
import tandem.task.email
import tandem.task.error_responses
import tandem.tools.constants
import tandem.task.db_models
import tandem.task.task_dispatcher
import tandem.task.worker_tasks
import tandem.tools.distributed_logging

from celery_interface import *

logger = tandem.tools.distributed_logging.get_logger( 'tandem.worker' )


def pass_logging( **kwargs ):
	pass


celery.signals.setup_logging.connect( pass_logging )


def init():
	logger.info( 'Starting up worker...' )

	logger.info( 'Connecting to mysql database...' )
	try:
		tandem.task.db_models.database.connect()
	except Exception:
		logger.exception( 'Unable to connect to mysql database' )
		raise

	logger.info( 'Verifying tables...' )
	for name, model in tandem.task.db_models._model_list.items():
		if not model.table_exists():
			logger.info( 'Creating table %s', name )
			try:
				model.create_table()
			except Exception:
				logger.exception( 'Unable to create table %s', name )

	tandem.task.db_models.database.commit()

	logger.info( 'Successfully started worker' )

	stripe.api_key = tandem.tools.constants.CONST.STRIPE_KEY


def str_or_encode( var ):
	try:
		return str( var )
	except UnicodeError:
		return str.encode( 'utf-8' )

	raise ValueError


@tandem_celery_api.task( name='run_task' )
def run_task( method_name, version, auth, req_id, *args ):
	""" Celery task that is responsible for running all tasks that don't
	 need authorization.

	This method helps channel all Web API backend requests through one
	 manageable function to reduce boilerplate. This method will select the
	 correct method to execute depending on the method_name and version
	 requested. If a method matching the request is not found an error is returned
	 instead.

	Args:
		method_name (str): The name of the method to be executed.
		version (str): The version of the method to be executed.
		*args: A list of arguments that will be passed on to the executing method.

	Returns:
		Returns anything that the method would have returned. This should always be
		 a dictionary that represents a json payload and an HTTP code to match with
		 the payload.

	"""

	try:
		tandem.tools.distributed_logging.start_request( req_id )

		logger.info( 'Received task %s <v%s>', method_name, version )
		method, permission = tandem.task.task_dispatcher.get_versioned_method(
			method_name,
			version )
		if method is None:
			logger.warning( 'Task %s <v%s> not defined', method_name, version )
			return tandem.task.error_responses.create_error(
				tandem.task.error_responses.error_types.InternalServerError,
				tandem.task.error_responses.error_codes.VersionNotSupported )

		if auth is None:
			access = tandem.task.authorization.AccessToken(
				'',
				'user:0',
				'tandem:',
				'user:0' )
		else:
			auth_split = auth.split()

			if len( auth_split ) != 2:
				logger.warning( 'Authorization incorrectly formatted' )
				return tandem.task.error_responses.create_error(
					tandem.task.error_responses.error_types.Unauthorized,
					tandem.task.error_responses.error_codes.InvalidAuthFormat,
					'Bearer' )

			auth_type = auth_split[0]
			auth_token = auth_split[1]

			if auth_type != 'Bearer':
				logger.warning( 'Authorization not in bearer format' )
				return tandem.task.error_responses.create_error(
					tandem.task.error_responses.error_types.Unauthorized,
					tandem.task.error_responses.error_codes.InvalidAuthFormat,
					'Bearer' )

			try:
				access = tandem.task.authorization.Token.get_token(
					constants.CONST.SECRET_KEY, auth_token,
					['Access', 'SingleAccess'] )
			except (
					tandem.task.authorization.exceptions.TokenExpired,
					tandem.task.authorization.exceptions.TokenInvalid ):
				return tandem.task.error_responses.create_error(
					tandem.task.error_responses.error_types.Unauthorized,
					tandem.task.error_responses.error_codes.BadToken )

		if permission is not None:
			if permission not in access:
				return tandem.task.error_responses.create_error(
					tandem.task.error_responses.error_types.Forbidden,
					tandem.task.error_responses.error_codes.UserMissingPermissions )

		logger.debug( 'Executing task %s <v%s>', method_name, version )
		ret = method( access, *args )
		logger.info( 'Tasks %s <v%s> completed', method_name, version )
	except Exception as e:
		client = tandem.task.email.EmailClient(
			constants.CONST.GMAIL_USER,
			constants.CONST.GMAIL_PASS
		)
		client.send_error( e )
		raise

	return ret
