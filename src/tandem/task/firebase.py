# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

import jwt
import Crypto.PublicKey.RSA as RSA
import datetime

import tandem.tools.constants

from ..tools import distributed_logging
logger = distributed_logging.get_logger( 'tandem.worker.firebase' )


class FirebaseClient(object):
	"""object for interacting with Firebase rest api"""
	def __init__(self):
		super(FirebaseClient, self).__init__()
		self.private_key = RSA.importKey(
			'-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgE'
			'AAoIBAQCypSye87DmTZxj\nyREwEezRbB4vGFJUuHWM5l0OYKHAEqWUnWgtkvJKkJ5+3pgOm'
			'Hf0tNnKTp33EgoA\nv3XHvrLJ3W/NaJUxXwNxL+Pu1JZrNmn4jZf0El3mQotc4odcEsyPcAt'
			'e3Od6PUFO\nzb9chKrB0wiuAGE5Vyhe8AGLOw/x0v+nsszOGGAIAUkBZPvHEl/O2TOMQoO/F'
			'HjD\n5BocBXj2PYDuIHH5t2XyaQLKt8Bxdf3wlay2axpWc4LTnle/7WB137D1GsLSxZ6S\n0'
			'I1EQ/u2ZlWjXJlc07ToVLDcH8vSRyhzD9SVUP+cKmPzk+qPiCdf3I7HELsAa6nQ\npMBD3g+'
			'1AgMBAAECggEAX0etdtGuhgba5HW3DxZgYonxx1iHgNwpBg9Pjn2Hh7Pq\nUEtr5vK6+34Kq'
			'sv5+b7kK8XRaw9nPQGICEIVhYV2QigymzGucPimE1ZhkV4dI+2P\ns5uBchHhtw/C2LWRUFB'
			'OfipaRiw34YCONoOSDrxLfrkZpgRZ1oVf6NLFYAcOSAFG\nKvjBhTnDRnIcAcJqlEh68icYX'
			'xfpjdWaOBR55INJyE+wYvnpS8jvCR4ewVUpg8a0\nvuLJhBosu90Q9aX+ztjuIkMrThkEcbq'
			'MI8LZjzTEZLVa/xeL6MBkH1fcQx2pwevr\nXavLSz3Q82+5WYItsAPNR3ZaE4Cmzrxag2dwE'
			'XesuQKBgQDtmWgX3gBdiOlsGua0\nDMO8AHMa1U3ulhbabGdjruxHj8l6mhiVeCQ6rSGtNCq'
			'nE7zrR/gkRF6wwvxC8hqJ\n3tjYD8ystJfoghR5plb3IV887Ry53EaCjFOktmuS8R3xUtGHV'
			'122zjYxOkaC90Gb\n3OwAzy0jyU3/NeKfSErqkXvVuwKBgQDAevUOr2oMbRh1y5jGjuIequR'
			'moAPMCGBM\n7nUJj1ncZt4C4ZIIQuypRVQqsEm6QDw5xXXZXkL0bCcL8vuTfKCaTeVVTcPbG'
			'cjf\n6mNA5nRIPstRMAxxineApfNnDCQjPfTwU5zXMmvSZr+PpuJcKFpX2IkWhLDrZqwv\n0'
			'Cenua4hTwKBgQDtfKKhCJNQ6qJ2HcVvPjr+1Ww36/EDgx3AoQ4Ogy9EsCHw28mq\nzPVZeOf'
			'qWfQwH/h7K+qJRZVoFN1UhO2l1Qrlm5U3IyObkTmqBmtyhPbOzSojVFtd\n7alFP9jyfPMy3'
			'Vb1cowHRsLAHcGQ8lev1PhILsytsm61o0SrdPT81QH0NQKBgGBy\nfC7SmNoHJHWKAlCkMgv'
			'Z577AuYZYQCDsb7rNmOKGrQJWkz0Og9XBxo3pbdgk7cio\n+AdmYzhkpSAorgTWMwd+tsaKw'
			'/QatHv2z6sqjuPQi0JwSBvwnTbacN+Wxtjd58kw\nrGtseXihQaWyosQ4+CQCfZbKAHjEJs2'
			'CiQxTTzyjAoGAeSxMna+CogF0dOEbChG2\nZwWSBB8LPbScIpMV2GHpcx0FEdP7El3r92rNP'
			'zxTkciAYgDGjxImxYPlp4GA4+uL\nCjoMH/GoOlek3tr9wP67TVroZpYc2pHlBEzUqdyQ1Vw'
			'VIwzUZRWwJqwpFvMMHg5x\n9ltBf7zRkIGob9LOsNliB3M=\n-----END PRIVATE KEY---'
			'--\n' )
		self.service_account_email = (
			'firebase@tandem-parking.iam.gserviceaccount.com' )

	def create_auth_token(self, user_id):
		payload = {
			"iss": self.service_account_email,
			"sub": self.service_account_email,
			"aud": (
				'https://identitytoolkit.googleapis.com/google.identity.identitytool'
				'kit.v1.IdentityToolkit' ),
			"uid": ''.join(
				[
					'deb2' if tandem.tools.constants.CONST.DEBUG_MODE else '1eed',
					hex( user_id ).rjust( 28, '0' )
				] ),
			"claims": {
				"user_id": user_id
			}
		}
		exp = datetime.timedelta( minutes=60 )
		try:
			return jwt.generate_jwt( payload, self.private_key, 'RS256', exp )
		except Exception:
			logger.exception( 'Error creating custom token' )
			return None
