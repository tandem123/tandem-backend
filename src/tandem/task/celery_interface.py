# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

import celery
from ..tools import constants

tandem_celery_api = celery.Celery( 'tandem_tasks', broker='amqp://' )
tandem_celery_api.conf.update(
	CELERY_TASK_SERIALIZER='json',
	CELERY_ACCEPT_CONTENT=['json'],
	CELERY_RESULT_SERIALIZER='json',
	CELERY_RESULT_BACKEND=constants.CONST.REDIS_HOST + ':6379' )
