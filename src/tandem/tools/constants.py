# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

""" File represents any constants throughout the entire Tandem Backend API.
"""

import toml

with open( '/etc/tandem/conf.d' ) as conffile:
	_cfg = toml.loads( conffile.read() )


def _constant( f ):
	def fset( self, value ):
		raise TypeError

	def fget( self ):
		return f()

	return property( fget, fset )


class _Const( object ):

	@_constant
	def DEBUG_MODE():
		try:
			return _cfg['runtime'].get( 'debug_mode', True )
		except KeyError:
			return True

	@_constant
	def SECRET_KEY():
		try:
			return _cfg['runtime'].get( 'secret_key', '' )
		except KeyError:
			return ''

	@_constant
	def MYSQL_HOST():
		try:
			return _cfg['databases']['mysql'].get( 'host', '' )
		except KeyError:
			return ''

	@_constant
	def MYSQL_PORT():
		try:
			return _cfg['databases']['mysql'].get( 'port', 0 )
		except KeyError:
			return 0

	@_constant
	def MYSQL_USER():
		try:
			return _cfg['databases']['mysql'].get( 'user', '' )
		except KeyError:
			return ''

	@_constant
	def MYSQL_PASS():
		try:
			return _cfg['databases']['mysql'].get( 'password', '' )
		except KeyError:
			return ''

	@_constant
	def MYSQL_DB():
		try:
			return _cfg['databases']['mysql'].get( 'database', '' )
		except KeyError:
			return ''

	@_constant
	def REDIS_HOST():
		try:
			return _cfg['databases']['redis'].get( 'host', '' )
		except KeyError:
			return ''

	@_constant
	def REDIS_PORT():
		try:
			return _cfg['databases']['redis'].get( 'port', 0 )
		except KeyError:
			return 0

	@_constant
	def MONGO_HOST():
		try:
			return _cfg['databases']['mongo'].get( 'host', '127.0.0.1' )
		except KeyError:
			return '127.0.0.1'

	@_constant
	def MONGO_PORT():
		try:
			return _cfg['databases']['mongo'].get( 'port', 27017 )
		except KeyError:
			return 27017

	@_constant
	def MONGO_USER():
		try:
			return _cfg['databases']['mongo'].get( 'user', '' )
		except KeyError:
			return ''

	@_constant
	def MONGO_PASS():
		try:
			return _cfg['databases']['mongo'].get( 'password', '' )
		except KeyError:
			return ''

	@_constant
	def MONGO_DB():
		try:
			return _cfg['databases']['mongo'].get( 'database', '' )
		except KeyError:
			return ''

	@_constant
	def RABBITMQ_HOST():
		try:
			return _cfg['databases']['rabbitmq'].get( 'host', '' )
		except KeyError:
			return ''

	@_constant
	def FACEBOOK_APP_ID():
		try:
			return _cfg['external_apis']['facebook'].get( 'app_id', '' )
		except KeyError:
			return ''

	@_constant
	def FACEBOOK_APP_OAUTH():
		try:
			return _cfg['external_apis']['facebook'].get( 'app_oauth', '' )
		except KeyError:
			return ''

	@_constant
	def FCM_API_KEY():
		try:
			return _cfg['external_apis']['firebase'].get( 'api_key', '' )
		except KeyError:
			return ''

	@_constant
	def FCM_SENDER_ID():
		try:
			return _cfg['external_apis']['firebase'].get( 'sender_id', '' )
		except KeyError:
			return ''

	@_constant
	def STRIPE_KEY():
		try:
			return _cfg['external_apis']['stripe'].get( 'key', '' )
		except KeyError:
			return ''

	@_constant
	def GMAIL_USER():
		try:
			return _cfg['external_apis']['gmail'].get( 'user', '' )
		except KeyError:
			return ''

	@_constant
	def GMAIL_PASS():
		try:
			return _cfg['external_apis']['gmail'].get( 'pass', '' )
		except KeyError:
			return ''

	@_constant
	def DICT_FORMAT_JSON():
		return 0x00000000

	@_constant
	def DICT_FORMAT_PYTHON():
		return 0x00000001

	@_constant
	def MINIMUM_TRANSACTION():
		return 350

	@_constant
	def API_VERSION():
		return '0.0.1'

	@_constant
	def DEFAULT_TIMEOUT():
		return 5

	@_constant
	def PROTECTED_ARGUMENT():
		return 'tandem_protected_argument_akfasnfsngoiu54yiory'

	@_constant
	def PATCH_PERMISSION_SYSTEM():
		return 1

	@_constant
	def PATCH_PERMISSION_USER():
		return 2

	@_constant
	def PATCH_PERMISSION_ALL():
		return 3


CONST = _Const()
