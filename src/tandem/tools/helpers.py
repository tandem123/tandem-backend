# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, February 2017

import dateutil.parser
import pytz


def datetime_str2naive( datestr ):
	date = dateutil.parser.parse( datestr )
	return date.astimezone( pytz.utc ).replace( tzinfo=None )


def datetime_naive2aware( date ):
	return date.replace( tzinfo=pytz.utc )
