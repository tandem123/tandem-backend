# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

import logging
import logging.handlers

_active_req_id = 'None'
_setup = False


def start_request( req_id ):
	global _active_req_id
	_active_req_id = req_id


def get_request():
	return _active_req_id


class _RequestFilter( logging.Filter ):
	def filter( self, record ):
		record.reqid = _active_req_id
		return True


def _logging_setup():
	logger = logging.getLogger()
	logger.setLevel( logging.DEBUG )
	socketHandler = logging.handlers.SocketHandler(
		'localhost',
		logging.handlers.DEFAULT_TCP_LOGGING_PORT )
	socketHandler.addFilter( _RequestFilter() )
	logger.addHandler( socketHandler )


def get_logger( name ):
	global _setup
	if not _setup:
		_setup = True
		_logging_setup()

	return logging.getLogger( name )
