# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

import functools

import uuid
import flask

import tandem.tools.distributed_logging


def prepare_endpoint( method ):
	@functools.wraps( method )
	def method_wrapper( *args, **kwargs ):
		tandem.tools.distributed_logging.start_request( uuid.uuid4().hex )
		logger = tandem.tools.distributed_logging.get_logger( 'tandem.web' )
		logger.info( 'Request recieved at endpoint {}[{}]'.format(
			flask.request.base_url,
			flask.request.method ) )
		ret = method( *args, **kwargs )
		logger.info( 'Response sent' )
		return ret

	return method_wrapper
