# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

import flask


def get_flask():
	import tandem.web.users
	import tandem.web.listings
	import tandem.web.tasks
	import tandem.web.agreements
	import tandem.web.authorization

	tandem_web_api = flask.Flask( __name__, )

	tandem_web_api.register_blueprint(
		tandem.web.users.users_bp,
		url_prefix='/v1' )

	tandem_web_api.register_blueprint(
		tandem.web.listings.listings_bp,
		url_prefix='/v1' )

	tandem_web_api.register_blueprint(
		tandem.web.tasks.tasks_bp,
		url_prefix='/v1' )

	tandem_web_api.register_blueprint(
		tandem.web.agreements.agreements_bp,
		url_prefix='/v1' )

	tandem_web_api.register_blueprint(
		tandem.web.authorization.authorization_bp,
		url_prefix='/v1' )

	return tandem_web_api
