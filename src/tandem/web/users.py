# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

import flask

import tandem.web.worker_broker
import tandem.web.web_logging
import tandem.tools.constants

users_bp = flask.Blueprint( 'users_bp', __name__ )


@users_bp.route( '/users', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def new_user():
	"""Create a new user. Be default all new users start as drivers. Either a
	facebook id or an email/password combination can be supplied.

	.. :quickref: User; Create a new user.

	.. note::

		.. versionadded:: 1.0.0

	**Example request**:

	.. sourcecode:: http

		POST /v1/users HTTP/1.1
		Host: api.tandemparking.co
		Accept: application/json
		Content-Type: application/json

		{
			"email": "driver@tandemparking.co",
			"password": "password",
			"firstName": "Tandem",
			"lastName": "Driver"
		}

	**Example response**:

	.. sourcecode:: http

		HTTP/1.1 201 OK
		Content-Type: application/json

		{
			"accountType": "Driver",
			"authToken": "eiaWF0joxNDkzQ.eyJpZCI6H0.lxAH9u6r",
			"creationDate": "2012-03-25T08:30:11+00:00",
			"email": "driver@tandemparking.co",
			"facebookId": "",
			"firebaseAuthToken": "OiAiUlMyNT",
			"firstName": "Tandem",
			"id": 1,
			"lastName": "Driver",
		}

	:<json string email: Email user wants to register with;
		 must not be already registered
	:<json string password: Password user wants to secure account with
	:<json string facebookId: If logging in through facebook,
		 the user's facebook id
	:<json string facebookAccessToken: If logging through facebook,
		 the user's facebook access token
	:<json string firstName: The user's first name
	:<json string lastName: The user's last name
	:<json string referralCode: A referral code the user wants to sign up with

	:>json int id: User's id
	:>json string accountType: Account type; either "Driver" or "Host"
	:>json string authToken: Authorization token to be used against all other
		 endpoints
	:>json string firebaseAuthToken: Authorization token for firebase
	:>json string facebookId: User's facebook id, if they registered through
		 facebook
	:>json string email: User's email address
	:>json string firstName: User's first name
	:>json string lastName: User's last name
	:>json string creationDate: User's creation date in iso-8601 formatting

	:status 201: User successfully created
	:status 400:

		* FieldsMissing - Some or all fields are missing
		* InvalidPassword - Supplied password is invalid
		* InvalidAccessToken - User's facebookAccessToken is invalid
		* UserExists - User already registered with the supplied email

	"""

	email = flask.request.json.get( 'email' )
	password = flask.request.json.get( 'password' )
	fb_id = flask.request.json.get( 'facebookId' )
	fb_token = flask.request.json.get( 'facebookAccessToken' )
	first_name = flask.request.json.get( 'firstName' )
	last_name = flask.request.json.get( 'lastName' )
	referral_code = flask.request.json.get( 'referralCode' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_new_user',
		'1',
		None,
		email,
		fb_id,
		password,
		fb_token,
		first_name,
		last_name,
		referral_code )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/<int:user_id>', methods=['PATCH'] )
@tandem.web.web_logging.prepare_endpoint
def update_user( user_id ):
	"""Makes changes to a user following the JSON Patch format. Only replace
	 and test operations are supported and they can only affect the user and
	 not any childen models. Returns only the fields that are changed if
	successful.

	.. :quickref: User; Updates a user's fields.

	.. note::

		.. versionadded:: 1.0.0

	**Example Request**:

	.. sourcecode:: http

		PATCH /v1/users/1 HTTP/1.1
		Host: api.tandemparking.co
		Authorization: Bearer {accessToken}
		Accept: application/json

		[
			{
				"op": "replace",
				"path": "firstName",
				"value": "NewName"
			}
		]

	**Example Response**:

	.. sourcecode:: http

		HTTP/1.1 200 OK
		Content-Type: application/json

		{
			"firstName": "NewName"
		}

	:reqheader Authorization: Access token in bearer format

	**Request JSON Object**:

		A list of all JSON Patch operations to be performed on the user

	**Response JSON Object**:

		A dictionary of all fields changed and their new value

	:status 200: User successfully updated
	:status 401:

		* UserMissingPermissions - You do not have permissions to make
			 the requested patch

	:status 400:

		* InvalidPatch - The supplied patch is either malformed or contains
			 an invalid path
		* PatchFailedTest - One or more test operations failed

	:status 404:

		* UserDoesNotExist - The requested user does not exist

	"""

	auth = flask.request.headers.get( 'Authorization' )

	patches = flask.request.json

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_update_user',
		'1',
		auth,
		user_id,
		patches )
	return flask.jsonify( msg ), code


@users_bp.route( '/users', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_all_users():
	"""Retrieves the entire user collection

	.. :quickref: User; Gets the user collection.

	.. note::

		.. versionadded:: 1.0.0

	**Example Request**:

	.. sourcecode:: http

		GET /v1/users HTTP/1.1
		Host: api.tandemparking.co
		Authorization: Bearer {accessToken}
		Accept: application/json

	**Example Response**:

	.. sourcecode:: http

		HTTP/1.1 200 OK
		Content-Type: application/json

		{
			"firstName": "NewName"
		}

	:reqheader Authorization: Access token in bearer format

	:>json int id: The user's id
	:>json string accountType: The user's account type
	:>json string firstName: The user's first name
	:>json string lastName: The user's first name

	:status 200: User successfully updated
	:status 401:

		* UserMissingPermissions - User does not have permissions to make this
			 request

	:status 400:

		* InvalidUserId - The supplied user id isn't valid

	:status 404:

		* UserDoesNotExist - The requested user does not exist

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_user',
		'1',
		auth,
		-1 )

	return flask.jsonify( msg ), code


@users_bp.route('/users/<int:user_id>/listings', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_all_user_listings( user_id ):
	"""Retrieves all of a `user`'s listings

	.. :quickref: User; Retrieves all user's listings.

	**Example Request**:

	.. sourcecode:: http

		GET /users/2/listings HTTP/1.1
		Host: api.tandemparking.co
		Authorization: Bearer {accessToken}
		Accept: application/json

	**Example Response**:

	.. sourcecode:: http

		HTTP/1.1 200 OK
		Content-Type: application/json

		[
			{
				"id": 1,
				"description": "A beautiful description of a beautiful* parking lot.",
				"status": "Open",
				"images": ["https://tandem-listing-images.s3.amazonaws.com/
				img_d4e9e80e2c280a"],
				"address": {
					"country": "US",
					"administrativeArea": "CA",
					"locality": "Goleta",
					"postalCode": 93117,
					"throughfare": "6715 Pasado Rd",
					"premise": "",
					"fullAddress": "6715 Pasado Rd, Goleta, CA 93117 US",
					"latitude": 34.41142,
					"longitude": -119.86325
				}
			}
		]

	:reqheader Authorization: Access token in bearer format

	**Errors**:

	:UserMissingPermissions: You do not have permissions to retrieve this user
	:InvalidUserId: Supplied user id is not a valid id
	:UserDoesNotExist: The requested user does not exist

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_user_listing',
		'1',
		auth,
		user_id,
		-1 )

	return flask.jsonify( msg ), code


@users_bp.route( '/users/<int:user_id>', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_user( user_id ):
	""".. http:get:: /users/(int:user)

	Retrieves public information of `user`.

	**Example Request**:

	.. sourcecode:: http

		GET /users/2 HTTP/1.1
		Host: api.tandemparking.co
		Authorization: Bearer {accessToken}
		Accept: application/json

	**Example Response**:

	.. sourcecode:: http

		HTTP/1.1 200 OK
		Content-Type: application/json

		{
			"accountType": "Host",
			"creationDate": "2012-03-25T08:30:11+00:00",
			"facebookId": "",
			"firstName": "Tandem",
			"id": 2,
			"lastName": "Host",
			"profilePicture": "https://tandem-user-images.s3.amazonaws.com/img_321"
		}

	:Request Header Authorization: Access token in bearer format

	**Errors**:

	:UserMissingPermissions: You do not have permissions to retrieve this user
	:InvalidUserId: Supplied user id is not a valid id
	:UserDoesNotExist: The requested user does not exist

	**Permissions**:

	Anyone who has been authenticated and has a access token can freely
	 lookup the information of any other
	user.

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_user',
		'1',
		auth,
		user_id )

	return flask.jsonify( msg ), code


@users_bp.route( '/users/auth', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def auth_user():
	"""Takes user credentials in "Basic" format and returns an authorization
	 token if the credentials are valid. Also accepts a facebook access token
	 for facebook logins.

	.. note::

		.. deprecated:: 1.1.0
			Use /authorization/user instead.

		.. versionadded:: 1.0.0

	**Example request**:

	.. sourcecode:: http

		GET /users/auth HTTP/1.1
		Host: api.tandemparking.co
		Authorization: Basic dXNlcjpwYXNz
		Accept: application/json

	**Example response**:

	.. sourcecode:: http

		HTTP/1.1 200 OK
		Content-Type: application/json

		{
			"accountType": "Driver",
			"authToken": "eiaWF0joxNDkzQ.eyJpZCI6H0.lxAH9u6r",
			"creationDate": "2012-03-25T08:30:11+00:00",
			"email": "driver@tandemparking.co",
			"facebookId": "",
			"firebaseAuthToken": "OiAiUlMyNT",
			"firstName": "Tandem",
			"id": 1,
			"lastName": "Driver",
			"phoneNumber": "+1(000) 0101",
			"profilePicture": "https://tandem-user-images.s3.amazonaws.com/img_123"
		}

	:reqheader Authorization: User credentials in "Basic" formatting

	:<json string facebookAccessToken: Facebook provided access token

	:>json int id: User's id
	:>json string accountType: Account type; either "Driver" or "Host"
	:>json string authToken: Authorization token to be used against all
		 other endpoints
	:>json string firebaseAuthToken: Authorization token for firebase
	:>json string facebookId: User's facebook id, if they registered
		 through facebook
	:>json string email: User's email address
	:>json string firstName: User's first name
	:>json string lastName: User's last name
	:>json string phoneNumber: User's phone number
	:>json string creationDate: User's creation date in iso-8601 formatting
	:>json string profilePicture: Link to user's profile picture

	:status 201: User successfully verified
	:status 401:

		* AuthHeaderMissing - Authorization header is missing
		* InvalidAuthFormat - Authorization is not in a valid format
		* BadCredentials - User with the supplied credentials does not exist
		* FieldsMissing - Some or all fields are missing

	"""

	auth = flask.request.headers.get( 'Authorization' )

	fb_token = flask.request.args.get( 'facebookAccessToken' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_auth_user',
		'1',
		None,
		auth,
		fb_token )

	return flask.jsonify( msg ), code


@users_bp.route( '/users/promote', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def promote_user():
	"""Promotes a user from Driver to Host.

	URI:
		http://api.tandemparkingco.com/vX.X.X/users/promote

	HTTP Verb:
		POST

	Request Headers:
		Authorization: The auth token of the driver should be supplied
			 in bearer format.

	Request JSON::

		{
			"phoneNumber": (str)
		}

	Response JSON::

		"data": {
			"accountType": (str),
			"phoneNumber": (str)
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	phone_number = flask.request.json.get( 'phoneNumber' )

	country = flask.request.json.get( 'country' )
	administrative_area = flask.request.json.get( 'administrativeArea' )
	sub_administrative_area = flask.request.json.get( 'subadministrativeArea' )
	locality = flask.request.json.get( 'locality' )
	dependent_locality = flask.request.json.get( 'dependentLocality' )
	postal_code = flask.request.json.get( 'postalCode' )
	throughfare = flask.request.json.get( 'throughfare' )
	premise = flask.request.json.get( 'premise' )
	sub_premise = flask.request.json.get( 'subpremise' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_promote_user',
		'1',
		auth,
		phone_number,
		country,
		administrative_area,
		sub_administrative_area,
		locality,
		dependent_locality,
		postal_code,
		throughfare,
		premise,
		sub_premise,
		timeout=5 )

	return flask.jsonify( msg ), code


@users_bp.route( '/users/password_reset', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def user_reset_password():
	"""Creates a reset request for a user of the specified email. If no
	 such user exists then a error is returned.

	The request will email instructions for resetting their password to the
	 user's registered email.

	URI:
		http://api.tandemparkingco.com/vX.X.X/users/password_reset

	HTTP Verb:
		POST

	Request JSON::

		{
			"email": (str)
		}

	Response JSON::

		"data": {
			"email": (str),
			"firstName": (str),
			"lastName": (str)
		}

	"""

	email = flask.request.json.get( 'email' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_user_reset_password',
		'1',
		None,
		email )

	return flask.jsonify( msg ), code


@users_bp.route( '/users/referral', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def join_referral_program():
	auth = flask.request.headers.get( 'Authorization' )

	program = flask.request.json.get( 'program' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_join_referral_program',
		'1',
		auth,
		program )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/referral/code', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_user_referral_code():
	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_user_referral_code',
		'1',
		auth )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/vehicles', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def new_user_vehicle():
	"""Creates a new vehicle for a user. Called at at
	/tandem/api/vX.X.X/users/auth with POST.

	URI:
		http://api.tandemparkingco.com/vX.X.X/users/vehicles

	HTTP Verb:
		POST

	Adds a new vehicle for a user to be used for arranging parking with hosts.

	Request Headers:
		Authorization: The auth token of the driver should be supplied in bearer
		 format.

	Request JSON::

		{
			"licensePlateNumber": (str),
			"color": (str),
			"make": (str),
			"model": (str),
			"year": (str)
		}

	Response JSON::

		"data": {
			"id": (int),
			"ownerId": (int),
			"licensePlateNumber": (str),
			"color": (str),
			"make": (str),
			"model": (str),
			"year": (str)
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	license_plate = flask.request.json.get( 'licensePlateNumber' )
	color = flask.request.json.get( 'color' )
	make = flask.request.json.get( 'make' )
	model = flask.request.json.get( 'model' )
	year = flask.request.json.get( 'year' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_new_user_vehicle',
		'1',
		auth,
		license_plate,
		color,
		make,
		model,
		year )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/vehicles', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_user_vehicles():
	"""Retrieves all of a users vehicles.

	URI:
		http://api.tandemparkingco.com/vX.X.X/users/vehicles

	HTTP Verb:
		GET

	Request Headers:
		Authorization: The auth token of the driver should be supplied
		 in bearer format.

	Response JSON::

		data: [
			{
				"id": (int),
				"ownerId": (int),
				"licensePlateNumber": (str),
				"color": (str),
				"make": (str),
				"model": (str),
				"year": (str)
			},
			...
		]

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_user_vehicles',
		'1',
		auth )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/vehicles/<int:vehicle_id>', methods=['PATCH'] )
@tandem.web.web_logging.prepare_endpoint
def update_user_vehicle( vehicle_id ):
	"""Modifys a current vehicle with a JSON Patch payload. Returns only the
	 attributes that were affected by the patch and their
	new value.

	URI:
		http://api.tandemparkingco.com/vX.X.X/users/vehicles/{id}

	HTTP Verb:
		PATCH

	Request Headers:
		Authorization: The auth token of the driver should be supplied in bearer
		 format.

	Request JSON::

		{
			"licensePlateNumber": (str,optional),
			"color": (str,optional),
			"make": (str,optional),
			"model": (str,optional),
			"year": (str,optional)
		}

	Response JSON::

		"data": {
			"licensePlateNumber": (str,optional),
			"color": (str,optional),
			"make": (str,optional),
			"model": (str,optional),
			"year": (str,optional)
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	patches = flask.request.json

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_update_user_vehicle',
		'1',
		auth,
		vehicle_id,
		patches )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/payments', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def new_user_payment_method():
	"""Creates a new payment method for a user. Called at
	 /tandem/api/vX.X.X/users/payments with POST.

	URI:
		http://api.tandemparkingco.com/vX.X.X/users/payments

	HTTP Verb:
		POST

	Request Headers:
		Authorization: The auth token of the driver should be supplied in
		 bearer format.

	Request JSON::

		{
			"token": (str)
		}

	Response JSON::

		"data": (str)

	"""

	auth = flask.request.headers.get( 'Authorization' )

	token = flask.request.json.get( 'token' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_new_user_payment_method',
		'1',
		auth,
		token,
		timeout=5 )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/payments', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_user_payment_methods():
	"""Interfaces with Stripe API to retrieve all payment
	 methods registered by the user

	URI:
		http://api.tandemparkingco.com/vX.X.X/users/payments

	HTTP Verb:
		GET

	Request Headers:
		Authorization: The auth token of the driver should be supplied in bearer
		 format.


	Response JSON::

		"data": {
		"defaultCardId": (str),
		"cards": (list)
		}

		for format see (https://stripe.com/docs/api?lang=python#list_cards)
	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_user_payment_methods',
		'1',
		auth )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/payments/<path:card_id>', methods=['DELETE'] )
@tandem.web.web_logging.prepare_endpoint
def delete_user_payment_method( card_id ):
	"""Delete's the payment method of the user from Stripe

	URI:
		http://api.tandemparkingco.com/vX.X.X/users/payments/<card_id>

	HTTP Verb:
		DELETE

	Request Headers:
		Authorization: The auth token of the driver should be supplied in bearer
		 format.


	Response JSON::

		"data" : { "deleted" : (int: 1 or 0), "id" : (str: card_id) }
	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_delete_user_payment_method',
		'1',
		auth,
		card_id )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/payments/default', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_user_default_payment_method():
	"""Retrieves the default payment method of the user

	URI:
		http://api.tandemparkingco.com/vX.X.X/users/default_payment

	HTTP Verb:
		GET

	Request Headers:
		Authorization: The auth token of the driver should be supplied in bearer
		 format.


	Response JSON::

		"data" : (dict) for format see
			(https://stripe.com/docs/api?lang=python#list_cards)
	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_user_default_payment_method',
		'1',
		auth )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/payments/default', methods=['PATCH'] )
@tandem.web.web_logging.prepare_endpoint
def update_user_default_payment_method():
	"""Sets user's default payment method to the parameterized card

	URI:
		http://api.tandemparkingco.com/vX.X.X/users/default_payment

	HTTP Verb:
		GET

	Request Headers:
		Authorization: The auth token of the driver should be supplied in
		 bearer format.

	Request JSON::

		{
			"cardId": (str)
		}

	Response JSON::

		"data" : { "cardId": (str) } // default payment method id
	"""

	auth = flask.request.headers.get( 'Authorization' )

	card_id = flask.request.json.get( 'cardId' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_update_user_default_payment_method',
		'1',
		auth,
		card_id )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/payments/transfer', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def new_user_transfer():
	auth = flask.request.headers.get( 'Authorization' )

	card_id = flask.request.json.get( 'cardId' )
	amount = flask.request.json.get( 'amount' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_new_user_transfer',
		'1',
		auth,
		card_id,
		amount )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/devices', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def user_add_device():
	auth = flask.request.headers.get( 'Authorization' )

	tokens = flask.request.json.get( 'registrationToken' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_user_add_device',
		'1',
		auth,
		tokens )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/devices/<path:token>', methods=['DELETE'] )
@tandem.web.web_logging.prepare_endpoint
def user_delete_device( token ):
	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_user_delete_device',
		'1',
		auth,
		token )
	return flask.jsonify( msg ), code


@users_bp.route( '/users/images', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def user_add_image():
	auth = flask.request.headers.get( 'Authorization' )

	file_type = flask.request.json.get( 'fileType' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_user_add_image',
		'1',
		auth,
		file_type )
	return flask.jsonify( msg ), code
