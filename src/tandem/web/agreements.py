# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

"""API for dealing with manipulation of agreements at
/tandem/api/v1.0/agreements/.

Agreements are held between hosts and drivers. They contain all
the information related to a single instance of a driver leasing
 a parking space from host. This will include the driver's
 vehicle information and the hosts's listing information. Chat
messages are also tied to agreements and allow drivers and hosts
 to maintain a conversation throughout their agreement.

Attributes:
	agreements_bp (Bluprint): Blueprint that allows the agreement
		 program to link into the main web API.

"""

# Import web api related functions
import flask

import tandem.web.worker_broker
import tandem.web.web_logging

agreements_bp = flask.Blueprint( 'agreements_bp', __name__ )


@agreements_bp.route( '/agreements', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def new_agreement():
	"""Creates a new agreement.

	Agreements are created by logged in drivers( they have an auth
	 token ) when they have a listing they would like to start an
	  agreement with. On success the information of the agreement is returned.
	if an error occurs an error is returned instead.

	URI:
		https://api.tandemparking.co/v1/agreements

	HTTP Verb:
		POST

	Request Headers:
		Authorization: Bearer Token

	Request JSON::

		{
			"listingId": (int),
			"vehicleId": (int),
			"card": (str, optional), /* The card associated with the driver
				 to be used to make the payment. If none is supplied then the
				 user's default card is used. */
			"start": (str), /* Datetime of the desired start in ISO-8601 formatting */
			"end": (str) /* Datetime of the desired end in ISO-8601 formatting */
		}

	Response JSON::

		{
			"id": (int),
			"hostId": (int),
			"driverId": (int),
			"listingId": (int),
			"timesliceId": (int),
			"vehicleId": (int),
			"status": (str),
			"creationDate": (date),
			"finishDate": (date),
			"chargeAmount": (int)
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )
	listing_id = flask.request.json.get( 'listingId' )
	vehicle_id = flask.request.json.get( 'vehicleId' )
	card = flask.request.json.get( 'card' )

	start = flask.request.json.get( 'start' )
	end = flask.request.json.get( 'end' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_new_agreement',
		'1',
		auth,
		listing_id,
		vehicle_id,
		card, start,
		end,
		timeout=5 )
	return flask.jsonify( msg ), code


@agreements_bp.route( '/agreements/<int:agreement_id>', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_agreement( agreement_id ):
	"""Retrieves a specific agreement. Note that only the host and driver
	 of an agreement can retrieve their own agreement while all other users
	  will get an unauthorized access.

	URI:
		http://api.tandemparkingco.com/vX.X.X/agreements/{id}

	HTTP Verb:
		GET

	Request Headers:
		Authorization: The auth token of the user trying to access the agreement.

	Response JSON::

		{
			"id": (int),
			"host": {
				/* "host" is only returned if the user making the call is the driver. */
				"id": (int),
				"firstName": (str),
				"lastName": (str)
			},
			"driver": {
				/* "host" is only returned if the user making the call is the host. */
				"id": (int),
				"firstName": (str),
				"lastName": (str)
			},
			"listing": (int) {
				"id": (int),
				"status": (str),
				"description": (str),
				"rate": (float),
				"latitude": (float),
				"longitude": (float)
			},
			"booking": {
				"id": (int),
				"type": (str),
				"repeating": (str),
				"startDateTime": (datetime),
				"endDateTime": (datetime)
			},
			"vehicle": {
				"id": (int),
				"licensePlateNumber": (str),
				"color": (str),
				"make": (str),
				"model": (str),
				"year": (str)
			},
			"messages": [
				/* Only returns the 5 latest messages. To get a complete or paginated
				 message history make a call to /agreements/{id}/messages. */
				{
					"id": (int),
					"messageContents": (str),
					"status": (str),
					"creationDate": (datetime)
				},
				...
			]
			"status": (str),
			"creationDate": (date),
			"finishDate": (date)
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_agreement',
		'1',
		auth,
		agreement_id )
	return flask.jsonify( msg ), code


@agreements_bp.route( '/agreements/current', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_cur_agreement():
	"""Retrieves all agreements curently open by a user.

	URI:
		http://api.tandemparkingco.com/vX.X.X/agreements/current

	HTTP Verb:
		GET

	Request Headers:
		Authorization: The auth token of the user.

	Response JSON::

		{
			"hostAgreements": [
				{
					"id": (int),
					"driver": {
						"id": (int),
						"firstName": (str),
						"lastName": (str)
					},
					"listing": (int) {
						"id": (int),
						"status": (str),
						"description": (str),
						"rate": (float),
						"latitude": (float),
						"longitude": (float)
					},
					"booking": {
						"id": (int),
						"type": (str),
						"repeating": (str),
						"startDate": (datetime),
						"endDate": (datetime)
					},
					"vehicle": {
						"id": (int),
						"licensePlateNumber": (str),
						"color": (str),
						"make": (str),
						"model": (str),
						"year": (str)
					},
					"messages": [
						/* Only returns the 5 latest messages. To get a complete or
						 paginated message history make a call to /agreements/{id}/messages. */
						{
							"id": (int),
							"messageContents": (str),
							"status": (str),
							"creationDate": (datetime)
						},
						...
					]
					"status": (str),
					"creationDate": (date),
					"finishDate": (date)
				},
				...
			],
			"driverAgreements": [
				/* "driverAgreements" will look exactly like "hostAgreements" but instead
				 of a "driver" object the agreements will have "host" objects. */
			]
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_cur_agreement',
		'1',
		auth )
	return flask.jsonify( msg ), code


@agreements_bp.route(
	'/agreements/<int:agreement_id>/cancel',
	methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def cancel_agreement( agreement_id ):
	"""Cancels an agreement. This can be called by either the host or the
	 driver, but the effects of cancellation will differ between the two and
	 it will also depend on whether the agreement is currently ongoing.

	URI:
		http://api.tandemparkingco.com/vX.X.X/agreements/{id}/cancel

	HTTP Verb:
		POST

	Request Headers:
		Authorization: The auth token of the user trying to access the agreement.

	Response JSON::

		data: {
			'refund': (int, optional)
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_cancel_agreement',
		'1',
		auth,
		agreement_id )
	return flask.jsonify( msg ), code


@agreements_bp.route(
	'/agreements/<int:agreement_id>/messages',
	methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def new_agreement_message( agreement_id ):
	"""Creates a new message for a specific agreement. A user may only write
	 to an open agreement they are in.

	URI:
		http://api.tandemparkingco.com/vX.X.X/agreements/{id}/messages

	HTTP Verb:
		POST

	Request Headers:
		Authorization: The auth token of the user trying to access the agreement.

	Request JSON::

		{
			"message": (str)
		}

	Response JSON:

		{
			"id": (int),
			"agreementId": (int),
			"messageContents": (str),
			"status": (str),
			"author": (str),
			"creationDate": (datetime)
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	message = flask.request.json.get( 'message' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_new_agreement_message',
		'1',
		auth,
		agreement_id,
		message )
	return flask.jsonify( msg ), code


@agreements_bp.route(
	'/agreements/<int:agreement_id>/messages',
	methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_agreement_messages( agreement_id ):
	"""Retrieves all the messages exchanged between host and driver within the
	 agreement. Only a host or driver may access the message history.

	URI:
		http://api.tandemparkingco.com/vX.X.X/agreements/{id}/messages

	HTTP Verb:
		GET

	Request Headers:
		Authorization: The auth token of the user trying to access the agreement.

	Response JSON:

		{ [
			{
				"id": (int),
				"messageContents": (str),
				"status": (str),
				"creationDate": (datetime)
			},
			...
		] }

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_agreement_messages',
		'1',
		auth,
		agreement_id )
	return flask.jsonify( msg ), code


@agreements_bp.route(
	'/agreements/<int:agreement_id>/dispute',
	methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def file_agreement_dispute( agreement_id ):
	"""Retrieves all the messages exchanged between host and driver within
	the agreement. Only a host or driver may access the message history.

	URI:
		http://api.tandemparkingco.com/vX.X.X/agreements/{id}/messages

	HTTP Verb:
		GET

	Request Headers:
		Authorization: The auth token of the user trying to access the agreement.

	Response JSON:

		"data": {

		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	dispute_type = flask.request.json.get( 'disputeType' )
	file_type = flask.request.json.get( 'fileType' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_file_agreement_dispute',
		'1',
		auth,
		agreement_id,
		dispute_type,
		file_type )
	return flask.jsonify( msg ), code
