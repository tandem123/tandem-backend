# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

"""Easy common function that performs asynchronous call and returns either
function data on timely calls and a job token for untimely jobs.

"""

# The access to celery results for the async system
import time

import celery

from tandem.task.celery_interface import tandem_celery_api
import tandem.tools.constants
import tandem.tools.distributed_logging

logger = tandem.tools.distributed_logging.get_logger( 'tandem.web.broker' )


def perform_async_call( method_name, version, auth, *args, **kwargs ):
	debug_dict = {}
	msg = {}

	start = time.time()

	args_list = [
		method_name,
		version,
		auth,
		tandem.tools.distributed_logging.get_request()
	]
	args_list += args
	async_call = (
		tandem_celery_api
		.signature( 'run_task' )
		.apply_async(
			args=args_list,
			serializer='json' ) )

	if kwargs.get( 'ignore_result', False ) is False:
		try:
			logger.info( 'Dispatched task request for %s' % method_name )
			data, code = async_call.get(
				timeout=kwargs.get(
					'timeout',
					tandem.tools.constants.CONST.DEFAULT_TIMEOUT ),
				interval=0.01 )
		except celery.exceptions.TimeoutError:
			logger.warning( 'Task timed out after %f seconds' % kwargs.get(
				'timeout',
				tandem.tools.constants.CONST.DEFAULT_TIMEOUT ) )
			data, code = { 'asyncToken': async_call.task_id }, 200
		except Exception:
			logger.error(
				'Task failed unexpectedly and caused an internal server error' )
			debug_dict.update( { 'error': async_call.traceback } )
			data, code = { 'whoops': 'Something went wrong.' }, 500

		msg.update(
			{
				'data': data,
				'status': (
					'success' if 200 <= code <= 299 else
					(
						'fail' if 400 <= code < 499 else
						'error' ) ) } )

		if msg['status'] != 'success':
			logger.warning( 'Task failed with error code %d' % code )

		elapsed_time = time.time() - start
		debug_dict.update( { 'elapsedTime': elapsed_time } )

		if tandem.tools.constants.CONST.DEBUG_MODE:
			msg.update( { 'debug': debug_dict } )

		return msg, code


def get_async_result( async_token ):
	logger.info( 'Fetching asyncronous task <%s>' % async_token )
	async_call = tandem_celery_api.AsyncResult( async_token )
	try:
		msg, code = async_call.get( timeout=0.01 )
	except celery.exceptions.TimeoutError:
		logger.warning( 'Asyncronous task <%s> not finished' % async_token )
		return { 'state': async_call.state  }, 200

	logger.info( 'Successfully fetched asyncronous task <%s>' % async_token )
	return { 'state': 'COMPLETE', 'data': msg }, code
