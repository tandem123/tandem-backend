# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

"""API for dealing with manipulation of listings at /tandem/api/v1.0/listings/.

Listings are the parking spaces that hosts lease out to drivers. They contain
 information such as address, long/lat coordinates, images, names, and other
  properties to help driver identify the space when looking for they one they
   rented. Each listing is unique and restricted to only hosts.

Attributes:
	listings_bp (Bluprint): Blueprint that allows the agreement program to link
		 into the main API.

"""

import flask

import tandem.web.worker_broker
import tandem.web.web_logging

listings_bp = flask.Blueprint( 'listings_bp', __name__ )


@listings_bp.route( '/listings', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_all_listings():
	"""Retrieves all the listings. Called at '/tandem/api/vX.X/listings' with GET

	Returns all the information of all the listings currently in the database.

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings

	HTTP Verb:
		GET

	Response JSON::

		{
			"data": [
				{
					"id": (int),
					"host": {
						"id": (int),
						"firstName": (str),
						"lastName": (str)
					},
					"images": [
						(str)url,
						...
					],
					"status": (str),
					"description": (str),
					"rate": (int),
				},
				...
			]
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_listing',
		'1',
		auth,
		-1 )

	return flask.jsonify( msg ), code


@listings_bp.route('/listings/<int:listing_id>', methods=['GET'])
@tandem.web.web_logging.prepare_endpoint
def get_listing( listing_id ):
	"""Retrieves a specific listing. Called at
	 '/tandem/api/vX.X/listings/<listing_id>' with GET.

	Returns all the information of a specific listing identified by it's id.

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings/{id}

	HTTP Verb:
		GET

	Response JSON::

		{
			"data": {
				"id": (int),
				"host": {
					"id": (int),
					"firstName": (str),
					"lastName": (str)
				},
				"images": [
					(str)url,
					...
				],
				"status": (str),
				"description": (str),
			}
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_listing',
		'1',
		auth,
		listing_id )

	return flask.jsonify( msg ), code


@listings_bp.route('/listings', methods=['POST'])
@tandem.web.web_logging.prepare_endpoint
def new_listing():
	"""Creates a new listing. Called at '/tandem/api/vX.X/listings' with POST.

	Creates a new listing by the user as specified by the authorization token
	 supplied in the header.
	Note that listings can only be created by hosts.

	If the supplied address matches against an address with slightly different
	 fields then the later is saved. The returned address information will reflect
	 this as well.

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings

	HTTP Verb:
		POST

	Examples::

		For a fully complete US address you would need the following:
		{
			"throughfare": "6552 Del Playa Dr", /* Street number and road */
			"premise": "Unit 101", /* Apartment # */
			"locality": "Isla Vista", /* City */
			"administrativeArea": "CA", /* State in ISO format */
			"postalCode": "93117", /* Zipcode */
			"country": "US" /* Country in ISO format */
		}

	Request Headers:
		Authorization: The auth token of the host should be supplied in bearer
			 format.

	Request JSON::

		{
			"rules": [
				{
				},
				...
			],
			"description": (str)
			"country": (optional, str),
			"administrativeArea": (optional, str),
			"subadministrativeArea": (optional, str),
			"locality": (optional, str),
			"dependentLocality": (optional, str),
			"postalCode": (optional, str),
			"throughfare": (optional, str),
			"premise": (optional, str),
			"subpremise": (optional, str),
			"latitude": (optional, str),
			"longitude": (optional, str)
		}

	Response JSON::

		{
			"id": (int),
			"hostId": (int),
			"address": {
				"id": (int),
				"country": (str),
				"administrativeArea": (optional, str),
				"subadministrativeArea": (optional, str),
				"locality": (optional, str),
				"dependentLocality": (optional, str),
				"postalCode": (optional, str),
				"throughfare": (optional, str),
				"premise": (optional, str),
				"subpremise": (optional, str),
				"latitude": (optional, str),
				"longitude": (optional, str)
			}
			"status": (str),
			"description": (str),
			"images": [
				(str),
				...
			]
			"geohash": (str)
		}

	"""

	auth = flask.request.headers.get( 'Authorization' )

	rules = flask.request.json.get( 'rules' )
	description = flask.request.json.get( 'description' )
	country = flask.request.json.get( 'country' )
	administrative_area = flask.request.json.get( 'administrativeArea' )
	sub_administrative_area = flask.request.json.get( 'subadministrativeArea' )
	locality = flask.request.json.get( 'locality' )
	dependent_locality = flask.request.json.get( 'dependentLocality' )
	postal_code = flask.request.json.get( 'postalCode' )
	throughfare = flask.request.json.get( 'throughfare' )
	premise = flask.request.json.get( 'premise' )
	sub_premise = flask.request.json.get( 'subpremise' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_new_listing',
		'1',
		auth,
		description,
		rules,
		country,
		administrative_area,
		sub_administrative_area,
		locality,
		dependent_locality,
		postal_code,
		throughfare,
		premise,
		sub_premise )

	return flask.jsonify( msg ), code


@listings_bp.route( '/listings/<int:listing_id>', methods=['PATCH'] )
@tandem.web.web_logging.prepare_endpoint
def update_listing( listing_id ):
	""" Modifys a current vehicle with a JSON Patch payload. Returns only the
	 attributes that were affected by the patch and their new value.

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings/{id}

	HTTP Verb:
		PATCH

	Request Headers:
		Authorization: The auth token of the driver should be supplied in
			 bearer format.

	Request JSON::

		[
			{
				"op": (str),
				"path": (str),
				"value": ()
			},
			...
		]

		Patch should follow JSON Patch( RFC6902 ) format.

	Response JSON::

		Returns any attributes that are changed as keys and their new value.
		 If an attribute is not returned then it wasn't affected by the
		 update.

	"""

	auth = flask.request.headers.get( 'Authorization' )

	patches = flask.request.json

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_update_listing',
		'1',
		auth,
		listing_id,
		patches )
	return flask.jsonify( msg ), code


@listings_bp.route( '/listings/<int:listing_id>', methods=['DELETE'] )
@tandem.web.web_logging.prepare_endpoint
def delete_listing( listing_id ):
	""" Deletes a listing.

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings/{id}

	HTTP Verb:
		Delete

	Request Headers:
		Authorization: The auth token of the driver should be supplied
		 in bearer format.

	Response JSON::

		Returns any attributes that are changed as keys and their new
		 value. If an attribute is not returned then it wasn't affected
		 by the
		update.

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_delete_listing',
		'1',
		auth,
		listing_id )
	return flask.jsonify( msg ), code


@listings_bp.route( '/listings/<int:listing_id>/images', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def listing_add_image( listing_id ):
	auth = flask.request.headers.get( 'Authorization' )

	file_types = flask.request.json.get( 'fileTypes' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_listing_add_image',
		'1',
		auth,
		listing_id,
		file_types )
	return flask.jsonify( msg ), code


@listings_bp.route(
	'/listings/<int:listing_id>/images/<path:image_id>',
	methods=['DELETE'] )
@tandem.web.web_logging.prepare_endpoint
def listing_delete_image( listing_id, image_id ):
	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_listing_delete_image',
		'1',
		auth,
		listing_id,
		image_id )
	return flask.jsonify( msg ), code


@listings_bp.route( '/listings/search', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def search_listings():
	""" Searches for all listings nearby the specified location up to 50km

	Radius should be in kilometers.

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings/search

	HTTP Verb:
		GET

	Request::

		?latitude = (float)
		&longitude = (float)
		&radius = (float)
		&filters = (str)

	Response JSON::

		{ [
			{
				"id": (int),
				"hostId": (int),
				"status": (str),
				"descrption": (str),
				"rate": (int),
				"latitude": (float),
				"longitude": (float),
				"geohash": (str)
			}, ...
		] }

	"""

	auth = flask.request.headers.get( 'Authorization' )
	latitude = flask.request.args.get( 'latitude' )
	longitude = flask.request.args.get( 'longitude' )
	radius = flask.request.args.get( 'radius' )
	filters = flask.request.args.get( 'filters' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_search_listings',
		'1',
		auth,
		latitude,
		longitude,
		radius,
		filters )
	return flask.jsonify( msg ), code


@listings_bp.route( '/listings/<int:listing_id>/rules', methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def new_listing_rule( listing_id ):
	""" Create a blackout for a listing

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings/{id}/rules

	HTTP Verb:
		POST

	Request JSON::

		The type specifies extra parameters that should be sent for the rule
		 creation. Hourly and Flatrate only accept the "rate" parameter.

		For each rule you can also specify the dates it should be created with.

		[
			{
				"type": (str),
				(optional)"rate": (int),
				(optional)"dates": [
					{
						"start": (datetime),
						"end": (datetime),
						(optional)"repeating": (str, default="None")
					},
					...
				]
			},
			...
		]

	Response JSON::

		The response will include the request json and whether each rule
		 and date creation was successful. If a rule or date was unsuccessfully
		 created then the reason will also be returned. Successfully created
		 rules will also have an "id" with them.

		"data": [
			{
				"type": (str),
				"rate": (int),
				"id": (str),
				"status": {
					"accepted": (bool),
					"reason": (str)
				}
				"dates": [
					{
						"start": (datetime),
						"end": (datetime),
						"repeating": (str)
						"status": {
							"accepted": (bool),
							"reason": (str)
						}
					},
					...
				]
			},
			...
		]

	"""
	auth = flask.request.headers.get( 'Authorization' )

	rules = flask.request.json

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_new_listing_rule',
		'1',
		auth,
		listing_id,
		rules )
	return flask.jsonify( msg ), code


@listings_bp.route( '/listings/<int:listing_id>/rules', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_all_listing_rules( listing_id ):
	""" Create a blackout for a listing

	URI:
		https://api.tandemparkingco.com/vX.X.X/listings/{id}/rules

	HTTP Verb:
		GET

	Response JSON::

		Returns all the rules of the requested listing and the rules dates.

		"data": [
			{
				"type": (str),
				"rate": (int),
				"id": (str),
				"dates": [
					{
						"start": (datetime),
						"end": (datetime),
						"repeating": (str)
					},
					...
				]
			},
			...
		]

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_all_listing_rules',
		'1',
		auth,
		listing_id )
	return flask.jsonify( msg ), code


@listings_bp.route(
	'/listings/<int:listing_id>/rules/<path:rule_id>',
	methods=['DELETE'] )
@tandem.web.web_logging.prepare_endpoint
def delete_listing_rule( listing_id, rule_id ):
	""" Remove a rule from a listing

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings/{id}/rules/{id}

	HTTP Verb:
		DELETE

	Response JSON::

		Returns the id of the rule if successfully deleted.

		"data": (str)

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_delete_listing_rule',
		'1',
		auth,
		listing_id,
		rule_id )
	return flask.jsonify( msg ), code


@listings_bp.route(
	'/listings/<int:listing_id>/rules/<path:rule_id>/dates',
	methods=['POST'] )
@tandem.web.web_logging.prepare_endpoint
def new_listing_rule_date( listing_id, rule_id ):
	""" Create a date for a listing's rule

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings/{id}/rules/{id}/dates

	HTTP Verb:
		POST

	Request JSON::

		[
			{
				"start": (str),
				"end": (str),
				(optional)"repeating": (str, default="None")
			},
			...
		]

	"""

	auth = flask.request.headers.get( 'Authorization' )
	dates = flask.request.json

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_new_listing_rule_date',
		'1',
		auth,
		listing_id,
		rule_id,
		dates )
	return flask.jsonify( msg ), code


@listings_bp.route(
	'/listings/<int:listing_id>/rules/<path:rule_id>/dates/<path:start>',
	methods=['DELETE'] )
@tandem.web.web_logging.prepare_endpoint
def delete_listing_rule_date( listing_id, rule_id, start ):
	""" Remove a date from a listing's rule

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings/{id}/rules/{id}/dates/{start}

	HTTP Verb:
		DELETE

	Response JSON::

		Returns the start date if successfully deleted.

		"data": (str)

	"""

	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_delete_listing_rule_date',
		'1',
		auth,
		listing_id,
		rule_id,
		start )
	return flask.jsonify( msg ), code


@listings_bp.route(
	'/listings/<int:listing_id>/bookings',
	methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def get_listing_bookings( listing_id ):
	""" Get the bookings for a listing on a particular day

	URI:
		http://api.tandemparkingco.com/vX.X.X/listings/{id}/bookings

	HTTP Verb:
		GET

	Request::

		?date = (str) /* format should be YYYY-MM-DD[T00:00Z] which is UTC time */

	Response JSON::

		{ [
			{
				"startTime": (str)
				"endTime": (str)
			}, ...
		] }

	"""

	auth = flask.request.headers.get( 'Authorization' )
	date = flask.request.args.get( 'date' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_get_listing_bookings',
		'1',
		auth,
		listing_id,
		date )
	return flask.jsonify( msg ), code
