# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, September 2016

"""API for dealing with asynchronous tasks at /tandem/api/v1.0/tasks/.

URI for checking the state and data of asynchronous tasks from previous
 web calls that did not complete in a timely manner. These tokens are the
 Id of the task that sits in the RabbitMQ Server. Tasks do not exist
 indefintely and will be deleted after a short time of not being read.

Attributes:
	tasks_api (Bluprint): Blueprint that allows the agreement program to
	 link into the main API.

"""

import flask

import tandem.web.worker_broker
import tandem.web.web_logging

tasks_bp = flask.Blueprint( 'tasks_bp', __name__ )


@tasks_bp.route('/tasks/<path:async_token>', methods=['GET'])
@tandem.web.web_logging.prepare_endpoint
def get_task( async_token ):
	"""Get's the state and data from the task with id async
	token. Called at '/tandem/api/vX.X/tasks/<path:async_token>'
	with GET.

	If the task hasn't expired in the rabbitmq server, the task is
	 polled for it's state. If complete we return the data. If not
	 PENDING is returned instead.

	URI:
		http://api.tandemparkingco.com/vX.X.X/tasks/{token}

	HTTP Verb:
		GET

	HTTP Response JSON::

		The response to an asynchronous task request will be the data
		 that the original task returned.

	"""

	msg, code = tandem.web.worker_broker.get_async_result( async_token )

	return flask.jsonify( msg ), code
