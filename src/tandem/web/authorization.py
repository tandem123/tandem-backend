# Copyright (C) Tandem Parking, Inc - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
# Written by Jacob Hull <jacob@tandemparking.co>, May 2017

import flask

import tandem.web.worker_broker
import tandem.web.web_logging

authorization_bp = flask.Blueprint( 'authorization_bp', __name__ )


@authorization_bp.route( '/authorization/user', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def authenticate_user():
	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_authenticate_user',
		'1',
		None,
		auth,
		None )

	return flask.jsonify( msg ), code


@authorization_bp.route( '/authorization/token', methods=['GET'] )
@tandem.web.web_logging.prepare_endpoint
def authenticate_token():
	auth = flask.request.headers.get( 'Authorization' )

	msg, code = tandem.web.worker_broker.perform_async_call(
		'task_authenticate_token',
		'1',
		None,
		auth )

	return flask.jsonify( msg ), code
