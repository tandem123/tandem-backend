.. Tandem Parking API documentation master file, created by
   sphinx-quickstart on Sat Feb  6 12:15:00 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tandem Parking API's documentation!
==============================================

Contents:

.. toctree::
   :maxdepth: 2
   
   beginner-client
   beginner-server
   modules
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

