Getting Started - Client
========================

Retrieving All Users
--------------------

Using the tandem server api is easy as hell. All connections are made using HTTP protocol. These simple
commands allow you to do view and make changes to resources on the server.

These resources are located at specific URI's that show which resource you are accessing IE:

* /tandem/api/v1.0/users for the resource of Users
* /tandem/api/v1.0/users/auth for the authentication of Users
* /tandem/api/v1.0/tasks for the resource of tasks.

An easy resource read that can be shown now is retrieving all the users in the database. Open your
web browser and visit http://tandemtestsite.net/tandem/api/v1.0/users. You should see
something like this::

	{
		"users": [
		{
			"id": 1, 
			"password": "$2b$15$Afv4lYZmzWOFAgeFbsO.4e6z9kvVPWf8eI9qSCr1FLU1cJFZQNl2m", 
			"username": "test1"
		}, 
		{
			"id": 2, 
			"password": "$2b$15$Afv4lYZmzWOFAgeFbsO.4e6z9kvVPWf8eI9qSCr1FLU1cJFZQNl2m", 
			"username": "test2"
		}, 
		{
			"id": 3, 
			"password": "$2b$15$fEfRnfMOFhBDFW1p2Q7r1.YUWo7rZj345uIGcmQuWirDnXMaG3TEy", 
			"username": "test3"
		},
		...
	}
	
A more practical example that shows how to construct a message with curl is shown as::

	curl -H "Content-Type: application/json" -X GET http://tandemtestsite.com/tandem/api/v1.0/users
	
This should retrieve the same information as the web browser call, but using curl allows us to do more
complex calls such as creating and updating resources.

Retrieving A Single User
------------------------

From now on we're going to make all our calls to the server with curl so when developing the app
you know how to structure the message.

We are now going to try getting a single user by their id. Every user has a unique id and most
interactions with other users is with their id. We can get the user with id '1' with this call::

	curl -H "Content-Type: application/json" -X GET http://tandemtestsite.com/tandem/api/v1.0/users/1
	
It's as easy as putting the id after the resource. This will work for any user.