Getting Started - Server
========================

Server Components
--------------------

.. image:: server-diagram.png

A key part is understanding the multiple components running on the server that allows fast and quick
IO response. Here is a quick breakdown of each from the diagram above.

Web Server
~~~~~~~~~~

This is the web interface. Our code is not dependent and this will form the robust HTTP server that
interacts with the user. The best setup for our purpose would be running Gunicorn behind nginx.

Flask
~~~~~

This is the RESTful API and is responsible for decoding what the client is asking for when accessing
a URI. Usually run through Gunicorn, you can start a flask server for testing by calling::

	python wsgi.py
	
Flask does not do the heavy lifting in terms of processing power, but acts as an intermediary between
the client and server resources. In moments of high server stress Flask still acts in a timely manner
by handing out asynchronous tasks ids to clients so they can repoll the server for their request.
	
Flask Worker
~~~~~~~~~~~~

When flask is running it creates *n* concurrent running workers that are responsible for handling every
server request. *n* is the number of processesors on the machine. This allows easy vertical scaling
by adding more processors to handle more concurrent server requests.

Memcache
~~~~~~~~

Note::

	This is not currently implemented
	
Memcache is a ram based caching mechanism for existing calls. In this information that is being accessed
frequently can be taken from ram( quick ) instead of going through the distributed work queue and possibly
reading from the MySQL database( slow ). This should speed up flask response time in cases where many users
are inquiring the same information IE: A large event occuring in a small area.

Warning::

	It's important to carefully note what can be cached and what can not. Information that is changing
	frequently is not a good candidate to be changed, and even for information that does changed
	infrequently there should be adequate measures to update changed information in the cache.
	
RabbitMQ Server
~~~~~~~~~~~~~~~

This ram based database houses all the tasks currently in queue or finished but waiting to be polled.
Flask will write task requests into this server and Celery will be reading/processing tasks as they appear.
By seperating handling IO and processing, the server can still run as fast as possible in either area
should the other become choked.

Celery Worker
~~~~~~~~~~~~~

The Celery Worker is responsible for all MySQL interaction and serverside processing. There are *n*
workers where *n* is the number of processors on the machine. This easily allows server processing to
be scaled vertically.

Celery Worker can be invoked by calling::

	celery worker -A workerd
	
MySQL Server
~~~~~~~~~~~~

The database that stores all relevant information about Tandem things. This includes User data, car data,
parking spot data, etc..