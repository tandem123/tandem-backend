Tandem Restful API
==================

Summary
-------
.. qrefflask:: tandem.web:tandem_web_api
	:order: path

Users
-----

.. autoflask:: tandem.web:tandem_web_api
	:blueprints: users_bp
	:order: path
	
Listings
--------

.. autoflask:: tandem.web:tandem_web_api
	:blueprints: listings_bp
	:order: path